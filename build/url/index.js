"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/* eslint-disable no-useless-escape */
var Url =
/*#__PURE__*/
function () {
  function Url(routes) {
    _classCallCheck(this, Url);

    this.routes = Url.parseRoutes(routes);
  }

  _createClass(Url, [{
    key: "parse",

    /**
     * ANCHOR Parse Function
     * @author: yas
     * @desc return Params
     * @return {object}
     */
    value: function parse() {
      var _window$location = window.location,
          pathname = _window$location.pathname,
          search = _window$location.search;
      var routename = this.getRouteName();
      var segments = pathname.slice(1).split('/');
      var params = Url.getParams(segments, this.routes[routename]);
      var query = Url.getQuery(search);
      return _objectSpread({}, window.location, {
        routename: routename,
        params: params,
        query: query,
        format: this.routes[routename].format
      });
    }
    /**
     * ANCHOR GetRouteName Function
     * @author: yas
     * @desc
     * @return {string} route name
     */

  }, {
    key: "getRouteName",
    value: function getRouteName() {
      var href = window.location.href;
      var routeName;
      Object.keys(this.getRoutes).map(function (route) {
        if (href.indexOf(route) > -1) {
          routeName = route;
        }

        return false;
      });
      return routeName;
    }
    /**
     * ANCHOR GetSegment Function
     * @author: yas
     * @desc
     * @param {object} routes
     * @return {object}
     */

  }, {
    key: "getRoutes",
    get: function get() {
      return this.routes;
    }
  }, {
    key: "params",
    get: function get() {
      return this.parse().params;
    }
  }, {
    key: "query",
    get: function get() {
      return this.parse().query;
    }
  }, {
    key: "routename",
    get: function get() {
      return this.getRouteName();
    }
  }], [{
    key: "parseRoutes",
    value: function parseRoutes(routes) {
      var newRoutes = {};
      Object.keys(routes).map(function (key) {
        newRoutes[key] = {
          format: routes[key],
          parse: function () {
            return routes[key].slice(1).split('/').map(function (item) {
              var _item$split = item.split(':'),
                  _item$split2 = _slicedToArray(_item$split, 2),
                  name = _item$split2[0],
                  format = _item$split2[1];

              return [name, {
                name: name,
                format: format ? format.replace(/({)([A-Za-z0-9])\w+\S/g, '') : '',
                content: format ? format.replace(/(([a-zA-Z]*-{)|(}([a-zA-z]){)|(}(-){)|{|})/g, '|').split('|').filter(function (i) {
                  return i !== '';
                }) : []
              }];
            });
          }()
        };
        return newRoutes[key];
      });
      return newRoutes;
    }
  }, {
    key: "getSegment",
    value: function getSegment(url, route) {
      var obj = {};
      url.map(function (item, i) {
        obj[route.parse[i][0]] = item;
        return false;
      });
      return obj;
    }
  }, {
    key: "getParams",
    value: function getParams(segments, route) {
      var obj = {};
      if (!route) return obj;
      segments.map(function (seqment, i) {
        var values = route.parse[i][1].format !== '' ? seqment.split(route.parse[i][1].format).filter(function (ii) {
          return ii !== '';
        }) : [seqment];
        values.map(function (param, iii) {
          if (route.parse[i][1].content[iii]) {
            obj[route.parse[i][1].content[iii]] = param;
          }

          return false;
        });
        return false;
      });
      return obj;
    }
  }, {
    key: "getQuery",
    value: function getQuery(str) {
      if (str) {
        return str.slice(1).split('&').map(function (p) {
          return p.split('=');
        }).reduce(function (obj, pair) {
          var _pair$map = pair.map(decodeURIComponent),
              _pair$map2 = _slicedToArray(_pair$map, 2),
              key = _pair$map2[0],
              value = _pair$map2[1];

          return _objectSpread({}, obj, _defineProperty({}, key, value));
        }, {});
      }

      return {};
    }
  }]);

  return Url;
}();

exports.default = Url;