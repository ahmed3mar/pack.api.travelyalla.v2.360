"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _api = _interopRequireDefault(require("../api"));

var _hotels = _interopRequireDefault(require("../config/hotels"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var instance = null;
/**
 * example : Hotels.search({...params}, [sort, fliter, tec...])
 */

var Hotels =
/*#__PURE__*/
function (_Api) {
  _inherits(Hotels, _Api);

  function Hotels(props) {
    var _this;

    _classCallCheck(this, Hotels);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Hotels).call(this, _objectSpread({}, _hotels.default, {}, props)));

    if (instance) {
      return _possibleConstructorReturn(_this, instance);
    }

    instance = _assertThisInitialized(_this);
    return _this;
  }

  _createClass(Hotels, [{
    key: "details",
    value: function () {
      var _details = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee(id) {
        var route;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                route = this.route('details', {
                  id: id
                });
                _context.next = 3;
                return this.get(route, {
                  id: id
                }).then(function (res) {
                  return res.data.data;
                });

              case 3:
                return _context.abrupt("return", _context.sent);

              case 4:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function details(_x) {
        return _details.apply(this, arguments);
      }

      return details;
    }()
  }, {
    key: "rooms",
    value: function () {
      var _rooms2 = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee2(id, _ref) {
        var check_in, check_out, _rooms, route;

        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                check_in = _ref.check_in, check_out = _ref.check_out, _rooms = _ref.rooms;
                route = this.route('rooms', {
                  id: id
                });
                if (_typeof(_rooms) === 'object') _rooms = JSON.stringify(_rooms);
                _context2.next = 5;
                return this.post(route, {
                  check_in: check_in,
                  check_out: check_out,
                  rooms: _rooms
                }).then(function (res) {
                  return res.data.data;
                });

              case 5:
                return _context2.abrupt("return", _context2.sent);

              case 6:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function rooms(_x2, _x3) {
        return _rooms2.apply(this, arguments);
      }

      return rooms;
    }()
  }, {
    key: "cancellationPolicy",
    value: function cancellationPolicy(id, _ref2) {
      var check_in = _ref2.check_in,
          check_out = _ref2.check_out,
          rooms = _ref2.rooms,
          params = _objectWithoutProperties(_ref2, ["check_in", "check_out", "rooms"]);

      var route = this.route('cancellationPolicy', {
        id: id
      });
      if (_typeof(rooms) === 'object') rooms = JSON.stringify(rooms);
      return this.post(route, _objectSpread({
        check_in: check_in,
        check_out: check_out,
        rooms: rooms
      }, params));
    }
  }, {
    key: "room",
    value: function () {
      var _room = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee3(id, _ref3) {
        var check_in, check_out, rooms, params, route;
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                check_in = _ref3.check_in, check_out = _ref3.check_out, rooms = _ref3.rooms, params = _objectWithoutProperties(_ref3, ["check_in", "check_out", "rooms"]);
                route = this.route('room', {
                  id: id
                });
                _context3.next = 4;
                return this.post(route, _objectSpread({
                  check_in: check_in,
                  check_out: check_out,
                  rooms: rooms
                }, params)).then(function (res) {
                  return res.data.data;
                });

              case 4:
                return _context3.abrupt("return", _context3.sent);

              case 5:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function room(_x4, _x5) {
        return _room.apply(this, arguments);
      }

      return room;
    }()
  }, {
    key: "preBooking",
    value: function () {
      var _preBooking = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee4(_ref4) {
        var props, route;
        return regeneratorRuntime.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                props = _extends({}, _ref4);
                route = this.route('pre_booking');
                _context4.next = 4;
                return this.post(route, props).then(function (res) {
                  return res.data.data;
                });

              case 4:
                return _context4.abrupt("return", _context4.sent);

              case 5:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this);
      }));

      function preBooking(_x6) {
        return _preBooking.apply(this, arguments);
      }

      return preBooking;
    }() // roomDetails(id, { check_in, check_out, rooms, ...params }) {
    //   return this.room(id, { check_in, check_out, rooms, ...params });
    // }
    // cancellationPolicy(id, { check_in, check_out, rooms, ...params }) {
    //   if (typeof rooms === 'object') rooms = JSON.stringify(rooms);//   return Api.post(Api.url(`hotels/${id}/cancellation-policy`), { check_in, check_out, rooms, ...params });
    // }
    // search(req, fn = []) {
    //   let fns = fn;
    //   if (req && typeof req === 'object' && req.constructor === Array) {
    //     fns = req;
    //   }
    //   if (fns.length > 0) {
    //     this._fns = fns;
    //   }
    //   if (req && typeof req === 'object' && req.constructor === Object) {
    //     this._req = req;
    //   }
    //   const { config } = this;
    //   const { data } = config;
    //   const param = { ...data, ...req };
    //   return new Promise((resolve, reject) => {
    //     const request = axios.post(Api.url('hotels/search'), { ...param });
    //     request.then((res) => {
    //       resolve(pipe(...fns)(res.data));
    //       this._results = res.data;
    //     });
    //     request.catch((err) => {
    //       reject(err);
    //     });
    //   });
    // }

  }]);

  return Hotels;
}(_api.default);

exports.default = Hotels;