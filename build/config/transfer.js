"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = {
  name: 'transfer',
  url: 'http://192.168.1.111:8000/api',
  version: 1,
  data: {
    transfer: true
  },
  routes: {
    search: 'transfer/search',
    details: 'test',
    arrivals: function arrivals(url, version, params) {
      return "".concat(url, "/v").concat(version, "/transfers/transfer_locations/").concat(params.key);
    }
  }
};
exports.default = _default;