"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = {
  name: 'hotels',
  url: 'https://www.travelyalla.com/api',
  // url: 'http://localhost:8000/api',
  version: 1,
  data: {
    transfer: true
  },
  routes: {
    details: function details(url, version, params) {
      return "".concat(url, "/v").concat(version, "/hotels/").concat(params.id, "/details");
    },
    rooms: function rooms(url, version, params) {
      return "".concat(url, "/v").concat(version, "/hotels/").concat(params.id, "/rooms");
    },
    room: function room(url, version, params) {
      return "".concat(url, "/v").concat(version, "/hotels/").concat(params.id, "/reservation");
    },
    cancellationPolicy: function cancellationPolicy(url, version, params) {
      return "".concat(url, "/v").concat(version, "/hotels/").concat(params.id, "/cancellation-policy");
    },
    arrivals: 'arrivals',
    pre_booking: 'hotels/pre_booking'
  }
};
exports.default = _default;