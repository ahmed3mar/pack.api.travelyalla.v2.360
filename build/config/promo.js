"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = {
  name: 'promo',
  url: 'https://www.travelyalla.com/api',
  // url: 'http://localhost:8000/api',
  version: 1,
  data: {
    promo: true
  },
  routes: {
    check: 'promo'
  }
};
exports.default = _default;