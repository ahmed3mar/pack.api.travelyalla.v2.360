"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

/**
 * ANCHOR GetValueByObject Function
 * @author: yas
 * @desc
 * @param {object} object
 * @param {str} str
 * @return {*}
 */
function getValueByObject(object, str) {
  var arr = str.split('.');
  var val = object;
  arr.map(function (k) {
    val = val[k];
    return false;
  });
  return val;
}

var _default = getValueByObject;
exports.default = _default;