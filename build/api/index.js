"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _axios = _interopRequireDefault(require("axios"));

require("babel-polyfill");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Api =
/*#__PURE__*/
function () {
  function Api() {
    var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    _classCallCheck(this, Api);

    this.config = config;
    Api.info = this.config;
  }

  _createClass(Api, [{
    key: "setHeaders",
    value: function setHeaders(headers) {
      this.config = _objectSpread({}, this.config, {
        data: _objectSpread({}, this.config.data, {
          headers: _objectSpread({}, this.config.headers, {}, headers)
        })
      });
      return this;
    }
    /**
     * ANCHOR Route Function
     * @author: yas
     * @param {string} param
     * @return {string} url
     */

  }, {
    key: "route",
    value: function route(param) {
      var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      var _this$config = this.config,
          url = _this$config.url,
          version = _this$config.version;

      if (typeof param !== 'string') {
        // add new route
        var route = Api.routeParser(url, version, param.path);
        this.config.routes[param.name] = param.path;
        return route;
      } // return route


      return Api.routeParser(url, version, this.config.routes[param], params);
    }
    /**
     * ANCHOR Get Function
     * @author: yas
     * @param {string} url
     * @param {object} params
     * @return {object} return axios result
     */

  }, {
    key: "get",
    value: function () {
      var _get = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee(url, params) {
        var data, param;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                data = this.config.data;
                param = _objectSpread({}, data, {}, params); // const headers = {
                //   'Access-Control-Allow-Origin': '*'
                // };

                _context.next = 5;
                return _axios.default.get(url, _objectSpread({
                  params: _objectSpread({}, params)
                }, data));

              case 5:
                return _context.abrupt("return", _context.sent);

              case 8:
                _context.prev = 8;
                _context.t0 = _context["catch"](0);
                throw _context.t0;

              case 11:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 8]]);
      }));

      function get(_x, _x2) {
        return _get.apply(this, arguments);
      }

      return get;
    }()
    /**
     * ANCHOR Post Function
     * @author: yas
     * @param {string} url
     * @param {object} params
     * @return {object} return axios result
     */

  }, {
    key: "post",
    value: function () {
      var _post = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee2(url, params) {
        var data, headers;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                data = this.config.data;
                headers = _objectSpread({
                  'Access-Control-Allow-Origin': '*',
                  'Content-Type': 'application/json'
                }, data.headers);
                _context2.next = 5;
                return _axios.default.post(url, params, _objectSpread({}, data, {
                  headers: headers
                }));

              case 5:
                return _context2.abrupt("return", _context2.sent);

              case 8:
                _context2.prev = 8;
                _context2.t0 = _context2["catch"](0);
                throw _context2.t0;

              case 11:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[0, 8]]);
      }));

      function post(_x3, _x4) {
        return _post.apply(this, arguments);
      }

      return post;
    }()
    /**
     * ANCHOR Get Info Function
     * @author: yas
     * @return {object} api information
     */

  }, {
    key: "configObject",

    /**
    * ANCHOR Route Parser Function
    * @author: yas
    * @return {object} config
    */
    get: function get() {
      return this.config;
    }
  }], [{
    key: "routeParser",

    /**
    * ANCHOR Route Parser Function
    * @author: yas
    * @param {string} url
    * @param {number} version
    * @param {string} path
    * @return {string} path
    */
    value: function routeParser(url, version, path, params) {
      if (typeof path === 'function') {
        return path(url, version, params);
      }

      return "".concat(url, "/v").concat(version, "/").concat(path);
    }
  }, {
    key: "info",
    get: function get() {
      return this.APIINFO;
    }
    /**
    * ANCHOR Set Info Function
    * @author: yas
    * @param {object} i
    */
    ,
    set: function set(i) {
      if (!Object.prototype.hasOwnProperty.call(Api, 'APIINFO')) {
        this.APIINFO = {};
      }

      this.APIINFO = _objectSpread({}, this.APIINFO, _defineProperty({}, i.name, i));
    }
  }]);

  return Api;
}();

exports.default = Api;