"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var instance = null;

var ParseUrl =
/*#__PURE__*/
function () {
  function ParseUrl() {
    _classCallCheck(this, ParseUrl);

    this.params = [];
    this.schemaOneWay = "{from}-{from_code}/{to}-{to_code}/{outbound}/{outboundTime}/{adults}-{children}-{infants}";
    this.schemaRoundTrip = "{from}-{from_code}/{to}-{to_code}/{outbound}/{outboundTime}/{inbound}/{inboundTime}/{adults}-{children}-{infants}";

    if (instance) {
      return instance;
    }
  }

  _createClass(ParseUrl, [{
    key: "toString",
    value: function toString(obj) {
      console.log('obj', obj);
      var url = this.schemaOneWay;

      if (obj.inbound) {
        url = this.schemaRoundTrip;
      }

      Object.keys(obj).map(function (k) {
        var val = obj[k];

        try {
          val = obj[k].split(' ').join('');
        } catch (e) {}

        url = url.replace("{".concat(k, "}"), val);
      });
      return url;
    }
  }, {
    key: "toObject",
    value: function toObject(str) {
      var param = str.split('/');
      var schema = this.schemaOneWay.split('/');
      var obj = {};

      if (param.length > 5) {
        schema = this.schemaRoundTrip.split('/');
      }

      schema.map(function (s, i) {
        var keys = s;
        var values = param[i];

        if (keys.search('-') > -1) {
          keys = s.split('-');
          values = param[i].split('-');
        }

        if (_typeof(keys) === 'object') {
          keys.map(function (key, ii) {
            obj[key.replace(/([^a-zA-Z0-9_])/g, '')] = values[ii];
          });
          return;
        }

        obj[keys.replace(/([^a-zA-Z0-9_])/g, '')] = values;
      });
      return obj;
    }
  }]);

  return ParseUrl;
}();

exports.default = ParseUrl;