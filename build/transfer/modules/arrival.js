"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _index = _interopRequireDefault(require("../index"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Arrival =
/*#__PURE__*/
function () {
  function Arrival(arrival) {
    _classCallCheck(this, Arrival);

    this.arrival = arrival;
  }

  _createClass(Arrival, [{
    key: "transfer",
    value: function transfer() {
      return new _index.default();
    }
  }, {
    key: "data",
    value: function data() {
      return "".concat(this.arrival.name, ", ").concat(this.arrival.country, ", ").concat(this.arrival.country_code);
    }
  }, {
    key: "type",
    value: function type() {
      return this.arrival.type;
    }
  }, {
    key: "to",
    value: function () {
      var _to = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee() {
        var _this = this;

        var route;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                route = this.transfer().route('arrivals');
                _context.next = 3;
                return this.transfer().get(route, {
                  name: this.arrival.name,
                  code: this.arrival.code
                }).then(function (res) {
                  return res.data.status === 200 ? res.data.data : _this.handleError();
                });

              case 3:
                _context.t0 = function (item) {
                  return new Arrival(item);
                };

                return _context.abrupt("return", _context.sent.map(_context.t0));

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function to() {
        return _to.apply(this, arguments);
      }

      return to;
    }()
  }]);

  return Arrival;
}();

exports.default = Arrival;