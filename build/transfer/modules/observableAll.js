"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var instance = null;

var ObservableAll =
/*#__PURE__*/
function () {
  function ObservableAll() {
    _classCallCheck(this, ObservableAll);

    this.observers = [];

    if (instance) {
      return instance;
    }
  }

  _createClass(ObservableAll, [{
    key: "subscribe",
    value: function subscribe(fn) {
      this.observers.push(fn);
    }
  }, {
    key: "unsubscribe",
    value: function unsubscribe(fn) {
      this.observers = this.observers.filter(function (subscriber) {
        return subscriber !== fn;
      });
    }
  }, {
    key: "notify",
    value: function notify(data) {
      this.observers.forEach(function (observer) {
        return observer(data);
      });
    }
  }]);

  return ObservableAll;
}();

exports.default = ObservableAll;