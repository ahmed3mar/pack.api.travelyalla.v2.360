"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _pipe = _interopRequireDefault(require("../../common/pipe"));

var _getValueByObject = _interopRequireDefault(require("../../common/getValueByObject"));

var _observableAll = _interopRequireDefault(require("./observableAll.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var observable = new _observableAll.default();
var instance = null;

var Result =
/*#__PURE__*/
function () {
  function Result() {
    _classCallCheck(this, Result);

    this.data = [];
    this.filteredData = [];
    this.selected = null;
    this.observers = [];
    this.paginationinfo = {
      pageLimit: 15,
      pages: null,
      total: null,
      current: 1
    };
    this.filtrationInfo = {
      transferType: new Set([]),
      price: new Set([]),
      category: new Set([])
    };
    this.isCurrent = false;

    if (instance) {
      return instance;
    }
  }

  _createClass(Result, [{
    key: "sort",

    /**
     * ANCHOR Srot Function
     * @author: yas
     * @desc
     * @param {object} params
     * params content
     * ---------------------------------------------------------
     *   name          type      required   default   example
     * ---------------------------------------------------------
     * - price         {string}  no         none      'DESC' || 'ASC'
     * ---------------------------------------------------------
     * @return {object} this
     */
    value: function sort(params) {
      var _this = this;

      var keysPostion = {
        price: 'price.totalAmount'
      };
      Object.keys(params).map(function (k) {
        var type = k;
        var value = params[k];
        _this.data = _this.data.sort(function (a, b) {
          var dir = value === 'ASC' ? [a, b] : [b, a];
          return parseInt((0, _getValueByObject.default)(dir[0], keysPostion[type]), 10) - parseInt((0, _getValueByObject.default)(dir[1], keysPostion[type]), 10);
        });
        return _this;
      });
      return this;
    }
    /**
     * ANCHOR Filter Function
     * @author: yas
     * @desc
     * @param {object} params
     * params content
     * ---------------------------------------------------------
     *   name          type      required   default   example
     * ---------------------------------------------------------
     * - price         {object}  no         none      { max: 500, min: 0 }
     * ---------------------------------------------------------
     * @return {object} this
     */

  }, {
    key: "filter",
    value: function filter(params) {
      var _this2 = this;

      var keysPostion = {
        price: 'price.totalAmount',
        transferType: 'transferType',
        category: 'content.category.name'
      };
      this.data = [].concat(_toConsumableArray(this.filteredData), _toConsumableArray(this.data));
      this.filteredData = [];
      Object.keys(params).map(function (k) {
        var type = k;
        var value = params[k];
        _this2.data = _this2.data.filter(function (item) {
          var val = (0, _getValueByObject.default)(item, keysPostion[type]);

          if (_typeof(value) === 'object' && type === 'price') {
            if (Math.floor(val) <= value.max && Math.ceil(val) >= value.min) {
              return true;
            } else {
              _this2.filteredData.push(item);

              return false;
            }
          }

          if (type === 'transferType') {
            if (value.size === 0) return true;

            if (value.has(val)) {
              return true;
            } else {
              _this2.filteredData.push(item);

              return false;
            }
          }

          if (type === 'category') {
            if (value.size === 0) return true;

            if (value.has(val)) {
              return true;
            } else {
              _this2.filteredData.push(item);

              return false;
            }
          }

          return true;
        });
        return _this2;
      });
      return this;
    }
    /**
     * ANCHOR set filtration info
     * @author: yas
     * @desc
     * @param {object} params
     */

  }, {
    key: "setFiltrationInfo",
    value: function setFiltrationInfo(data) {
      var _this3 = this;

      this.filtrationInfo = {
        transferType: new Set([]),
        price: new Set([]),
        category: new Set([])
      };
      data.map(function (d) {
        if (!_this3.filtrationInfo.transferType.has(d.transferType)) {
          _this3.filtrationInfo.transferType.add(d.transferType);
        }

        if (!_this3.filtrationInfo.price.has(d.price.totalAmount)) {
          _this3.filtrationInfo.price.add(d.price.totalAmount);
        }

        if (!_this3.filtrationInfo.category.has(d.content.category.name)) {
          _this3.filtrationInfo.category.add(d.content.category.name);
        }
      });
      return this;
    }
  }, {
    key: "getFiltrationInfo",
    value: function getFiltrationInfo() {
      return this.filtrationInfo;
    }
    /**
     * ANCHOR Pagination Function
     * @author: yas
     * @desc
     * @param {object || number || string} param
     * @param {number} page
     * param status
     *  string : 'next' || 'prev'
     *  number : page number
     *  object : pagination config
     * ---------------------------------------------------------
     *   name          type      required   default   example
     * ---------------------------------------------------------
     * - limit         {number}  yes        1         10
     * ---------------------------------------------------------
     * @return {*} this || currentData
     */

  }, {
    key: "pagination",
    value: function pagination() {
      var param = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
      var page = arguments.length > 1 ? arguments[1] : undefined;
      var _this$paginationinfo = this.paginationinfo,
          pageLimit = _this$paginationinfo.pageLimit,
          current = _this$paginationinfo.current;
      var data = null;

      if (_typeof(param) === 'object') {
        var limit = param.limit;
        var total = this.data.length;
        this.paginationinfo = {
          pageLimit: limit,
          pages: Math.ceil(total / limit),
          total: total,
          current: 1
        };
        data = page ? this.pagination(page) : this;
      }

      this.paginationinfo = _objectSpread({}, this.paginationinfo, {
        total: this.data.length,
        pages: Math.ceil(this.data.length / this.paginationinfo.pageLimit)
      });

      if (typeof param === 'number') {
        this.paginationinfo = _objectSpread({}, this.paginationinfo, {
          current: param
        });

        if (param === 1) {
          data = _toConsumableArray(this.data.slice(0, pageLimit));
        }

        data = _toConsumableArray(this.data.slice((param - 1) * pageLimit, param * pageLimit));
      }

      if (param === 'next') {
        this.paginationinfo.current += 1;
        data = this.pagination(current + 1);
      }

      if (param === 'prev') {
        this.paginationinfo.current -= 1;
        data = this.pagination(current - 1);
      }

      if (param === 'info') {
        data = this.paginationinfo;
      }

      if (param !== 'info') {
        this.notify(data);
      }

      return data;
    }
    /**
     * ANCHOR Process Function
     * @author: yas
     * @desc
     * @param {array} fns
     * @return {object} this
     */

  }, {
    key: "process",
    value: function process() {
      var fns = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
      this.data = _pipe.default.apply(void 0, _toConsumableArray(fns))(this.data);
      return this;
    }
    /**
    * ANCHOR Run Function
    * @author: yas
    * @desc
    * @return {function} this.pagination
    */

  }, {
    key: "run",
    value: function run() {
      return this.pagination();
    }
    /**
    * ANCHOR subscribeAll Function
    * @author: yas
    * @desc
    * @param {function} fn
    * @return {object} this
    */

  }, {
    key: "subscribeAll",
    value: function subscribeAll(fn) {
      observable.subscribe(fn);
      return this;
    }
    /**
    * ANCHOR Subscribe Function
    * @author: yas
    * @desc
    * @param {function} fn
    * @return {object} this
    */

  }, {
    key: "subscribe",
    value: function subscribe(fn) {
      this.observers.push(fn);
      return this;
    }
    /**
    * ANCHOR Unsubscribe Function
    * @author: yas
    * @desc
    * @param {function} fn
    * @return {object} this
    */

  }, {
    key: "unsubscribe",
    value: function unsubscribe(fn) {
      this.observers = this.observers.filter(function (subscriber) {
        return subscriber !== fn;
      });
      observable.unsubscribe(fn);
      return this;
    }
  }, {
    key: "select",
    value: function select(obj) {
      this.selected = obj;
    }
    /**
    * ANCHOR notify Function
    * @author: yas
    * @desc
    * @return {object} this
    */

  }, {
    key: "notify",
    value: function notify(data) {
      this.observers.forEach(function (observer) {
        return observer(data);
      });
      observable.notify(data);
      return this;
    }
  }, {
    key: "isCurrentValue",
    set: function set(val) {
      this.isCurrent = val;
    }
  }, {
    key: "setData",
    set: function set(data) {
      this.data = data;
      this.setFiltrationInfo(data);

      if (this.isCurrent) {
        this.run();
      }
    }
  }, {
    key: "selectedTrip",
    get: function get() {
      return this.selected;
    }
  }]);

  return Result;
}();

exports.default = Result;