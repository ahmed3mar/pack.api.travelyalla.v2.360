"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var data = {
  "status": true,
  "data": {
    "departure": [{
      "direction": "DEPARTURE",
      "transferType": "PRIVATE",
      "pickupInformation": {
        "from": {
          "code": "1523",
          "description": "Visit Hotel Alexandra",
          "type": "ATLAS"
        },
        "to": {
          "code": "PMI",
          "description": "Palma Majorca, Son Sant Joan Airport",
          "type": "IATA"
        },
        "date": "2020-02-25",
        "time": "14:40:00",
        "pickup": {
          "address": "DELS PINS,15  ",
          "number": null,
          "town": "CAN PASTILLA",
          "zip": "07610",
          "description": "You will be picked up outside the hotel entrance. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694. Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.",
          "altitude": null,
          "latitude": 39.5362,
          "longitude": 2.71746,
          "checkPickup": {
            "mustCheckPickupTime": false,
            "url": null,
            "hoursBeforeConsulting": null
          },
          "pickupId": 13171,
          "stopName": "OUTSIDE",
          "image": null
        }
      },
      "content": {
        "vehicle": {
          "code": "CR",
          "name": "Car"
        },
        "category": {
          "code": "PRM",
          "name": "Premium"
        },
        "images": [{
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/small/prvt-prm-cr.png",
          "type": "SMALL"
        }, {
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/medium/prvt-prm-cr.png",
          "type": "MEDIUM"
        }, {
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/large/prvt-prm-cr.png",
          "type": "LARGE"
        }, {
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/extralarge/prvt-prm-cr.png",
          "type": "EXTRALARGE"
        }],
        "transferDetailInfo": [{
          "id": "ER",
          "name": "Exclusive ride for you",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "DTDS",
          "name": "Door to door service",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "AV247",
          "name": "Available 24/7",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "M&GS",
          "name": "Meet & Greet service",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "BAHB",
          "name": "1 item of hand baggage allowed per person",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "BA",
          "name": "1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "INTERNAT",
          "name": "International arrivals",
          "description": "International arrivals: Journeys made with the origin outside of European airspace (Schengen) and arrival into it. Passengers must pass through customs control.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "CHIN",
          "name": "CHECK INFORMATION",
          "description": "If the details do not correspond with the reservation, please contact your agency immediately.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "DRVR",
          "name": "CAN'T FIND DRIVER",
          "description": "In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "CBBS",
          "name": "CHILDBOOSTER / BABY SEAT",
          "description": "Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.",
          "type": "GENERIC_GUIDELINES"
        }],
        "customerTransferTimeInfo": [{
          "value": 30,
          "type": "CUSTOMER_MAX_WAITING_TIME",
          "metric": "minutes"
        }],
        "supplierTransferTimeInfo": [{
          "value": 15,
          "type": "SUPPLIER_MAX_WAITING_TIME_DOMESTIC",
          "metric": "minutes"
        }],
        "transferRemarks": [{
          "type": "CONTRACT",
          "description": "Pick-up point\nYou will be picked up outside the hotel entrance. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694. Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.\n\nTransfer information:\n\n* Maximum client waiting time 30 minutes\n* Maximum waiting time for drivers in domestic arrivals 15 minutes\n\n\n* International arrivals: Journeys made with the origin outside of European airspace (Schengen) and arrival into it. Passengers must pass through customs control.* If the details do not correspond with the reservation, please contact your agency immediately.* In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.* Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.\n\n* Exclusive ride for you* Door to door service* Available 24/7* Meet & Greet service* 1 item of hand baggage allowed per person* 1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm\n\n",
          "mandatory": true
        }]
      },
      "price": {
        "totalAmount": 53.25,
        "netAmount": 46.22,
        "currencyId": "EUR"
      },
      "rateKey": "DEPARTURE|ATLAS|1523|IATA|PMI|2020-02-25|14:40|2020-02-25|17:00|1~1~1|3|108483|EB-PVT-PRM-X|108483|EB-PVT-PRM-X|1|PRVT|X|CR|PRM|53.25|CPASTILLA|AEROPUERTO|13171|2246|PMI|SIMPLE|1d7bb3b14acdd3c320038a282b81ac8d",
      "cancellationPolicies": [],
      "key": "87530"
    }, {
      "direction": "DEPARTURE",
      "transferType": "PRIVATE",
      "pickupInformation": {
        "from": {
          "code": "1523",
          "description": "Visit Hotel Alexandra",
          "type": "ATLAS"
        },
        "to": {
          "code": "PMI",
          "description": "Palma Majorca, Son Sant Joan Airport",
          "type": "IATA"
        },
        "date": "2020-02-25",
        "time": "14:40:00",
        "pickup": {
          "address": "DELS PINS,15  ",
          "number": null,
          "town": "CAN PASTILLA",
          "zip": "07610",
          "description": "You will be picked up outside the hotel entrance. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694. Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.",
          "altitude": null,
          "latitude": 39.5362,
          "longitude": 2.71746,
          "checkPickup": {
            "mustCheckPickupTime": false,
            "url": null,
            "hoursBeforeConsulting": null
          },
          "pickupId": 13171,
          "stopName": "OUTSIDE",
          "image": null
        }
      },
      "content": {
        "vehicle": {
          "code": "DSBLD",
          "name": "Disabled"
        },
        "category": {
          "code": "SPCL",
          "name": "Special"
        },
        "images": [{
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/small/prvt-spcl-dsbld.png",
          "type": "SMALL"
        }, {
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/medium/prvt-spcl-dsbld.png",
          "type": "MEDIUM"
        }, {
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/large/prvt-spcl-dsbld.png",
          "type": "LARGE"
        }, {
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/extralarge/prvt-spcl-dsbld.png",
          "type": "EXTRALARGE"
        }],
        "transferDetailInfo": [{
          "id": "ER",
          "name": "Exclusive ride for you",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "DTDS",
          "name": "Door to door service",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "AV247",
          "name": "Available 24/7",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "BAHB",
          "name": "1 item of hand baggage allowed per person",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "BA",
          "name": "1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "SPLU",
          "name": "SPECIAL LUGGAGE",
          "description": "In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "DRVR",
          "name": "CAN'T FIND DRIVER",
          "description": "In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "CHIN",
          "name": "CHECK INFORMATION",
          "description": "If the details do not correspond with the reservation, please contact your agency immediately.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "CBBS",
          "name": "CHILDBOOSTER / BABY SEAT",
          "description": "Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.",
          "type": "GENERIC_GUIDELINES"
        }],
        "customerTransferTimeInfo": [{
          "value": 20,
          "type": "CUSTOMER_MAX_WAITING_TIME",
          "metric": "minutes"
        }],
        "supplierTransferTimeInfo": [{
          "value": 15,
          "type": "SUPPLIER_MAX_WAITING_TIME_DOMESTIC",
          "metric": "minutes"
        }],
        "transferRemarks": [{
          "type": "CONTRACT",
          "description": "Pick-up point\nYou will be picked up outside the hotel entrance. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694. Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.\n\nTransfer information:\n\n* Maximum client waiting time 20 minutes\n* Maximum waiting time for drivers in domestic arrivals 15 minutes\n\n\n* In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.* In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.* If the details do not correspond with the reservation, please contact your agency immediately.* Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.\n\n* Exclusive ride for you* Door to door service* Available 24/7* 1 item of hand baggage allowed per person* 1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm\n\n",
          "mandatory": true
        }]
      },
      "price": {
        "totalAmount": 46.38,
        "netAmount": 40.26,
        "currencyId": "EUR"
      },
      "rateKey": "DEPARTURE|ATLAS|1523|IATA|PMI|2020-02-25|14:40|2020-02-25|17:00|1~1~1|3|108482|EB-PVT-SPL-H|108482|EB-PVT-SPL-H|1|PRVT|H|DSBLD|SPCL|46.38|CPASTILLA|AEROPUERTO|13171|220|PMI|SIMPLE|5efccdc1203b3aa7345389fd2f3dab1e",
      "cancellationPolicies": [],
      "key": "34971"
    }, {
      "direction": "DEPARTURE",
      "transferType": "PRIVATE",
      "pickupInformation": {
        "from": {
          "code": "1523",
          "description": "Visit Hotel Alexandra",
          "type": "ATLAS"
        },
        "to": {
          "code": "PMI",
          "description": "Palma Majorca, Son Sant Joan Airport",
          "type": "IATA"
        },
        "date": "2020-02-25",
        "time": "14:40:00",
        "pickup": {
          "address": "DELS PINS,15  ",
          "number": null,
          "town": "CAN PASTILLA",
          "zip": "07610",
          "description": "You will be picked up outside the hotel entrance. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694. Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.",
          "altitude": null,
          "latitude": 39.5362,
          "longitude": 2.71746,
          "checkPickup": {
            "mustCheckPickupTime": false,
            "url": null,
            "hoursBeforeConsulting": null
          },
          "pickupId": 13171,
          "stopName": "OUTSIDE",
          "image": null
        }
      },
      "content": {
        "vehicle": {
          "code": "MVEL",
          "name": "Minivan extra luggage"
        },
        "category": {
          "code": "STND",
          "name": "Standard"
        },
        "images": [{
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/small/prvt-stnd-mvel.png",
          "type": "SMALL"
        }, {
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/medium/prvt-stnd-mvel.png",
          "type": "MEDIUM"
        }, {
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/large/prvt-stnd-mvel.png",
          "type": "LARGE"
        }, {
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/extralarge/prvt-stnd-mvel.png",
          "type": "EXTRALARGE"
        }],
        "transferDetailInfo": [{
          "id": "ER",
          "name": "Exclusive ride for you",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "DTDS",
          "name": "Door to door service",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "AV247",
          "name": "Available 24/7",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "BAHB",
          "name": "1 item of hand baggage allowed per person",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "LGPR",
          "name": "LUGGAGE PROBLEMS",
          "description": "In the event of a delay on your flight or a problem with customs or luggage,  please call the emergency number in order to advise of the delay and take the necessary steps.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "DRVR",
          "name": "CAN'T FIND DRIVER",
          "description": "In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "CHIN",
          "name": "CHECK INFORMATION",
          "description": "If the details do not correspond with the reservation, please contact your agency immediately.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "EBIKE",
          "name": "EXTRA BICYCLES",
          "description": "Bicycles need to arrive in boxes 178 x 87 x 20 cm",
          "type": "GENERIC_GUIDELINES"
        }],
        "customerTransferTimeInfo": [{
          "value": 30,
          "type": "CUSTOMER_MAX_WAITING_TIME",
          "metric": "minutes"
        }],
        "supplierTransferTimeInfo": [{
          "value": 15,
          "type": "SUPPLIER_MAX_WAITING_TIME_DOMESTIC",
          "metric": "minutes"
        }],
        "transferRemarks": [{
          "type": "CONTRACT",
          "description": "Pick-up point\nYou will be picked up outside the hotel entrance. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694. Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.\n\nTransfer information:\n\n* Maximum client waiting time 30 minutes\n* Maximum waiting time for drivers in domestic arrivals 15 minutes\n\n\n* In the event of a delay on your flight or a problem with customs or luggage,  please call the emergency number in order to advise of the delay and take the necessary steps.* In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.* If the details do not correspond with the reservation, please contact your agency immediately.* Bicycles need to arrive in boxes 178 x 87 x 20 cm\n\n* Exclusive ride for you* Door to door service* Available 24/7* 1 item of hand baggage allowed per person\n\n",
          "mandatory": true
        }]
      },
      "price": {
        "totalAmount": 48.57,
        "netAmount": 42.16,
        "currencyId": "EUR"
      },
      "rateKey": "DEPARTURE|ATLAS|1523|IATA|PMI|2020-02-25|14:40|2020-02-25|17:00|1~1~1|4|108481|TAB-PVT-MVEL-3|108481|TAB-PVT-MVEL-3|1|PRVT|3|MVEL|STND|48.57|CPASTILLA|AEROPUERTO|13171|1517|PMI|SIMPLE|edf4999c14fc2731c410abd22abb05b3",
      "cancellationPolicies": [],
      "key": "39192"
    }, {
      "direction": "DEPARTURE",
      "transferType": "PRIVATE",
      "pickupInformation": {
        "from": {
          "code": "1523",
          "description": "Visit Hotel Alexandra",
          "type": "ATLAS"
        },
        "to": {
          "code": "PMI",
          "description": "Palma Majorca, Son Sant Joan Airport",
          "type": "IATA"
        },
        "date": "2020-02-25",
        "time": "14:40:00",
        "pickup": {
          "address": "DELS PINS,15  ",
          "number": null,
          "town": "CAN PASTILLA",
          "zip": "07610",
          "description": "You will be picked up outside the hotel entrance. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694. Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.",
          "altitude": null,
          "latitude": 39.5362,
          "longitude": 2.71746,
          "checkPickup": {
            "mustCheckPickupTime": false,
            "url": null,
            "hoursBeforeConsulting": null
          },
          "pickupId": 13171,
          "stopName": "OUTSIDE",
          "image": null
        }
      },
      "content": {
        "vehicle": {
          "code": "CR",
          "name": "Car"
        },
        "category": {
          "code": "STND",
          "name": "Standard"
        },
        "images": [{
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/small/prvt-stnd-cr.png",
          "type": "SMALL"
        }, {
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/medium/prvt-stnd-cr.png",
          "type": "MEDIUM"
        }, {
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/large/prvt-stnd-cr.png",
          "type": "LARGE"
        }, {
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/extralarge/prvt-stnd-cr.png",
          "type": "EXTRALARGE"
        }],
        "transferDetailInfo": [{
          "id": "ER",
          "name": "Exclusive ride for you",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "DTDS",
          "name": "Door to door service",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "AV247",
          "name": "Available 24/7",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "BA",
          "name": "1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "BAHB",
          "name": "1 item of hand baggage allowed per person",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "SPLU",
          "name": "SPECIAL LUGGAGE",
          "description": "In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "DRVR",
          "name": "CAN'T FIND DRIVER",
          "description": "In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "CHIN",
          "name": "CHECK INFORMATION",
          "description": "If the details do not correspond with the reservation, please contact your agency immediately.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "CBBS",
          "name": "CHILDBOOSTER / BABY SEAT",
          "description": "Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.",
          "type": "GENERIC_GUIDELINES"
        }],
        "customerTransferTimeInfo": [{
          "value": 30,
          "type": "CUSTOMER_MAX_WAITING_TIME",
          "metric": "minutes"
        }],
        "supplierTransferTimeInfo": [{
          "value": 15,
          "type": "SUPPLIER_MAX_WAITING_TIME_DOMESTIC",
          "metric": "minutes"
        }],
        "transferRemarks": [{
          "type": "CONTRACT",
          "description": "Pick-up point\nYou will be picked up outside the hotel entrance. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694. Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.\n\nTransfer information:\n\n* Maximum client waiting time 30 minutes\n* Maximum waiting time for drivers in domestic arrivals 15 minutes\n\n\n* In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.* In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.* If the details do not correspond with the reservation, please contact your agency immediately.* Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.\n\n* Exclusive ride for you* Door to door service* Available 24/7* 1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm* 1 item of hand baggage allowed per person\n\n",
          "mandatory": true
        }]
      },
      "price": {
        "totalAmount": 26.86,
        "netAmount": 23.31,
        "currencyId": "EUR"
      },
      "rateKey": "DEPARTURE|ATLAS|1523|IATA|PMI|2020-02-25|14:40|2020-02-25|17:00|1~1~1|4|108478|EB-PVT-STD-C|108478|EB-PVT-STD-C|1|PRVT|C|CR|STND|26.86|CPASTILLA|AEROPUERTO|13171|2211|PMI|SIMPLE|9badf645229b24b8d25d6a36f3698622",
      "cancellationPolicies": [],
      "key": "4143"
    }, {
      "direction": "DEPARTURE",
      "transferType": "PRIVATE",
      "pickupInformation": {
        "from": {
          "code": "1523",
          "description": "Visit Hotel Alexandra",
          "type": "ATLAS"
        },
        "to": {
          "code": "PMI",
          "description": "Palma Majorca, Son Sant Joan Airport",
          "type": "IATA"
        },
        "date": "2020-02-25",
        "time": "14:40:00",
        "pickup": {
          "address": "DELS PINS,15  ",
          "number": null,
          "town": "CAN PASTILLA",
          "zip": "07610",
          "description": "You will be picked up outside the hotel entrance. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694. Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.",
          "altitude": null,
          "latitude": 39.5362,
          "longitude": 2.71746,
          "checkPickup": {
            "mustCheckPickupTime": false,
            "url": null,
            "hoursBeforeConsulting": null
          },
          "pickupId": 13171,
          "stopName": "OUTSIDE",
          "image": null
        }
      },
      "content": {
        "vehicle": {
          "code": "CR",
          "name": "Car"
        },
        "category": {
          "code": "ECO",
          "name": "Economy"
        },
        "images": [{
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/small/prvt-eco-cr.png",
          "type": "SMALL"
        }, {
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/medium/prvt-eco-cr.png",
          "type": "MEDIUM"
        }, {
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/large/prvt-eco-cr.png",
          "type": "LARGE"
        }, {
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/extralarge/prvt-eco-cr.png",
          "type": "EXTRALARGE"
        }],
        "transferDetailInfo": [{
          "id": "ER",
          "name": "Exclusive ride for you",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "DTDS",
          "name": "Door to door service",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "AV247",
          "name": "Available 24/7",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "BA",
          "name": "1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "VOUC",
          "name": "VOUCHER ",
          "description": "Remember to bring a printed copy of this voucher and a valid photo ID with you.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "CHIN",
          "name": "CHECK INFORMATION",
          "description": "If the details do not correspond with the reservation, please contact your agency immediately.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "CBBS",
          "name": "CHILDBOOSTER / BABY SEAT",
          "description": "Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "DRVR",
          "name": "CAN'T FIND DRIVER",
          "description": "In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.",
          "type": "GENERIC_GUIDELINES"
        }],
        "customerTransferTimeInfo": [{
          "value": 30,
          "type": "CUSTOMER_MAX_WAITING_TIME",
          "metric": "minutes"
        }],
        "supplierTransferTimeInfo": [{
          "value": 15,
          "type": "SUPPLIER_MAX_WAITING_TIME_DOMESTIC",
          "metric": "minutes"
        }],
        "transferRemarks": [{
          "type": "CONTRACT",
          "description": "Pick-up point\nYou will be picked up outside the hotel entrance. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694. Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.\n\nTransfer information:\n\n* Maximum client waiting time 30 minutes\n* Maximum waiting time for drivers in domestic arrivals 15 minutes\n\n\n* Remember to bring a printed copy of this voucher and a valid photo ID with you.* If the details do not correspond with the reservation, please contact your agency immediately.* Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.* In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.\n\n* Exclusive ride for you* Door to door service* Available 24/7* 1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm\n\n",
          "mandatory": true
        }]
      },
      "price": {
        "totalAmount": 25.22,
        "netAmount": 21.89,
        "currencyId": "EUR"
      },
      "rateKey": "DEPARTURE|ATLAS|1523|IATA|PMI|2020-02-25|14:40|2020-02-25|17:00|1~1~1|4|108477|EB-PVT-ECO-5|108477|EB-PVT-ECO-5|1|PRVT|5|CR|ECO|25.22|CPASTILLA|AEROPUERTO|13171|2248|PMI|SIMPLE|779e28ca88411e7bce06828ba96b741d",
      "cancellationPolicies": [],
      "key": "6214"
    }],
    "return": [{
      "direction": "RETURN",
      "transferType": "PRIVATE",
      "pickupInformation": {
        "from": {
          "code": "PMI",
          "description": "Palma Majorca, Son Sant Joan Airport",
          "type": "IATA"
        },
        "to": {
          "code": "1523",
          "description": "Visit Hotel Alexandra",
          "type": "ATLAS"
        },
        "date": "2020-02-26",
        "time": "22:00:00",
        "pickup": {
          "address": null,
          "number": null,
          "town": null,
          "zip": null,
          "description": "Once you have collected your luggage, leave the baggage area through door B, turn right to proceed to the Destination Services airport Desk which is the last desk in the arrival area, located in front of column number 1.  Here you will be attendend and escorted to the transfer vehicle.-If you are unable to locate the driver/agent, please call DESTINATION SERVICES on +34 971 922 694.Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.",
          "altitude": null,
          "latitude": 39.547654,
          "longitude": 2.730388,
          "checkPickup": {
            "mustCheckPickupTime": false,
            "url": null,
            "hoursBeforeConsulting": null
          },
          "pickupId": 3941,
          "stopName": "PMI OFICINA AEROPUERTO",
          "image": "http://media.activitiesbank.com/pickup/maps/all.jpg"
        }
      },
      "content": {
        "vehicle": {
          "code": "CR",
          "name": "Car"
        },
        "category": {
          "code": "PRM",
          "name": "Premium"
        },
        "images": [{
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/small/prvt-prm-cr.png",
          "type": "SMALL"
        }, {
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/medium/prvt-prm-cr.png",
          "type": "MEDIUM"
        }, {
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/large/prvt-prm-cr.png",
          "type": "LARGE"
        }, {
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/extralarge/prvt-prm-cr.png",
          "type": "EXTRALARGE"
        }],
        "transferDetailInfo": [{
          "id": "ER",
          "name": "Exclusive ride for you",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "DTDS",
          "name": "Door to door service",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "AV247",
          "name": "Available 24/7",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "M&GS",
          "name": "Meet & Greet service",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "BAHB",
          "name": "1 item of hand baggage allowed per person",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "BA",
          "name": "1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "DOMEST",
          "name": "Domestic arrivals",
          "description": "Domestic arrivals: Journeys made within the borders of the same country or European airspace (Schengen) or from Schengen space to any of these countries: Bulgaria, Cyprus, Croatia, Ireland, United Kingdom and Romania. Passengers do not have to go through customs controll",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "INTERNAT",
          "name": "International arrivals",
          "description": "International arrivals: Journeys made with the origin outside of European airspace (Schengen) and arrival into it. Passengers must pass through customs control.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "DRVR",
          "name": "CAN'T FIND DRIVER",
          "description": "In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "CBBS",
          "name": "CHILDBOOSTER / BABY SEAT",
          "description": "Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "CHIN",
          "name": "CHECK INFORMATION",
          "description": "If the details do not correspond with the reservation, please contact your agency immediately.",
          "type": "GENERIC_GUIDELINES"
        }],
        "customerTransferTimeInfo": [{
          "value": 0,
          "type": "CUSTOMER_MAX_WAITING_TIME",
          "metric": "minutes"
        }],
        "supplierTransferTimeInfo": [{
          "value": 60,
          "type": "SUPPLIER_MAX_WAITING_TIME_DOMESTIC",
          "metric": "minutes"
        }, {
          "value": 90,
          "type": "SUPPLIER_MAX_WAITING_TIME_INTERNATIONAL",
          "metric": "minutes"
        }],
        "transferRemarks": [{
          "type": "CONTRACT",
          "description": "Pick-up point\nOnce you have collected your luggage, leave the baggage area through door B, turn right to proceed to the Destination Services airport Desk which is the last desk in the arrival area, located in front of column number 1.  Here you will be attendend and escorted to the transfer vehicle.-If you are unable to locate the driver/agent, please call DESTINATION SERVICES on +34 971 922 694.Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.\n\nTransfer information:\n\n* Maximum client waiting time 0 minutes\n* Maximum waiting time for drivers in domestic arrivals 60 minutes\n* Maximum waiting time for drivers in international arrivals 90 minutes\n\n\n* Domestic arrivals: Journeys made within the borders of the same country or European airspace (Schengen) or from Schengen space to any of these countries: Bulgaria, Cyprus, Croatia, Ireland, United Kingdom and Romania. Passengers do not have to go through customs controll* International arrivals: Journeys made with the origin outside of European airspace (Schengen) and arrival into it. Passengers must pass through customs control.* In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.* Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.* If the details do not correspond with the reservation, please contact your agency immediately.\n\n* Exclusive ride for you* Door to door service* Available 24/7* Meet & Greet service* 1 item of hand baggage allowed per person* 1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm\n\n",
          "mandatory": true
        }]
      },
      "price": {
        "totalAmount": 53.25,
        "netAmount": 46.22,
        "currencyId": "EUR"
      },
      "rateKey": "ARRIVAL|IATA|PMI|ATLAS|1523|2020-02-26|22:00|2020-02-26|22:00|1~1~1|3|108483|EB-PVT-PRM-X|108483|EB-PVT-PRM-X|1|PRVT|X|CR|PRM|53.25|AEROPUERTO|CPASTILLA|3941|2246|PMI|SIMPLE|f4557b273c87bddd2b2c2ee797b9c8c3",
      "cancellationPolicies": [],
      "key": "38955"
    }, {
      "direction": "RETURN",
      "transferType": "PRIVATE",
      "pickupInformation": {
        "from": {
          "code": "PMI",
          "description": "Palma Majorca, Son Sant Joan Airport",
          "type": "IATA"
        },
        "to": {
          "code": "1523",
          "description": "Visit Hotel Alexandra",
          "type": "ATLAS"
        },
        "date": "2020-02-26",
        "time": "22:00:00",
        "pickup": {
          "address": null,
          "number": null,
          "town": null,
          "zip": null,
          "description": "Once you have collected your luggage, leave the baggage area through door B, turn right to proceed to the Destination Services airport Desk which is the last desk in the arrival area, located in front of column number 1.  Here you will be attendend and escorted to the transfer vehicle.-If you are unable to locate the driver/agent, please call DESTINATION SERVICES on +34 971 922 694.Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.",
          "altitude": null,
          "latitude": 39.547654,
          "longitude": 2.730388,
          "checkPickup": {
            "mustCheckPickupTime": false,
            "url": null,
            "hoursBeforeConsulting": null
          },
          "pickupId": 3941,
          "stopName": "PMI OFICINA AEROPUERTO",
          "image": "http://media.activitiesbank.com/pickup/maps/all.jpg"
        }
      },
      "content": {
        "vehicle": {
          "code": "DSBLD",
          "name": "Disabled"
        },
        "category": {
          "code": "SPCL",
          "name": "Special"
        },
        "images": [{
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/small/prvt-spcl-dsbld.png",
          "type": "SMALL"
        }, {
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/medium/prvt-spcl-dsbld.png",
          "type": "MEDIUM"
        }, {
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/large/prvt-spcl-dsbld.png",
          "type": "LARGE"
        }, {
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/extralarge/prvt-spcl-dsbld.png",
          "type": "EXTRALARGE"
        }],
        "transferDetailInfo": [{
          "id": "ER",
          "name": "Exclusive ride for you",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "DTDS",
          "name": "Door to door service",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "AV247",
          "name": "Available 24/7",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "BAHB",
          "name": "1 item of hand baggage allowed per person",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "BA",
          "name": "1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "SPLU",
          "name": "SPECIAL LUGGAGE",
          "description": "In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "LGPR",
          "name": "LUGGAGE PROBLEMS",
          "description": "In the event of a delay on your flight or a problem with customs or luggage,  please call the emergency number in order to advise of the delay and take the necessary steps.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "DRVR",
          "name": "CAN'T FIND DRIVER",
          "description": "In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "CHIN",
          "name": "CHECK INFORMATION",
          "description": "If the details do not correspond with the reservation, please contact your agency immediately.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "CBBS",
          "name": "CHILDBOOSTER / BABY SEAT",
          "description": "Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "INTERNAT",
          "name": "International arrivals",
          "description": "International arrivals: Journeys made with the origin outside of European airspace (Schengen) and arrival into it. Passengers must pass through customs control.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "DOMEST",
          "name": "Domestic arrivals",
          "description": "Domestic arrivals: Journeys made within the borders of the same country or European airspace (Schengen) or from Schengen space to any of these countries: Bulgaria, Cyprus, Croatia, Ireland, United Kingdom and Romania. Passengers do not have to go through customs controll",
          "type": "GENERIC_GUIDELINES"
        }],
        "customerTransferTimeInfo": [{
          "value": 0,
          "type": "CUSTOMER_MAX_WAITING_TIME",
          "metric": "minutes"
        }],
        "supplierTransferTimeInfo": [{
          "value": 60,
          "type": "SUPPLIER_MAX_WAITING_TIME_DOMESTIC",
          "metric": "minutes"
        }, {
          "value": 90,
          "type": "SUPPLIER_MAX_WAITING_TIME_INTERNATIONAL",
          "metric": "minutes"
        }],
        "transferRemarks": [{
          "type": "CONTRACT",
          "description": "Pick-up point\nOnce you have collected your luggage, leave the baggage area through door B, turn right to proceed to the Destination Services airport Desk which is the last desk in the arrival area, located in front of column number 1.  Here you will be attendend and escorted to the transfer vehicle.-If you are unable to locate the driver/agent, please call DESTINATION SERVICES on +34 971 922 694.Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.\n\nTransfer information:\n\n* Maximum client waiting time 0 minutes\n* Maximum waiting time for drivers in domestic arrivals 60 minutes\n* Maximum waiting time for drivers in international arrivals 90 minutes\n\n\n* In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.* In the event of a delay on your flight or a problem with customs or luggage,  please call the emergency number in order to advise of the delay and take the necessary steps.* In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.* If the details do not correspond with the reservation, please contact your agency immediately.* Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.* International arrivals: Journeys made with the origin outside of European airspace (Schengen) and arrival into it. Passengers must pass through customs control.* Domestic arrivals: Journeys made within the borders of the same country or European airspace (Schengen) or from Schengen space to any of these countries: Bulgaria, Cyprus, Croatia, Ireland, United Kingdom and Romania. Passengers do not have to go through customs controll\n\n* Exclusive ride for you* Door to door service* Available 24/7* 1 item of hand baggage allowed per person* 1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm\n\n",
          "mandatory": true
        }]
      },
      "price": {
        "totalAmount": 46.38,
        "netAmount": 40.26,
        "currencyId": "EUR"
      },
      "rateKey": "ARRIVAL|IATA|PMI|ATLAS|1523|2020-02-26|22:00|2020-02-26|22:00|1~1~1|3|108482|EB-PVT-SPL-H|108482|EB-PVT-SPL-H|1|PRVT|H|DSBLD|SPCL|46.38|AEROPUERTO|CPASTILLA|3941|220|PMI|SIMPLE|9bd6281a41cbc1207781aa568d3bb9c9",
      "cancellationPolicies": [],
      "key": "17416"
    }, {
      "direction": "RETURN",
      "transferType": "PRIVATE",
      "pickupInformation": {
        "from": {
          "code": "PMI",
          "description": "Palma Majorca, Son Sant Joan Airport",
          "type": "IATA"
        },
        "to": {
          "code": "1523",
          "description": "Visit Hotel Alexandra",
          "type": "ATLAS"
        },
        "date": "2020-02-26",
        "time": "22:00:00",
        "pickup": {
          "address": null,
          "number": null,
          "town": null,
          "zip": null,
          "description": "Once you have collected your luggage, leave the baggage area through door B, turn right to proceed to the Destination Services airport Desk which is the last desk in the arrival area, located in front of column number 1.  Here you will be attendend and escorted to the transfer vehicle.-If you are unable to locate the driver/agent, please call DESTINATION SERVICES on +34 971 922 694.Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.",
          "altitude": null,
          "latitude": 39.547654,
          "longitude": 2.730388,
          "checkPickup": {
            "mustCheckPickupTime": false,
            "url": null,
            "hoursBeforeConsulting": null
          },
          "pickupId": 3941,
          "stopName": "PMI OFICINA AEROPUERTO",
          "image": "http://media.activitiesbank.com/pickup/maps/all.jpg"
        }
      },
      "content": {
        "vehicle": {
          "code": "MVEL",
          "name": "Minivan extra luggage"
        },
        "category": {
          "code": "STND",
          "name": "Standard"
        },
        "images": [{
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/small/prvt-stnd-mvel.png",
          "type": "SMALL"
        }, {
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/medium/prvt-stnd-mvel.png",
          "type": "MEDIUM"
        }, {
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/large/prvt-stnd-mvel.png",
          "type": "LARGE"
        }, {
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/extralarge/prvt-stnd-mvel.png",
          "type": "EXTRALARGE"
        }],
        "transferDetailInfo": [{
          "id": "ER",
          "name": "Exclusive ride for you",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "DTDS",
          "name": "Door to door service",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "AV247",
          "name": "Available 24/7",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "BAHB",
          "name": "1 item of hand baggage allowed per person",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "LGPR",
          "name": "LUGGAGE PROBLEMS",
          "description": "In the event of a delay on your flight or a problem with customs or luggage,  please call the emergency number in order to advise of the delay and take the necessary steps.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "DRVR",
          "name": "CAN'T FIND DRIVER",
          "description": "In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "CHIN",
          "name": "CHECK INFORMATION",
          "description": "If the details do not correspond with the reservation, please contact your agency immediately.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "EBIKE",
          "name": "EXTRA BICYCLES",
          "description": "Bicycles need to arrive in boxes 178 x 87 x 20 cm",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "DOMEST",
          "name": "Domestic arrivals",
          "description": "Domestic arrivals: Journeys made within the borders of the same country or European airspace (Schengen) or from Schengen space to any of these countries: Bulgaria, Cyprus, Croatia, Ireland, United Kingdom and Romania. Passengers do not have to go through customs controll",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "INTERNAT",
          "name": "International arrivals",
          "description": "International arrivals: Journeys made with the origin outside of European airspace (Schengen) and arrival into it. Passengers must pass through customs control.",
          "type": "GENERIC_GUIDELINES"
        }],
        "customerTransferTimeInfo": [{
          "value": 0,
          "type": "CUSTOMER_MAX_WAITING_TIME",
          "metric": "minutes"
        }],
        "supplierTransferTimeInfo": [{
          "value": 60,
          "type": "SUPPLIER_MAX_WAITING_TIME_DOMESTIC",
          "metric": "minutes"
        }, {
          "value": 90,
          "type": "SUPPLIER_MAX_WAITING_TIME_INTERNATIONAL",
          "metric": "minutes"
        }],
        "transferRemarks": [{
          "type": "CONTRACT",
          "description": "Pick-up point\nOnce you have collected your luggage, leave the baggage area through door B, turn right to proceed to the Destination Services airport Desk which is the last desk in the arrival area, located in front of column number 1.  Here you will be attendend and escorted to the transfer vehicle.-If you are unable to locate the driver/agent, please call DESTINATION SERVICES on +34 971 922 694.Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.\n\nTransfer information:\n\n* Maximum client waiting time 0 minutes\n* Maximum waiting time for drivers in domestic arrivals 60 minutes\n* Maximum waiting time for drivers in international arrivals 90 minutes\n\n\n* In the event of a delay on your flight or a problem with customs or luggage,  please call the emergency number in order to advise of the delay and take the necessary steps.* In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.* If the details do not correspond with the reservation, please contact your agency immediately.* Bicycles need to arrive in boxes 178 x 87 x 20 cm* Domestic arrivals: Journeys made within the borders of the same country or European airspace (Schengen) or from Schengen space to any of these countries: Bulgaria, Cyprus, Croatia, Ireland, United Kingdom and Romania. Passengers do not have to go through customs controll* International arrivals: Journeys made with the origin outside of European airspace (Schengen) and arrival into it. Passengers must pass through customs control.\n\n* Exclusive ride for you* Door to door service* Available 24/7* 1 item of hand baggage allowed per person\n\n",
          "mandatory": true
        }]
      },
      "price": {
        "totalAmount": 48.57,
        "netAmount": 42.16,
        "currencyId": "EUR"
      },
      "rateKey": "ARRIVAL|IATA|PMI|ATLAS|1523|2020-02-26|22:00|2020-02-26|22:00|1~1~1|4|108481|TAB-PVT-MVEL-3|108481|TAB-PVT-MVEL-3|1|PRVT|3|MVEL|STND|48.57|AEROPUERTO|CPASTILLA|3941|1517|PMI|SIMPLE|d90e7ba7ab252017e2164ecec373b10b",
      "cancellationPolicies": [],
      "key": "45467"
    }, {
      "direction": "RETURN",
      "transferType": "PRIVATE",
      "pickupInformation": {
        "from": {
          "code": "PMI",
          "description": "Palma Majorca, Son Sant Joan Airport",
          "type": "IATA"
        },
        "to": {
          "code": "1523",
          "description": "Visit Hotel Alexandra",
          "type": "ATLAS"
        },
        "date": "2020-02-26",
        "time": "22:00:00",
        "pickup": {
          "address": null,
          "number": null,
          "town": null,
          "zip": null,
          "description": "Once you have collected your luggage, leave the baggage area through door B, turn right to proceed to the Destination Services airport Desk which is the last desk in the arrival area, located in front of column number 1.  Here you will be attendend and escorted to the transfer vehicle.-If you are unable to locate the driver/agent, please call DESTINATION SERVICES on +34 971 922 694.Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.",
          "altitude": null,
          "latitude": 39.547654,
          "longitude": 2.730388,
          "checkPickup": {
            "mustCheckPickupTime": false,
            "url": null,
            "hoursBeforeConsulting": null
          },
          "pickupId": 3941,
          "stopName": "PMI OFICINA AEROPUERTO",
          "image": "http://media.activitiesbank.com/pickup/maps/all.jpg"
        }
      },
      "content": {
        "vehicle": {
          "code": "CR",
          "name": "Car"
        },
        "category": {
          "code": "STND",
          "name": "Standard"
        },
        "images": [{
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/small/prvt-stnd-cr.png",
          "type": "SMALL"
        }, {
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/medium/prvt-stnd-cr.png",
          "type": "MEDIUM"
        }, {
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/large/prvt-stnd-cr.png",
          "type": "LARGE"
        }, {
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/extralarge/prvt-stnd-cr.png",
          "type": "EXTRALARGE"
        }],
        "transferDetailInfo": [{
          "id": "ER",
          "name": "Exclusive ride for you",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "DTDS",
          "name": "Door to door service",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "AV247",
          "name": "Available 24/7",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "BA",
          "name": "1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "BAHB",
          "name": "1 item of hand baggage allowed per person",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "SPLU",
          "name": "SPECIAL LUGGAGE",
          "description": "In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "DRVR",
          "name": "CAN'T FIND DRIVER",
          "description": "In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "CHIN",
          "name": "CHECK INFORMATION",
          "description": "If the details do not correspond with the reservation, please contact your agency immediately.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "CBBS",
          "name": "CHILDBOOSTER / BABY SEAT",
          "description": "Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "LGPR",
          "name": "LUGGAGE PROBLEMS",
          "description": "In the event of a delay on your flight or a problem with customs or luggage,  please call the emergency number in order to advise of the delay and take the necessary steps.",
          "type": "GENERIC_GUIDELINES"
        }],
        "customerTransferTimeInfo": [{
          "value": 0,
          "type": "CUSTOMER_MAX_WAITING_TIME",
          "metric": "minutes"
        }],
        "supplierTransferTimeInfo": [{
          "value": 60,
          "type": "SUPPLIER_MAX_WAITING_TIME_DOMESTIC",
          "metric": "minutes"
        }, {
          "value": 90,
          "type": "SUPPLIER_MAX_WAITING_TIME_INTERNATIONAL",
          "metric": "minutes"
        }],
        "transferRemarks": [{
          "type": "CONTRACT",
          "description": "Pick-up point\nOnce you have collected your luggage, leave the baggage area through door B, turn right to proceed to the Destination Services airport Desk which is the last desk in the arrival area, located in front of column number 1.  Here you will be attendend and escorted to the transfer vehicle.-If you are unable to locate the driver/agent, please call DESTINATION SERVICES on +34 971 922 694.Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.\n\nTransfer information:\n\n* Maximum client waiting time 0 minutes\n* Maximum waiting time for drivers in domestic arrivals 60 minutes\n* Maximum waiting time for drivers in international arrivals 90 minutes\n\n\n* In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.* In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.* If the details do not correspond with the reservation, please contact your agency immediately.* Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.* In the event of a delay on your flight or a problem with customs or luggage,  please call the emergency number in order to advise of the delay and take the necessary steps.\n\n* Exclusive ride for you* Door to door service* Available 24/7* 1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm* 1 item of hand baggage allowed per person\n\n",
          "mandatory": true
        }]
      },
      "price": {
        "totalAmount": 26.86,
        "netAmount": 23.31,
        "currencyId": "EUR"
      },
      "rateKey": "ARRIVAL|IATA|PMI|ATLAS|1523|2020-02-26|22:00|2020-02-26|22:00|1~1~1|4|108478|EB-PVT-STD-C|108478|EB-PVT-STD-C|1|PRVT|C|CR|STND|26.86|AEROPUERTO|CPASTILLA|3941|2211|PMI|SIMPLE|72482623ea772ca843330d3d29daa8af",
      "cancellationPolicies": [],
      "key": "27368"
    }, {
      "direction": "RETURN",
      "transferType": "PRIVATE",
      "pickupInformation": {
        "from": {
          "code": "PMI",
          "description": "Palma Majorca, Son Sant Joan Airport",
          "type": "IATA"
        },
        "to": {
          "code": "1523",
          "description": "Visit Hotel Alexandra",
          "type": "ATLAS"
        },
        "date": "2020-02-26",
        "time": "22:00:00",
        "pickup": {
          "address": null,
          "number": null,
          "town": null,
          "zip": null,
          "description": "Once you have collected your luggage, leave the baggage area through door B, turn right to proceed to the Destination Services airport Desk which is the last desk in the arrival area, located in front of column number 1.  Here you will be attendend and escorted to the transfer vehicle.-If you are unable to locate the driver/agent, please call DESTINATION SERVICES on +34 971 922 694.Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.",
          "altitude": null,
          "latitude": 39.547654,
          "longitude": 2.730388,
          "checkPickup": {
            "mustCheckPickupTime": false,
            "url": null,
            "hoursBeforeConsulting": null
          },
          "pickupId": 3941,
          "stopName": "PMI OFICINA AEROPUERTO",
          "image": "http://media.activitiesbank.com/pickup/maps/all.jpg"
        }
      },
      "content": {
        "vehicle": {
          "code": "CR",
          "name": "Car"
        },
        "category": {
          "code": "ECO",
          "name": "Economy"
        },
        "images": [{
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/small/prvt-eco-cr.png",
          "type": "SMALL"
        }, {
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/medium/prvt-eco-cr.png",
          "type": "MEDIUM"
        }, {
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/large/prvt-eco-cr.png",
          "type": "LARGE"
        }, {
          "url": "http://media.stage.activitiesbank.com/giata/transfers/TRD/extralarge/prvt-eco-cr.png",
          "type": "EXTRALARGE"
        }],
        "transferDetailInfo": [{
          "id": "ER",
          "name": "Exclusive ride for you",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "DTDS",
          "name": "Door to door service",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "AV247",
          "name": "Available 24/7",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "BA",
          "name": "1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm",
          "description": null,
          "type": "GENERAL_INFO"
        }, {
          "id": "INTERNAT",
          "name": "International arrivals",
          "description": "International arrivals: Journeys made with the origin outside of European airspace (Schengen) and arrival into it. Passengers must pass through customs control.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "VOUC",
          "name": "VOUCHER ",
          "description": "Remember to bring a printed copy of this voucher and a valid photo ID with you.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "CHIN",
          "name": "CHECK INFORMATION",
          "description": "If the details do not correspond with the reservation, please contact your agency immediately.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "DOMEST",
          "name": "Domestic arrivals",
          "description": "Domestic arrivals: Journeys made within the borders of the same country or European airspace (Schengen) or from Schengen space to any of these countries: Bulgaria, Cyprus, Croatia, Ireland, United Kingdom and Romania. Passengers do not have to go through customs controll",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "CBBS",
          "name": "CHILDBOOSTER / BABY SEAT",
          "description": "Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.",
          "type": "GENERIC_GUIDELINES"
        }, {
          "id": "DRVR",
          "name": "CAN'T FIND DRIVER",
          "description": "In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.",
          "type": "GENERIC_GUIDELINES"
        }],
        "customerTransferTimeInfo": [{
          "value": 0,
          "type": "CUSTOMER_MAX_WAITING_TIME",
          "metric": "minutes"
        }],
        "supplierTransferTimeInfo": [{
          "value": 60,
          "type": "SUPPLIER_MAX_WAITING_TIME_DOMESTIC",
          "metric": "minutes"
        }, {
          "value": 60,
          "type": "SUPPLIER_MAX_WAITING_TIME_INTERNATIONAL",
          "metric": "minutes"
        }],
        "transferRemarks": [{
          "type": "CONTRACT",
          "description": "Pick-up point\nOnce you have collected your luggage, leave the baggage area through door B, turn right to proceed to the Destination Services airport Desk which is the last desk in the arrival area, located in front of column number 1.  Here you will be attendend and escorted to the transfer vehicle.-If you are unable to locate the driver/agent, please call DESTINATION SERVICES on +34 971 922 694.Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.\n\nTransfer information:\n\n* Maximum client waiting time 0 minutes\n* Maximum waiting time for drivers in domestic arrivals 60 minutes\n* Maximum waiting time for drivers in international arrivals 60 minutes\n\n\n* International arrivals: Journeys made with the origin outside of European airspace (Schengen) and arrival into it. Passengers must pass through customs control.* Remember to bring a printed copy of this voucher and a valid photo ID with you.* If the details do not correspond with the reservation, please contact your agency immediately.* Domestic arrivals: Journeys made within the borders of the same country or European airspace (Schengen) or from Schengen space to any of these countries: Bulgaria, Cyprus, Croatia, Ireland, United Kingdom and Romania. Passengers do not have to go through customs controll* Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.* In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.\n\n* Exclusive ride for you* Door to door service* Available 24/7* 1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm\n\n",
          "mandatory": true
        }]
      },
      "price": {
        "totalAmount": 25.22,
        "netAmount": 21.89,
        "currencyId": "EUR"
      },
      "rateKey": "ARRIVAL|IATA|PMI|ATLAS|1523|2020-02-26|22:00|2020-02-26|22:00|1~1~1|4|108477|EB-PVT-ECO-5|108477|EB-PVT-ECO-5|1|PRVT|5|CR|ECO|25.22|AEROPUERTO|CPASTILLA|3941|2248|PMI|SIMPLE|15655f7c10bd44eb3cae1e981f708c9f",
      "cancellationPolicies": [],
      "key": "55249"
    }]
  }
};
var _default = data;
exports.default = _default;