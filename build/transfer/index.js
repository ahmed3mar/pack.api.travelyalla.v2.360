"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _api = _interopRequireDefault(require("../api"));

var _arrival = _interopRequireDefault(require("./modules/arrival"));

var _result = _interopRequireDefault(require("./modules/result"));

var _transfer = _interopRequireDefault(require("../config/transfer"));

var _parseUrl = _interopRequireDefault(require("./modules/parseUrl"));

var _testData = _interopRequireDefault(require("./testData.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var instance = null;

var Transfer =
/*#__PURE__*/
function (_Api) {
  _inherits(Transfer, _Api);

  function Transfer(props) {
    var _this;

    _classCallCheck(this, Transfer);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Transfer).call(this, _objectSpread({}, _transfer.default, {}, props)));

    if (instance) {
      return _possibleConstructorReturn(_this, instance);
    }

    _this.parseUrl = new _parseUrl.default();
    _this.fromInstance = new _result.default();
    _this.toInstance = new _result.default();
    _this.current = 'from';
    _this.paginationinfo = {
      pageLimit: 10,
      pages: null,
      total: null,
      current: 1
    };
    _this.arrivals = _this.arrivals.bind(_assertThisInitialized(_this));
    _this.search = _this.search.bind(_assertThisInitialized(_this));
    _this.book = _this.book.bind(_assertThisInitialized(_this));
    instance = _assertThisInitialized(_this);
    return _this;
  }
  /**
   * ANCHOR Arrivals Function
   * @author: yas
   * @desc get arrivals data
   * @param {string} key
   * @return {array} Arrivals Array
   */


  _createClass(Transfer, [{
    key: "arrivals",
    value: function () {
      var _arrivals = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee(param) {
        var _this2 = this;

        var route;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                route = this.route('arrivals');
                _context.next = 3;
                return this.get(route, param).then(function (res) {
                  return res.data.status === 200 ? res.data.data : _this2.handleError(res);
                });

              case 3:
                _context.t0 = function (item) {
                  return new _arrival.default(item);
                };

                return _context.abrupt("return", _context.sent.map(_context.t0));

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function arrivals(_x) {
        return _arrivals.apply(this, arguments);
      }

      return arrivals;
    }()
    /**
     * ANCHOR Search Function
     * @author: yas
     * @desc
     * @param {object} params
     * params content
     * ---------------------------------------------------------
     *   name          type      required   default   example
     * ---------------------------------------------------------
     * - from          {string}  yes        none     'ATLAS'
     * - from_code     {string}  yes        none     '1523'
     * - to            {string}  yes        none     'IATA'
     * - to_code       {string}  yes        none     'PMI'
     * - outbound      {string}  yes        none     '2019-10-25'
     * - outboundTime  {string}  yes        none     '5:00:00'
     * - inbound       {string}  no         none     '2019-10-26'
     * - inboundTime   {string}  no         none     '10:00:00'
     * - adults        {number}  yes        none     10
     * - children      {number}  yes        none     1
     * - infants       {number}  yes        none     1
     * ---------------------------------------------------------
     * @return {object} original data
     */

  }, {
    key: "search",
    value: function () {
      var _search = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee2(params) {
        var route, res, data;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                route = this.route('search');
                _context2.next = 4;
                return this.post(route, params);

              case 4:
                res = _context2.sent;

                if (!(res.status === 200)) {
                  _context2.next = 10;
                  break;
                }

                data = res.status ? res.data.data : this.handleError();
                this.fromInstance.setData = data.departure;
                this.toInstance.setData = data.return;
                return _context2.abrupt("return", {
                  from: this.fromInstance,
                  to: this.toInstance
                });

              case 10:
                _context2.next = 15;
                break;

              case 12:
                _context2.prev = 12;
                _context2.t0 = _context2["catch"](0);
                return _context2.abrupt("return", _context2.t0);

              case 15:
                return _context2.abrupt("return", this);

              case 16:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[0, 12]]);
      }));

      function search(_x2) {
        return _search.apply(this, arguments);
      }

      return search;
    }() // async search(params) {
    //   const data = testData.data;
    //   this.fromInstance.setData = data.departure;
    //   this.toInstance.setData = data.return;
    //   return {
    //     from: this.fromInstance,
    //     to: this.toInstance,
    //   };
    // }

    /**
     * ANCHOR From Function
     * @author: yas
     * @desc
     * @return {object} this
     */

  }, {
    key: "setCurrent",
    value: function setCurrent(newCurrent) {
      this.current = newCurrent;
      return this;
    }
    /**
     * ANCHOR book
     * @author: yas
     * @desc
     */

  }, {
    key: "book",
    value: function book(info) {
      console.log(">>this.fromInstance", this.fromInstance.selectedTrip);
      console.log(">>this.toInstance", this.toInstance.selectedTrip);
      console.log('info argo', info);
      var data = {
        "holder": {
          "email": info.email.value,
          "name": {},
          "phone": "123456789",
          "surname": "sam",
          "title": "mr",
          "type": "ADULT"
        },
        "language": "en",
        "clientReference": "IntegrationAgency",
        "remark": "string",
        "transfers": [{
          "detail": {
            "departureFlightNumber": "IB4321"
          },
          "rateKey": "DEPARTURE|ATLAS|1523|IATA|PMI|2019-11-25|02:40|2019-11-25|05:00|1~0~0|3|108473|TAB-PVT-PRM-X|108473|TAB-PVT-PRM-X|1|PRVT|X|CR|PRM|53.99|CPASTILLA|AEROPUERTO|13171|2246|PMI|SIMPLE|137dc5e239595495cabbf15df5b76570"
        }]
      }; //   {
      //     "clientReference": "string",
      //     "holder": {
      //         "email": "sameh@gmail.com",
      //         "name": "sameh",
      //         "phone": "123456789",
      //         "surname": "sam",
      //         "title": "MR",
      //         "type": "ADULT"
      //     },
      //     "language": "en",
      //     "transfers": [
      //         {
      //             "detail": {
      //                 "arrivalFlightNumber": "string",
      //                 "arrivalShipName": "string",
      //                 "arrivalTrainInfo": {
      //                     "trainCompanyName": "string",
      //                     "trainNumber": "string"
      //                 },
      //                 "departureFlightNumber": "string",
      //                 "departureShipName": "string",
      //                 "departureTrainInfo": {
      //                     "trainCompanyName": "string",
      //                     "trainNumber": "string"
      //                 }
      //             },
      //             "rateKey": "DEPARTURE|ATLAS|1523|IATA|PMI|2019-10-25T02:40|10~1~1|16|108470|TAB-PVT-BUS-M|108470|TAB-PVT-BUS-M|1|PRVT|M|MNBS|STND|88.29|CPASTILLA|AEROPUERTO|13171|2212|PMI"
      //         }
      //     ]
      // }

      try {// const route = this.route('book');
        // const res = await this.post(route, params);
        // if (res.status === 200) {
        //   const data = res.status ? res.data.data : this.handleError();
        //   this.fromInstance.setData = data.departure;
        //   this.toInstance.setData = data.return;
        //   return {
        //     from: this.fromInstance,
        //     to: this.toInstance,
        //   };
        // }
      } catch (e) {
        return e;
      }

      return this;
    }
  }, {
    key: "handleError",
    value: function handleError(res) {
      console.log("handleError", res);
    }
  }, {
    key: "from",
    get: function get() {
      return this.fromInstance;
    }
    /**
     * ANCHOR To Function
     * @author: yas
     * @desc
     * @return {object} this
     */

  }, {
    key: "to",
    get: function get() {
      return this.toInstance;
    }
    /**
     * ANCHOR To Function
     * @author: yas
     * @desc
     * @return {object} this
     */

  }, {
    key: "currentData",
    get: function get() {
      this.fromInstance.isCurrentValue = false;
      this.toInstance.isCurrentValue = false;
      this["".concat(this.current, "Instance")].isCurrentValue = true;
      return this["".concat(this.current, "Instance")];
    }
  }]);

  return Transfer;
}(_api.default);

exports.default = Transfer;