"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "Hotels", {
  enumerable: true,
  get: function get() {
    return _hotels.default;
  }
});
Object.defineProperty(exports, "Transfer", {
  enumerable: true,
  get: function get() {
    return _transfer.default;
  }
});
Object.defineProperty(exports, "Mobile", {
  enumerable: true,
  get: function get() {
    return _mobile.default;
  }
});
Object.defineProperty(exports, "Auth", {
  enumerable: true,
  get: function get() {
    return _auth.default;
  }
});
Object.defineProperty(exports, "Url", {
  enumerable: true,
  get: function get() {
    return _url.default;
  }
});
exports.default = exports.promo = exports.auth = exports.mobile = exports.transfer = exports.hotels = void 0;

var _hotels = _interopRequireDefault(require("./hotels"));

var _transfer = _interopRequireDefault(require("./transfer"));

var _mobile = _interopRequireDefault(require("./mobile"));

var _auth = _interopRequireDefault(require("./auth"));

var _url = _interopRequireDefault(require("./url"));

var _api = _interopRequireDefault(require("./api"));

var _promo = _interopRequireDefault(require("./promo"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var hotels = new _hotels.default();
exports.hotels = hotels;
var transfer = new _transfer.default();
exports.transfer = transfer;
var mobile = new _mobile.default();
exports.mobile = mobile;
var auth = new _auth.default();
exports.auth = auth;
var promo = new _promo.default();
exports.promo = promo;
var _default = _api.default;
exports.default = _default;