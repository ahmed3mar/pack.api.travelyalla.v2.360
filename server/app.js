/* eslint-disable no-console */
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';

import { arrivalsDone } from './db/arrivals';
import { searchDone } from './db/search';
import { translation } from './db/trans';

const app = express();
app.use(cors());
app.use(bodyParser.json());

app.get('/api/v1/test', (req, res) => {
  // console.log('api/v1/test:get', { status: 200, response: req.query });
  res.status(200).send({ status: 200, response: req.query });
});

app.post('/api/v1/test', (req, res) => {
  // console.log('api/v1/test:post', { status: 200, response: req.body });
  res.status(200).send({ status: 200, response: req.body });
});

app.get('/api/v1/transfers/arrivals', (req, res) => {
  // console.log('api/v1/arrivals:get', req.query);
  res.status(200).send(arrivalsDone);
});

app.get('/api/v1/transfers/search', (req, res) => {
  // console.log('api/v1/transfer/search:get', req.query);
  res.status(200).send(searchDone);
});

app.post('/api/v1/transfers/search', (req, res) => {
  // console.log('api/v1/transfer/search:get', req.query);
  res.status(200).send(searchDone);
});

app.get('/api/v1/search', (req, res) => {
  // console.log('api/v1/search:get', req.query);
  res.status(200).send({ status: 200, response: req.query });
});

app.post('/api/v1/search', (req, res) => {
  // console.log('api/v1/search:post', req.body);
  res.status(200).send({ status: 200, response: req.body });
});

app.post('/api/v1/auth/login', (req, res) => {
  // console.log('/api/v1/auth/login', { status: 200, data: req.body });
  res.status(200).send({ status: 200,
    data: {
      access_token: 'TOKEN',
      token_type: 'bearer',
      expires_in: 3600,
      profile: {
        id: 1,
        name: 'Ahmed',
        email: 'ahmed@gmail.com',
      },
    } });
});

app.post('/api/v1/auth/register', (req, res) => {
  // console.log('/api/v1/auth/register', { status: 200, data: req.body });
  res.status(200).send({ status: 200,
    data: { access_token: 'TOKEN',
      token_type: 'bearer',
      expires_in: 3600,
      profile: {
        id: 2094,
        name: 'Ahmed',
        email: 'ahmed@gmail.com',
        thumb: null,
      } } });
});

app.post('/api/v1/translations', (req, res) => {
  console.log('/api/v1/translations', req.body);
  res.status(200).send(translation);
});

const PORT = 5004;

app.listen(PORT, () => {
  console.log(`server running on port ${PORT}`);
});
