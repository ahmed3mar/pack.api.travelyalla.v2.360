const arrivalsDone = {
  status: 200,
  response: [
    {
      dep_location: {
        dep_id: '6808',
        dep_name: 'Avari Dubai',
        dep_type: 'ATLAS',
      },
      arrival_locations: [
        {
          arrival_id: 'AUH',
          arrival_name: 'Abu Dhabi Int. Airport',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'AUHP',
          arrival_name: 'Abu Dhabi Harbour',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'DWC',
          arrival_name: 'Dubai World Central Airport',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'DXB',
          arrival_name: 'Dubai Int. Airport',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'DXBP',
          arrival_name: 'Dubai Harbour',
          arrival_type: 'IATA',
        },
      ],
    },
    {
      dep_location: {
        dep_id: '6811',
        dep_name: 'Dubai Grand',
        dep_type: 'ATLAS',
      },
      arrival_locations: [
        {
          arrival_id: 'AUH',
          arrival_name: 'Abu Dhabi Int. Airport',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'AUHP',
          arrival_name: 'Abu Dhabi Harbour',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'DWC',
          arrival_name: 'Dubai World Central Airport',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'DXB',
          arrival_name: 'Dubai Int. Airport',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'DXBP',
          arrival_name: 'Dubai Harbour',
          arrival_type: 'IATA',
        },
      ],
    },
    {
      dep_location: {
        dep_id: '6812',
        dep_name: 'Four Points Sheraton Bur Dubai',
        dep_type: 'ATLAS',
      },
      arrival_locations: [
        {
          arrival_id: 'AUH',
          arrival_name: 'Abu Dhabi Int. Airport',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'AUHP',
          arrival_name: 'Abu Dhabi Harbour',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'DWC',
          arrival_name: 'Dubai World Central Airport',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'DXB',
          arrival_name: 'Dubai Int. Airport',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'DXBP',
          arrival_name: 'Dubai Harbour',
          arrival_type: 'IATA',
        },
      ],
    },
    {
      dep_location: {
        dep_id: '7651',
        dep_name: 'Le Meridien Dubai Hotel & Conference Centre',
        dep_type: 'ATLAS',
      },
      arrival_locations: [
        {
          arrival_id: 'AUH',
          arrival_name: 'Abu Dhabi Int. Airport',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'AUHP',
          arrival_name: 'Abu Dhabi Harbour',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'DWC',
          arrival_name: 'Dubai World Central Airport',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'DXB',
          arrival_name: 'Dubai Int. Airport',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'DXBP',
          arrival_name: 'Dubai Harbour',
          arrival_type: 'IATA',
        },
      ],
    },
    {
      dep_location: {
        dep_id: '7654',
        dep_name: 'The Apartments Dubai World Trade Centre',
        dep_type: 'ATLAS',
      },
      arrival_locations: [
        {
          arrival_id: 'AUH',
          arrival_name: 'Abu Dhabi Int. Airport',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'AUHP',
          arrival_name: 'Abu Dhabi Harbour',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'DWC',
          arrival_name: 'Dubai World Central Airport',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'DXB',
          arrival_name: 'Dubai Int. Airport',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'DXBP',
          arrival_name: 'Dubai Harbour',
          arrival_type: 'IATA',
        },
      ],
    },
    {
      dep_location: {
        dep_id: '7661',
        dep_name: 'Grand Hyatt Dubai',
        dep_type: 'ATLAS',
      },
      arrival_locations: [
        {
          arrival_id: 'AUH',
          arrival_name: 'Abu Dhabi Int. Airport',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'AUHP',
          arrival_name: 'Abu Dhabi Harbour',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'DWC',
          arrival_name: 'Dubai World Central Airport',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'DXB',
          arrival_name: 'Dubai Int. Airport',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'DXBP',
          arrival_name: 'Dubai Harbour',
          arrival_type: 'IATA',
        },
      ],
    },
    {
      dep_location: {
        dep_id: '8919',
        dep_name: 'Panorama Bur Dubai',
        dep_type: 'ATLAS',
      },
      arrival_locations: [
        {
          arrival_id: 'AUH',
          arrival_name: 'Abu Dhabi Int. Airport',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'AUHP',
          arrival_name: 'Abu Dhabi Harbour',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'DWC',
          arrival_name: 'Dubai World Central Airport',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'DXB',
          arrival_name: 'Dubai Int. Airport',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'DXBP',
          arrival_name: 'Dubai Harbour',
          arrival_type: 'IATA',
        },
      ],
    },
    {
      dep_location: {
        dep_id: '9487',
        dep_name: 'Hilton Dubai Creek',
        dep_type: 'ATLAS',
      },
      arrival_locations: [
        {
          arrival_id: 'AUH',
          arrival_name: 'Abu Dhabi Int. Airport',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'AUHP',
          arrival_name: 'Abu Dhabi Harbour',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'DWC',
          arrival_name: 'Dubai World Central Airport',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'DXB',
          arrival_name: 'Dubai Int. Airport',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'DXBP',
          arrival_name: 'Dubai Harbour',
          arrival_type: 'IATA',
        },
      ],
    },
    {
      dep_location: {
        dep_id: '16027',
        dep_name: 'Orchid Hotel Dubai',
        dep_type: 'ATLAS',
      },
      arrival_locations: [
        {
          arrival_id: 'AUH',
          arrival_name: 'Abu Dhabi Int. Airport',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'AUHP',
          arrival_name: 'Abu Dhabi Harbour',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'DWC',
          arrival_name: 'Dubai World Central Airport',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'DXB',
          arrival_name: 'Dubai Int. Airport',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'DXBP',
          arrival_name: 'Dubai Harbour',
          arrival_type: 'IATA',
        },
      ],
    },
    {
      dep_location: {
        dep_id: '56315',
        dep_name: 'Movenpick Hotel & Apartments Bur Dubai',
        dep_type: 'ATLAS',
      },
      arrival_locations: [
        {
          arrival_id: 'AUH',
          arrival_name: 'Abu Dhabi Int. Airport',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'AUHP',
          arrival_name: 'Abu Dhabi Harbour',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'DWC',
          arrival_name: 'Dubai World Central Airport',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'DXB',
          arrival_name: 'Dubai Int. Airport',
          arrival_type: 'IATA',
        },
        {
          arrival_id: 'DXBP',
          arrival_name: 'Dubai Harbour',
          arrival_type: 'IATA',
        },
      ],
    },
  ],
  errors: {},
};
module.exports = {
  arrivalsDone,
};
