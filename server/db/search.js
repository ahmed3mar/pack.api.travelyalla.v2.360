const searchDone = {
  status: 200,
  response: {
    departure: [
      {
        id: '_1',
        direction: 'DEPARTURE',
        transferType: 'PRIVATE',
        pickupInformation: {
          from: {
            code: '1523',
            description: 'Visit Hotel Alexandra',
            type: 'ATLAS',
          },
          to: {
            code: 'PMI',
            description: 'Palma Majorca, Son Sant Joan Airport',
            type: 'IATA',
          },
          date: '2019-10-25',
          time: '02:40:00',
          pickup: {
            address: 'DELS PINS,15  ',
            number: null,
            town: 'CAN PASTILLA',
            zip: '07610',
            description:
              'You will be picked up outside the hotel entrance. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694. Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.',
            altitude: null,
            latitude: 39.5362,
            longitude: 2.71746,
            checkPickup: {
              mustCheckPickupTime: false,
              url: null,
              hoursBeforeConsulting: null,
            },
            pickupId: 13171,
            stopName: 'OUTSIDE',
            image: null,
          },
        },
        content: {
          vehicle: {
            code: 'MNBS',
            name: 'Minibus',
          },
          category: {
            code: 'STND',
            name: 'Standard',
          },
          images: [
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/small/prvt-stnd-mnbs.png',
              type: 'SMALL',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/medium/prvt-stnd-mnbs.png',
              type: 'MEDIUM',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/large/prvt-stnd-mnbs.png',
              type: 'LARGE',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/extralarge/prvt-stnd-mnbs.png',
              type: 'EXTRALARGE',
            }
          ],
          transferDetailInfo: [
            {
              id: 'ER',
              name: 'Exclusive ride for you',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'DTDS',
              name: 'Door to door service',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'AV247',
              name: 'Available 24/7',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'BA',
              name:
                '1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'SPLU',
              name: 'SPECIAL LUGGAGE',
              description:
                'In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'DRVR',
              name: "CAN'T FIND DRIVER",
              description:
                'In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'CHIN',
              name: 'CHECK INFORMATION',
              description:
                'If the details do not correspond with the reservation, please contact your agency immediately.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'CBBS',
              name: 'CHILDBOOSTER / BABY SEAT',
              description:
                'Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.',
              type: 'GENERIC_GUIDELINES',
            }
          ],
          customerTransferTimeInfo: [
            {
              value: 30,
              type: 'CUSTOMER_MAX_WAITING_TIME',
              metric: 'minutes',
            }
          ],
          supplierTransferTimeInfo: [
            {
              value: 15,
              type: 'SUPPLIER_MAX_WAITING_TIME_DOMESTIC',
              metric: 'minutes',
            },
          ],
          transferRemarks: [
            {
              type: 'CONTRACT',
              description:
                'Pick-up point\nYou will be picked up outside the hotel entrance. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694. Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.\n\nTransfer information:\n\n* Maximum client waiting time 30 minutes\n* Maximum waiting time for drivers in domestic arrivals 15 minutes\n\n\n* In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.* In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.* If the details do not correspond with the reservation, please contact your agency immediately.* Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.\n\n* Exclusive ride for you* Door to door service* Available 24/7* 1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm\n\n',
              mandatory: true,
            },
          ],
        },
        price: {
          totalAmount: 100.5,
          netAmount: 76.64,
          currencyId: 'EUR',
        },
        rateKey:
          'DEPARTURE|ATLAS|1523|IATA|PMI|2019-10-25T02:40|10~1~1|16|108470|TAB-PVT-BUS-M|108470|TAB-PVT-BUS-M|1|PRVT|M|MNBS|STND|88.29|CPASTILLA|AEROPUERTO|13171|2212|PMI',
        cancellationPolicies: [
          {
            amount: 88.29,
            from: '2019-10-23T23:59:00',
            currencyId: 'EUR',
          }
        ]
      },
      {
        id: '_2',
        direction: 'DEPARTURE',
        transferType: 'SHARED',
        pickupInformation: {
          from: {
            code: '1523',
            description: 'Visit Hotel Alexandra',
            type: 'ATLAS',
          },
          to: {
            code: 'PMI',
            description: 'Palma Majorca, Son Sant Joan Airport',
            type: 'IATA',
          },
          date: '2019-10-25',
          time: null,
          pickup: {
            address: 'DELS PINS,15  ',
            number: null,
            town: 'CAN PASTILLA',
            zip: '07610',
            description:
              'You will be picked up outside the hotel entrance. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694 .Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.',
            altitude: null,
            latitude: 39.5362,
            longitude: 2.71746,
            checkPickup: {
              mustCheckPickupTime: true,
              url: 'http://www.checkpickup.com/',
              hoursBeforeConsulting: 24,
            },
            pickupId: 2135,
            stopName: 'OUTSIDE',
            image: null,
          }
        },
        content: {
          vehicle: {
            code: 'BSEB',
            name: 'Bus bicycle carriage included',
          },
          category: {
            code: 'STND',
            name: 'Standard',
          },
          images: [
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/small/shrd-stnd-bseb.png',
              type: 'SMALL',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/medium/shrd-stnd-bseb.png',
              type: 'MEDIUM',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/large/shrd-stnd-bseb.png',
              type: 'LARGE',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/extralarge/shrd-stnd-bseb.png',
              type: 'EXTRALARGE',
            }
          ],
          transferDetailInfo: [
            {
              id: 'MST',
              name: 'Maximum',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'BAHB',
              name: '1 item of hand baggage allowed per person',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'BA',
              name:
                '1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'SPLU',
              name: 'SPECIAL LUGGAGE',
              description:
                'In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'CHIN',
              name: 'CHECK INFORMATION',
              description:
                'If the details do not correspond with the reservation, please contact your agency immediately.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'CBBS',
              name: 'CHILDBOOSTER / BABY SEAT',
              description:
                'Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'RESTACCESS',
              name: 'restricted access',
              description:
                'Please note that due to traffic restrictions within the city centre there are some hotels in which you will be dropped as close as possible with the chance of a short walk, instead of directly outside.',
              type: 'GENERIC_GUIDELINES',
            },
          ],
          customerTransferTimeInfo: [
            {
              value: 30,
              type: 'CUSTOMER_MAX_WAITING_TIME',
              metric: 'minutes',
            },
          ],
          supplierTransferTimeInfo: [
            {
              value: 5,
              type: 'SUPPLIER_MAX_WAITING_TIME_DOMESTIC',
              metric: 'minutes',
            },
          ],
          transferRemarks: [
            {
              type: 'CONTRACT',
              description:
                'Pick-up point\nYou will be picked up outside the hotel entrance. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694 .Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.\n\nTransfer information:\n\n* Maximum client waiting time 30 minutes\n* Maximum waiting time for drivers in domestic arrivals 5 minutes\n\n\n* In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.* If the details do not correspond with the reservation, please contact your agency immediately.* Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.* Please note that due to traffic restrictions within the city centre there are some hotels in which you will be dropped as close as possible with the chance of a short walk, instead of directly outside.\n\n* Maximum* 1 item of hand baggage allowed per person* 1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm\n\n',
              mandatory: true,
            }
          ]
        },
        price: {
          totalAmount: 200.5,
          netAmount: 145.3,
          currencyId: 'EUR',
        },
        rateKey:
          'DEPARTURE|ATLAS|1523|IATA|PMI|2019-10-25T05:00|10~1~1|999|108466|TAB-SHT-XBIKE-1|108466|TAB-SHT-XBIKE-1|1|SHRD|1|BSEB|STND|167.4|CPASTILLA|AEROPUERTO|2135|218|PMI',
        cancellationPolicies: [
          {
            amount: 167.44,
            from: '2019-10-22T23:59:00',
            currencyId: 'EUR',
          },
        ],
      },
      {
        id: '_3',
        direction: 'DEPARTURE',
        transferType: 'SHARED',
        pickupInformation: {
          from: {
            code: '1523',
            description: 'Visit Hotel Alexandra',
            type: 'ATLAS',
          },
          to: {
            code: 'PMI',
            description: 'Palma Majorca, Son Sant Joan Airport',
            type: 'IATA',
          },
          date: '2019-10-25',
          time: null,
          pickup: {
            address: 'DELS PINS,15  ',
            number: null,
            town: 'CAN PASTILLA',
            zip: '07610',
            description:
              'You will be picked up outside the hotel entrance. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694 .Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.',
            altitude: null,
            latitude: 39.5362,
            longitude: 2.71746,
            checkPickup: {
              mustCheckPickupTime: true,
              url: 'http://www.checkpickup.com/',
              hoursBeforeConsulting: 24,
            },
            pickupId: 2135,
            stopName: 'OUTSIDE',
            image: null,
          },
        },
        content: {
          vehicle: {
            code: 'BS',
            name: 'Bus',
          },
          category: {
            code: 'STND',
            name: 'Standard',
          },
          images: [
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/small/shrd-stnd-bs.png',
              type: 'SMALL',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/medium/shrd-stnd-bs.png',
              type: 'MEDIUM',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/large/shrd-stnd-bs.png',
              type: 'LARGE',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/extralarge/shrd-stnd-bs.png',
              type: 'EXTRALARGE',
            }
          ],
          transferDetailInfo: [
            {
              id: 'MST',
              name: 'Maximum',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'BAHB',
              name: '1 item of hand baggage allowed per person',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'BA',
              name:
                '1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'CHIN',
              name: 'CHECK INFORMATION',
              description:
                'If the details do not correspond with the reservation, please contact your agency immediately.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'DRVR',
              name: "CAN'T FIND DRIVER",
              description:
                'In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'BCNACCESS',
              name: 'Bus restricted access',
              description:
                'Some hotels have restricted bus access, in these cases costumers must follow indications as arrivals and departures will be from a meeting point.',
              type: 'GENERIC_GUIDELINES',
            },
          ],
          customerTransferTimeInfo: [
            {
              value: 30,
              type: 'CUSTOMER_MAX_WAITING_TIME',
              metric: 'minutes',
            },
          ],
          supplierTransferTimeInfo: [
            {
              value: 5,
              type: 'SUPPLIER_MAX_WAITING_TIME_DOMESTIC',
              metric: 'minutes',
            },
          ],
          transferRemarks: [
            {
              type: 'CONTRACT',
              description:
                'Pick-up point\nYou will be picked up outside the hotel entrance. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694 .Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.\n\nTransfer information:\n\n* Maximum client waiting time 30 minutes\n* Maximum waiting time for drivers in domestic arrivals 5 minutes\n\n\n* If the details do not correspond with the reservation, please contact your agency immediately.* In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.* Some hotels have restricted bus access, in these cases costumers must follow indications as arrivals and departures will be from a meeting point.\n\n* Maximum* 1 item of hand baggage allowed per person* 1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm\n\n',
              mandatory: true,
            },
          ],
        },
        price: {
          totalAmount: 300.5,
          netAmount: 70.83,
          currencyId: 'EUR',
        },
        rateKey:
          'DEPARTURE|ATLAS|1523|IATA|PMI|2019-10-25T05:00|10~1~1|999|108462|TAB-SHT-STD-B|108462|TAB-SHT-STD-B|1|SHRD|B|BS|STND|81.6|CPASTILLA|AEROPUERTO|2135|2143|PMI',
        cancellationPolicies: [
          {
            amount: 81.57,
            from: '2019-10-23T23:59:00',
            currencyId: 'EUR',
          },
        ],
      },
      {
        id: '_4',
        direction: 'DEPARTURE',
        transferType: 'SHARED',
        pickupInformation: {
          from: {
            code: '1523',
            description: 'Visit Hotel Alexandra',
            type: 'ATLAS',
          },
          to: {
            code: 'PMI',
            description: 'Palma Majorca, Son Sant Joan Airport',
            type: 'IATA',
          },
          date: '2019-10-25',
          time: null,
          pickup: {
            address: 'DELS PINS,15  ',
            number: null,
            town: 'CAN PASTILLA',
            zip: '07610',
            description:
              'You will be picked up outside the hotel entrance. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694 .Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.',
            altitude: null,
            latitude: 39.5362,
            longitude: 2.71746,
            checkPickup: {
              mustCheckPickupTime: true,
              url: 'http://www.checkpickup.com/',
              hoursBeforeConsulting: 24,
            },
            pickupId: 2135,
            stopName: 'OUTSIDE',
            image: null,
          }
        },
        content: {
          vehicle: {
            code: 'BSEB',
            name: 'Bus bicycle carriage included',
          },
          category: {
            code: 'STND',
            name: 'Standard',
          },
          images: [
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/small/shrd-stnd-bseb.png',
              type: 'SMALL',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/medium/shrd-stnd-bseb.png',
              type: 'MEDIUM',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/large/shrd-stnd-bseb.png',
              type: 'LARGE',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/extralarge/shrd-stnd-bseb.png',
              type: 'EXTRALARGE',
            },
          ],
          transferDetailInfo: [
            {
              id: 'MST',
              name: 'Maximum',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'BAHB',
              name: '1 item of hand baggage allowed per person',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'BA',
              name:
                '1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'SPLU',
              name: 'SPECIAL LUGGAGE',
              description:
                'In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'CHIN',
              name: 'CHECK INFORMATION',
              description:
                'If the details do not correspond with the reservation, please contact your agency immediately.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'CBBS',
              name: 'CHILDBOOSTER / BABY SEAT',
              description:
                'Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'RESTACCESS',
              name: 'restricted access',
              description:
                'Please note that due to traffic restrictions within the city centre there are some hotels in which you will be dropped as close as possible with the chance of a short walk, instead of directly outside.',
              type: 'GENERIC_GUIDELINES',
            },
          ],
          customerTransferTimeInfo: [
            {
              value: 30,
              type: 'CUSTOMER_MAX_WAITING_TIME',
              metric: 'minutes',
            },
          ],
          supplierTransferTimeInfo: [
            {
              value: 5,
              type: 'SUPPLIER_MAX_WAITING_TIME_DOMESTIC',
              metric: 'minutes',
            },
          ],
          transferRemarks: [
            {
              type: 'CONTRACT',
              description:
                'Pick-up point\nYou will be picked up outside the hotel entrance. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694 .Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.\n\nTransfer information:\n\n* Maximum client waiting time 30 minutes\n* Maximum waiting time for drivers in domestic arrivals 5 minutes\n\n\n* In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.* If the details do not correspond with the reservation, please contact your agency immediately.* Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.* Please note that due to traffic restrictions within the city centre there are some hotels in which you will be dropped as close as possible with the chance of a short walk, instead of directly outside.\n\n* Maximum* 1 item of hand baggage allowed per person* 1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm\n\n',
              mandatory: true,
            },
          ],
        },
        price: {
          totalAmount: 400.5,
          netAmount: 145.3,
          currencyId: 'EUR',
        },
        rateKey:
          'DEPARTURE|ATLAS|1523|IATA|PMI|2019-10-25T05:00|10~1~1|999|107147|TAB SHR XBIKE|107147|TAB SHR XBIKE|1|SHRD|1|BSEB|STND|167.4|CPASTILLA|AEROPUERTO|2135|218|PMI',
        cancellationPolicies: [
          {
            amount: 167.44,
            from: '2019-10-22T23:59:00',
            currencyId: 'EUR',
          },
        ],
      },
      {
        id: '_5',
        direction: 'DEPARTURE',
        transferType: 'SHARED',
        pickupInformation: {
          from: {
            code: '1523',
            description: 'Visit Hotel Alexandra',
            type: 'ATLAS',
          },
          to: {
            code: 'PMI',
            description: 'Palma Majorca, Son Sant Joan Airport',
            type: 'IATA',
          },
          date: '2019-10-25',
          time: null,
          pickup: {
            address: 'DELS PINS,15  ',
            number: null,
            town: 'CAN PASTILLA',
            zip: '07610',
            description:
              'You will be picked up outside the hotel entrance. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694 .Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.',
            altitude: null,
            latitude: 39.5362,
            longitude: 2.71746,
            checkPickup: {
              mustCheckPickupTime: true,
              url: 'http://www.checkpickup.com/',
              hoursBeforeConsulting: 24,
            },
            pickupId: 2135,
            stopName: 'OUTSIDE',
            image: null,
          },
        },
        content: {
          vehicle: {
            code: 'BS',
            name: 'Bus',
          },
          category: {
            code: 'STND',
            name: 'Standard',
          },
          images: [
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/small/shrd-stnd-bs.png',
              type: 'SMALL',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/medium/shrd-stnd-bs.png',
              type: 'MEDIUM',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/large/shrd-stnd-bs.png',
              type: 'LARGE',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/extralarge/shrd-stnd-bs.png',
              type: 'EXTRALARGE',
            },
          ],
          transferDetailInfo: [
            {
              id: 'MST',
              name: 'Maximum',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'BAHB',
              name: '1 item of hand baggage allowed per person',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'BA',
              name:
                '1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'CHIN',
              name: 'CHECK INFORMATION',
              description:
                'If the details do not correspond with the reservation, please contact your agency immediately.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'DRVR',
              name: "CAN'T FIND DRIVER",
              description:
                'In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'BCNACCESS',
              name: 'Bus restricted access',
              description:
                'Some hotels have restricted bus access, in these cases costumers must follow indications as arrivals and departures will be from a meeting point.',
              type: 'GENERIC_GUIDELINES',
            },
          ],
          customerTransferTimeInfo: [
            {
              value: 30,
              type: 'CUSTOMER_MAX_WAITING_TIME',
              metric: 'minutes',
            },
          ],
          supplierTransferTimeInfo: [
            {
              value: 5,
              type: 'SUPPLIER_MAX_WAITING_TIME_DOMESTIC',
              metric: 'minutes',
            },
          ],
          transferRemarks: [
            {
              type: 'CONTRACT',
              description:
                'Pick-up point\nYou will be picked up outside the hotel entrance. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694 .Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.\n\nTransfer information:\n\n* Maximum client waiting time 30 minutes\n* Maximum waiting time for drivers in domestic arrivals 5 minutes\n\n\n* If the details do not correspond with the reservation, please contact your agency immediately.* In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.* Some hotels have restricted bus access, in these cases costumers must follow indications as arrivals and departures will be from a meeting point.\n\n* Maximum* 1 item of hand baggage allowed per person* 1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm\n\n',
              mandatory: true,
            },
          ],
        },
        price: {
          totalAmount: 500.5,
          netAmount: 70.83,
          currencyId: 'EUR',
        },
        rateKey:
          'DEPARTURE|ATLAS|1523|IATA|PMI|2019-10-25T05:00|10~1~1|999|107146|TAB SHT STD|107146|TAB SHT STD|1|SHRD|B|BS|STND|81.6|CPASTILLA|AEROPUERTO|2135|2143|PMI',
        cancellationPolicies: [
          {
            amount: 81.57,
            from: '2019-10-23T23:59:00',
            currencyId: 'EUR',
          },
        ],
      },
      {
        id: '_6',
        direction: 'DEPARTURE',
        transferType: 'PRIVATE',
        pickupInformation: {
          from: {
            code: '1523',
            description: 'Visit Hotel Alexandra',
            type: 'ATLAS',
          },
          to: {
            code: 'PMI',
            description: 'Palma Majorca, Son Sant Joan Airport',
            type: 'IATA',
          },
          date: '2019-10-25',
          time: '02:40:00',
          pickup: {
            address: 'DELS PINS,15  ',
            number: null,
            town: 'CAN PASTILLA',
            zip: '07610',
            description:
              'You will be picked up outside the hotel entrance. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694. Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.',
            altitude: null,
            latitude: 39.5362,
            longitude: 2.71746,
            checkPickup: {
              mustCheckPickupTime: false,
              url: null,
              hoursBeforeConsulting: null,
            },
            pickupId: 13171,
            stopName: 'OUTSIDE',
            image: null,
          },
        },
        content: {
          vehicle: {
            code: 'MNBS',
            name: 'Minibus',
          },
          category: {
            code: 'STND',
            name: 'Standard',
          },
          images: [
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/small/prvt-stnd-mnbs.png',
              type: 'SMALL',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/medium/prvt-stnd-mnbs.png',
              type: 'MEDIUM',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/large/prvt-stnd-mnbs.png',
              type: 'LARGE',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/extralarge/prvt-stnd-mnbs.png',
              type: 'EXTRALARGE',
            },
          ],
          transferDetailInfo: [
            {
              id: 'ER',
              name: 'Exclusive ride for you',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'DTDS',
              name: 'Door to door service',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'AV247',
              name: 'Available 24/7',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'BA',
              name:
                '1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'SPLU',
              name: 'SPECIAL LUGGAGE',
              description:
                'In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'DRVR',
              name: "CAN'T FIND DRIVER",
              description:
                'In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'CHIN',
              name: 'CHECK INFORMATION',
              description:
                'If the details do not correspond with the reservation, please contact your agency immediately.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'CBBS',
              name: 'CHILDBOOSTER / BABY SEAT',
              description:
                'Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.',
              type: 'GENERIC_GUIDELINES',
            }
          ],
          customerTransferTimeInfo: [
            {
              value: 30,
              type: 'CUSTOMER_MAX_WAITING_TIME',
              metric: 'minutes',
            },
          ],
          supplierTransferTimeInfo: [
            {
              value: 15,
              type: 'SUPPLIER_MAX_WAITING_TIME_DOMESTIC',
              metric: 'minutes',
            },
          ],
          transferRemarks: [
            {
              type: 'CONTRACT',
              description:
                'Pick-up point\nYou will be picked up outside the hotel entrance. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694. Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.\n\nTransfer information:\n\n* Maximum client waiting time 30 minutes\n* Maximum waiting time for drivers in domestic arrivals 15 minutes\n\n\n* In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.* In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.* If the details do not correspond with the reservation, please contact your agency immediately.* Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.\n\n* Exclusive ride for you* Door to door service* Available 24/7* 1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm\n\n',
              mandatory: true,
            },
          ],
        },
        price: {
          totalAmount: 600.5,
          netAmount: 76.24,
          currencyId: 'EUR',
        },
        rateKey:
          'DEPARTURE|ATLAS|1523|IATA|PMI|2019-10-25T02:40|10~1~1|16|107141|TAB PVT BUS|107141|TAB PVT BUS|1|PRVT|M|MNBS|STND|87.83|CPASTILLA|AEROPUERTO|13171|2212|PMI',
        cancellationPolicies: [
          {
            amount: 87.83,
            from: '2019-10-22T23:59:00',
            currencyId: 'EUR',
          },
        ],
      },
      {
        id: '_7',
        direction: 'DEPARTURE',
        transferType: 'PRIVATE',
        pickupInformation: {
          from: {
            code: '1523',
            description: 'Visit Hotel Alexandra',
            type: 'ATLAS',
          },
          to: {
            code: 'PMI',
            description: 'Palma Majorca, Son Sant Joan Airport',
            type: 'IATA',
          },
          date: '2019-10-25',
          time: '02:40:00',
          pickup: {
            address: 'DELS PINS,15  ',
            number: null,
            town: 'CAN PASTILLA',
            zip: '07610',
            description:
              'You will be picked up outside the hotel entrance. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694. Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.',
            altitude: null,
            latitude: 39.5362,
            longitude: 2.71746,
            checkPickup: {
              mustCheckPickupTime: false,
              url: null,
              hoursBeforeConsulting: null,
            },
            pickupId: 13171,
            stopName: 'OUTSIDE',
            image: null,
          },
        },
        content: {
          vehicle: {
            code: 'MNBS',
            name: 'Minibus',
          },
          category: {
            code: 'STND',
            name: 'Standard',
          },
          images: [
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/small/prvt-stnd-mnbs.png',
              type: 'SMALL',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/medium/prvt-stnd-mnbs.png',
              type: 'MEDIUM',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/large/prvt-stnd-mnbs.png',
              type: 'LARGE',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/extralarge/prvt-stnd-mnbs.png',
              type: 'EXTRALARGE',
            },
          ],
          transferDetailInfo: [
            {
              id: 'ER',
              name: 'Exclusive ride for you',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'DTDS',
              name: 'Door to door service',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'AV247',
              name: 'Available 24/7',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'BA',
              name:
                '1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'SPLU',
              name: 'SPECIAL LUGGAGE',
              description:
                'In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'DRVR',
              name: "CAN'T FIND DRIVER",
              description:
                'In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'CHIN',
              name: 'CHECK INFORMATION',
              description:
                'If the details do not correspond with the reservation, please contact your agency immediately.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'CBBS',
              name: 'CHILDBOOSTER / BABY SEAT',
              description:
                'Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.',
              type: 'GENERIC_GUIDELINES',
            }
          ],
          customerTransferTimeInfo: [
            {
              value: 30,
              type: 'CUSTOMER_MAX_WAITING_TIME',
              metric: 'minutes',
            },
          ],
          supplierTransferTimeInfo: [
            {
              value: 15,
              type: 'SUPPLIER_MAX_WAITING_TIME_DOMESTIC',
              metric: 'minutes',
            },
          ],
          transferRemarks: [
            {
              type: 'CONTRACT',
              description:
                'Pick-up point\nYou will be picked up outside the hotel entrance. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694. Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.\n\nTransfer information:\n\n* Maximum client waiting time 30 minutes\n* Maximum waiting time for drivers in domestic arrivals 15 minutes\n\n\n* In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.* In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.* If the details do not correspond with the reservation, please contact your agency immediately.* Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.\n\n* Exclusive ride for you* Door to door service* Available 24/7* 1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm\n\n',
              mandatory: true,
            },
          ],
        },
        price: {
          totalAmount: 600.5,
          netAmount: 76.24,
          currencyId: 'EUR',
        },
        rateKey:
          'DEPARTURE|ATLAS|1523|IATA|PMI|2019-10-25T02:40|10~1~1|16|107141|TAB PVT BUS|107141|TAB PVT BUS|1|PRVT|M|MNBS|STND|87.83|CPASTILLA|AEROPUERTO|13171|2212|PMI',
        cancellationPolicies: [
          {
            amount: 87.83,
            from: '2019-10-22T23:59:00',
            currencyId: 'EUR',
          },
        ],
      },
      {
        id: '_8',
        direction: 'DEPARTURE',
        transferType: 'PRIVATE',
        pickupInformation: {
          from: {
            code: '1523',
            description: 'Visit Hotel Alexandra',
            type: 'ATLAS',
          },
          to: {
            code: 'PMI',
            description: 'Palma Majorca, Son Sant Joan Airport',
            type: 'IATA',
          },
          date: '2019-10-25',
          time: '02:40:00',
          pickup: {
            address: 'DELS PINS,15  ',
            number: null,
            town: 'CAN PASTILLA',
            zip: '07610',
            description:
              'You will be picked up outside the hotel entrance. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694. Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.',
            altitude: null,
            latitude: 39.5362,
            longitude: 2.71746,
            checkPickup: {
              mustCheckPickupTime: false,
              url: null,
              hoursBeforeConsulting: null,
            },
            pickupId: 13171,
            stopName: 'OUTSIDE',
            image: null,
          },
        },
        content: {
          vehicle: {
            code: 'MNBS',
            name: 'Minibus',
          },
          category: {
            code: 'STND',
            name: 'Standard',
          },
          images: [
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/small/prvt-stnd-mnbs.png',
              type: 'SMALL',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/medium/prvt-stnd-mnbs.png',
              type: 'MEDIUM',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/large/prvt-stnd-mnbs.png',
              type: 'LARGE',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/extralarge/prvt-stnd-mnbs.png',
              type: 'EXTRALARGE',
            },
          ],
          transferDetailInfo: [
            {
              id: 'ER',
              name: 'Exclusive ride for you',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'DTDS',
              name: 'Door to door service',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'AV247',
              name: 'Available 24/7',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'BA',
              name:
                '1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'SPLU',
              name: 'SPECIAL LUGGAGE',
              description:
                'In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'DRVR',
              name: "CAN'T FIND DRIVER",
              description:
                'In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'CHIN',
              name: 'CHECK INFORMATION',
              description:
                'If the details do not correspond with the reservation, please contact your agency immediately.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'CBBS',
              name: 'CHILDBOOSTER / BABY SEAT',
              description:
                'Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.',
              type: 'GENERIC_GUIDELINES',
            }
          ],
          customerTransferTimeInfo: [
            {
              value: 30,
              type: 'CUSTOMER_MAX_WAITING_TIME',
              metric: 'minutes',
            },
          ],
          supplierTransferTimeInfo: [
            {
              value: 15,
              type: 'SUPPLIER_MAX_WAITING_TIME_DOMESTIC',
              metric: 'minutes',
            },
          ],
          transferRemarks: [
            {
              type: 'CONTRACT',
              description:
                'Pick-up point\nYou will be picked up outside the hotel entrance. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694. Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.\n\nTransfer information:\n\n* Maximum client waiting time 30 minutes\n* Maximum waiting time for drivers in domestic arrivals 15 minutes\n\n\n* In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.* In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.* If the details do not correspond with the reservation, please contact your agency immediately.* Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.\n\n* Exclusive ride for you* Door to door service* Available 24/7* 1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm\n\n',
              mandatory: true,
            },
          ],
        },
        price: {
          totalAmount: 600.5,
          netAmount: 76.24,
          currencyId: 'EUR',
        },
        rateKey:
          'DEPARTURE|ATLAS|1523|IATA|PMI|2019-10-25T02:40|10~1~1|16|107141|TAB PVT BUS|107141|TAB PVT BUS|1|PRVT|M|MNBS|STND|87.83|CPASTILLA|AEROPUERTO|13171|2212|PMI',
        cancellationPolicies: [
          {
            amount: 87.83,
            from: '2019-10-22T23:59:00',
            currencyId: 'EUR',
          },
        ],
      },
      {
        id: '_9',
        direction: 'DEPARTURE',
        transferType: 'PRIVATE',
        pickupInformation: {
          from: {
            code: '1523',
            description: 'Visit Hotel Alexandra',
            type: 'ATLAS',
          },
          to: {
            code: 'PMI',
            description: 'Palma Majorca, Son Sant Joan Airport',
            type: 'IATA',
          },
          date: '2019-10-25',
          time: '02:40:00',
          pickup: {
            address: 'DELS PINS,15  ',
            number: null,
            town: 'CAN PASTILLA',
            zip: '07610',
            description:
              'You will be picked up outside the hotel entrance. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694. Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.',
            altitude: null,
            latitude: 39.5362,
            longitude: 2.71746,
            checkPickup: {
              mustCheckPickupTime: false,
              url: null,
              hoursBeforeConsulting: null,
            },
            pickupId: 13171,
            stopName: 'OUTSIDE',
            image: null,
          },
        },
        content: {
          vehicle: {
            code: 'MNBS',
            name: 'Minibus',
          },
          category: {
            code: 'STND',
            name: 'Standard',
          },
          images: [
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/small/prvt-stnd-mnbs.png',
              type: 'SMALL',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/medium/prvt-stnd-mnbs.png',
              type: 'MEDIUM',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/large/prvt-stnd-mnbs.png',
              type: 'LARGE',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/extralarge/prvt-stnd-mnbs.png',
              type: 'EXTRALARGE',
            },
          ],
          transferDetailInfo: [
            {
              id: 'ER',
              name: 'Exclusive ride for you',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'DTDS',
              name: 'Door to door service',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'AV247',
              name: 'Available 24/7',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'BA',
              name:
                '1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'SPLU',
              name: 'SPECIAL LUGGAGE',
              description:
                'In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'DRVR',
              name: "CAN'T FIND DRIVER",
              description:
                'In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'CHIN',
              name: 'CHECK INFORMATION',
              description:
                'If the details do not correspond with the reservation, please contact your agency immediately.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'CBBS',
              name: 'CHILDBOOSTER / BABY SEAT',
              description:
                'Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.',
              type: 'GENERIC_GUIDELINES',
            }
          ],
          customerTransferTimeInfo: [
            {
              value: 30,
              type: 'CUSTOMER_MAX_WAITING_TIME',
              metric: 'minutes',
            },
          ],
          supplierTransferTimeInfo: [
            {
              value: 15,
              type: 'SUPPLIER_MAX_WAITING_TIME_DOMESTIC',
              metric: 'minutes',
            },
          ],
          transferRemarks: [
            {
              type: 'CONTRACT',
              description:
                'Pick-up point\nYou will be picked up outside the hotel entrance. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694. Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.\n\nTransfer information:\n\n* Maximum client waiting time 30 minutes\n* Maximum waiting time for drivers in domestic arrivals 15 minutes\n\n\n* In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.* In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.* If the details do not correspond with the reservation, please contact your agency immediately.* Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.\n\n* Exclusive ride for you* Door to door service* Available 24/7* 1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm\n\n',
              mandatory: true,
            },
          ],
        },
        price: {
          totalAmount: 600.5,
          netAmount: 76.24,
          currencyId: 'EUR',
        },
        rateKey:
          'DEPARTURE|ATLAS|1523|IATA|PMI|2019-10-25T02:40|10~1~1|16|107141|TAB PVT BUS|107141|TAB PVT BUS|1|PRVT|M|MNBS|STND|87.83|CPASTILLA|AEROPUERTO|13171|2212|PMI',
        cancellationPolicies: [
          {
            amount: 87.83,
            from: '2019-10-22T23:59:00',
            currencyId: 'EUR',
          },
        ],
      },
      {
        id: '_10',
        direction: 'DEPARTURE',
        transferType: 'PRIVATE',
        pickupInformation: {
          from: {
            code: '1523',
            description: 'Visit Hotel Alexandra',
            type: 'ATLAS',
          },
          to: {
            code: 'PMI',
            description: 'Palma Majorca, Son Sant Joan Airport',
            type: 'IATA',
          },
          date: '2019-10-25',
          time: '02:40:00',
          pickup: {
            address: 'DELS PINS,15  ',
            number: null,
            town: 'CAN PASTILLA',
            zip: '07610',
            description:
              'You will be picked up outside the hotel entrance. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694. Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.',
            altitude: null,
            latitude: 39.5362,
            longitude: 2.71746,
            checkPickup: {
              mustCheckPickupTime: false,
              url: null,
              hoursBeforeConsulting: null,
            },
            pickupId: 13171,
            stopName: 'OUTSIDE',
            image: null,
          },
        },
        content: {
          vehicle: {
            code: 'MNBS',
            name: 'Minibus',
          },
          category: {
            code: 'STND',
            name: 'Standard',
          },
          images: [
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/small/prvt-stnd-mnbs.png',
              type: 'SMALL',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/medium/prvt-stnd-mnbs.png',
              type: 'MEDIUM',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/large/prvt-stnd-mnbs.png',
              type: 'LARGE',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/extralarge/prvt-stnd-mnbs.png',
              type: 'EXTRALARGE',
            },
          ],
          transferDetailInfo: [
            {
              id: 'ER',
              name: 'Exclusive ride for you',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'DTDS',
              name: 'Door to door service',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'AV247',
              name: 'Available 24/7',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'BA',
              name:
                '1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'SPLU',
              name: 'SPECIAL LUGGAGE',
              description:
                'In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'DRVR',
              name: "CAN'T FIND DRIVER",
              description:
                'In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'CHIN',
              name: 'CHECK INFORMATION',
              description:
                'If the details do not correspond with the reservation, please contact your agency immediately.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'CBBS',
              name: 'CHILDBOOSTER / BABY SEAT',
              description:
                'Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.',
              type: 'GENERIC_GUIDELINES',
            }
          ],
          customerTransferTimeInfo: [
            {
              value: 30,
              type: 'CUSTOMER_MAX_WAITING_TIME',
              metric: 'minutes',
            },
          ],
          supplierTransferTimeInfo: [
            {
              value: 15,
              type: 'SUPPLIER_MAX_WAITING_TIME_DOMESTIC',
              metric: 'minutes',
            },
          ],
          transferRemarks: [
            {
              type: 'CONTRACT',
              description:
                'Pick-up point\nYou will be picked up outside the hotel entrance. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694. Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.\n\nTransfer information:\n\n* Maximum client waiting time 30 minutes\n* Maximum waiting time for drivers in domestic arrivals 15 minutes\n\n\n* In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.* In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.* If the details do not correspond with the reservation, please contact your agency immediately.* Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.\n\n* Exclusive ride for you* Door to door service* Available 24/7* 1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm\n\n',
              mandatory: true,
            },
          ],
        },
        price: {
          totalAmount: 600.5,
          netAmount: 76.24,
          currencyId: 'EUR',
        },
        rateKey:
          'DEPARTURE|ATLAS|1523|IATA|PMI|2019-10-25T02:40|10~1~1|16|107141|TAB PVT BUS|107141|TAB PVT BUS|1|PRVT|M|MNBS|STND|87.83|CPASTILLA|AEROPUERTO|13171|2212|PMI',
        cancellationPolicies: [
          {
            amount: 87.83,
            from: '2019-10-22T23:59:00',
            currencyId: 'EUR',
          },
        ],
      },
    ],
    return: [
      {
        id: '_1',
        direction: 'RETURN',
        transferType: 'PRIVATE',
        pickupInformation: {
          from: {
            code: 'PMI',
            description: 'Palma Majorca, Son Sant Joan Airport',
            type: 'IATA',
          },
          to: {
            code: '1523',
            description: 'Visit Hotel Alexandra',
            type: 'ATLAS',
          },
          date: '2019-10-26',
          time: '10:00:00',
          pickup: {
            address: null,
            number: null,
            town: null,
            zip: null,
            description:
              'Once you have collected your luggage, leave the baggage area through door B, turn right to proceed to the Destination Services airport Desk which is the last desk in the arrival area, located in front of column number 1.  Here you will be attendend and escorted to the transfer vehicle.-If you are unable to locate the driver/agent, please call DESTINATION SERVICES on +34 971 922 694.Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.',
            altitude: null,
            latitude: 39.547654,
            longitude: 2.730388,
            checkPickup: {
              mustCheckPickupTime: false,
              url: null,
              hoursBeforeConsulting: null,
            },
            pickupId: 3941,
            stopName: 'PMI OFICINA AEROPUERTO',
            image: 'http://media.activitiesbank.com/pickup/maps/all.jpg',
          },
        },
        content: {
          vehicle: {
            code: 'MNBS',
            name: 'Minibus',
          },
          category: {
            code: 'STND',
            name: 'Standard',
          },
          images: [
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/small/prvt-stnd-mnbs.png',
              type: 'SMALL',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/medium/prvt-stnd-mnbs.png',
              type: 'MEDIUM'
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/large/prvt-stnd-mnbs.png',
              type: 'LARGE'
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/extralarge/prvt-stnd-mnbs.png',
              type: 'EXTRALARGE'
            }
          ],
          transferDetailInfo: [
            {
              id: 'ER',
              name: 'Exclusive ride for you',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'DTDS',
              name: 'Door to door service',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'AV247',
              name: 'Available 24/7',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'BA',
              name:
                '1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'SPLU',
              name: 'SPECIAL LUGGAGE',
              description:
                'In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'DRVR',
              name: "CAN'T FIND DRIVER",
              description:
                'In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'CHIN',
              name: 'CHECK INFORMATION',
              description:
                'If the details do not correspond with the reservation, please contact your agency immediately.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'CBBS',
              name: 'CHILDBOOSTER / BABY SEAT',
              description:
                'Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'LGPR',
              name: 'LUGGAGE PROBLEMS',
              description:
                'In the event of a delay on your flight or a problem with customs or luggage,  please call the emergency number in order to advise of the delay and take the necessary steps.',
              type: 'GENERIC_GUIDELINES',
            }
          ],
          customerTransferTimeInfo: [
            {
              value: 0,
              type: 'CUSTOMER_MAX_WAITING_TIME',
              metric: 'minutes',
            }
          ],
          supplierTransferTimeInfo: [
            {
              value: 60,
              type: 'SUPPLIER_MAX_WAITING_TIME_DOMESTIC',
              metric: 'minutes',
            },
            {
              value: 90,
              type: 'SUPPLIER_MAX_WAITING_TIME_INTERNATIONAL',
              metric: 'minutes',
            }
          ],
          transferRemarks: [
            {
              type: 'CONTRACT',
              description:
                'Pick-up point\nOnce you have collected your luggage, leave the baggage area through door B, turn right to proceed to the Destination Services airport Desk which is the last desk in the arrival area, located in front of column number 1.  Here you will be attendend and escorted to the transfer vehicle.-If you are unable to locate the driver/agent, please call DESTINATION SERVICES on +34 971 922 694.Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.\n\nTransfer information:\n\n* Maximum client waiting time 0 minutes\n* Maximum waiting time for drivers in domestic arrivals 60 minutes\n* Maximum waiting time for drivers in international arrivals 90 minutes\n\n\n* In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.* In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.* If the details do not correspond with the reservation, please contact your agency immediately.* Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.* In the event of a delay on your flight or a problem with customs or luggage,  please call the emergency number in order to advise of the delay and take the necessary steps.\n\n* Exclusive ride for you* Door to door service* Available 24/7* 1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm\n\n',
              mandatory: true
            }
          ]
        },
        price: {
          totalAmount: 88.29,
          netAmount: 76.64,
          currencyId: 'EUR'
        },
        rateKey:
          'ARRIVAL|IATA|PMI|ATLAS|1523|2019-10-26T10:00|10~1~1|16|108470|TAB-PVT-BUS-M|108470|TAB-PVT-BUS-M|1|PRVT|M|MNBS|STND|88.29|AEROPUERTO|CPASTILLA|3941|2212|PMI',
        cancellationPolicies: [
          {
            amount: 88.29,
            from: '2019-10-24T23:59:00',
            currencyId: 'EUR'
          }
        ]
      },
      {
        id: '_2',
        direction: 'RETURN',
        transferType: 'PRIVATE',
        pickupInformation: {
          from: {
            code: 'PMI',
            description: 'Palma Majorca, Son Sant Joan Airport',
            type: 'IATA',
          },
          to: {
            code: '1523',
            description: 'Visit Hotel Alexandra',
            type: 'ATLAS',
          },
          date: '2019-10-26',
          time: '10:00:00',
          pickup: {
            address: null,
            number: null,
            town: null,
            zip: null,
            description:
              'Once you have collected your luggage, leave the baggage area through door B, turn right to proceed to the Destination Services airport Desk which is the last desk in the arrival area, located in front of column number 1.  Here you will be attendend and escorted to the transfer vehicle.-If you are unable to locate the driver/agent, please call DESTINATION SERVICES on +34 971 922 694.Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.',
            altitude: null,
            latitude: 39.547654,
            longitude: 2.730388,
            checkPickup: {
              mustCheckPickupTime: false,
              url: null,
              hoursBeforeConsulting: null,
            },
            pickupId: 3941,
            stopName: 'PMI OFICINA AEROPUERTO',
            image: 'http://media.activitiesbank.com/pickup/maps/all.jpg',
          },
        },
        content: {
          vehicle: {
            code: 'MNBS',
            name: 'Minibus',
          },
          category: {
            code: 'STND',
            name: 'Standard',
          },
          images: [
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/small/prvt-stnd-mnbs.png',
              type: 'SMALL',
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/medium/prvt-stnd-mnbs.png',
              type: 'MEDIUM'
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/large/prvt-stnd-mnbs.png',
              type: 'LARGE'
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/extralarge/prvt-stnd-mnbs.png',
              type: 'EXTRALARGE'
            }
          ],
          transferDetailInfo: [
            {
              id: 'ER',
              name: 'Exclusive ride for you',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'DTDS',
              name: 'Door to door service',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'AV247',
              name: 'Available 24/7',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'BA',
              name:
                '1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'SPLU',
              name: 'SPECIAL LUGGAGE',
              description:
                'In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'DRVR',
              name: "CAN'T FIND DRIVER",
              description:
                'In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'CHIN',
              name: 'CHECK INFORMATION',
              description:
                'If the details do not correspond with the reservation, please contact your agency immediately.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'CBBS',
              name: 'CHILDBOOSTER / BABY SEAT',
              description:
                'Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'LGPR',
              name: 'LUGGAGE PROBLEMS',
              description:
                'In the event of a delay on your flight or a problem with customs or luggage,  please call the emergency number in order to advise of the delay and take the necessary steps.',
              type: 'GENERIC_GUIDELINES',
            }
          ],
          customerTransferTimeInfo: [
            {
              value: 0,
              type: 'CUSTOMER_MAX_WAITING_TIME',
              metric: 'minutes',
            }
          ],
          supplierTransferTimeInfo: [
            {
              value: 60,
              type: 'SUPPLIER_MAX_WAITING_TIME_DOMESTIC',
              metric: 'minutes',
            },
            {
              value: 90,
              type: 'SUPPLIER_MAX_WAITING_TIME_INTERNATIONAL',
              metric: 'minutes',
            }
          ],
          transferRemarks: [
            {
              type: 'CONTRACT',
              description:
                'Pick-up point\nOnce you have collected your luggage, leave the baggage area through door B, turn right to proceed to the Destination Services airport Desk which is the last desk in the arrival area, located in front of column number 1.  Here you will be attendend and escorted to the transfer vehicle.-If you are unable to locate the driver/agent, please call DESTINATION SERVICES on +34 971 922 694.Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.\n\nTransfer information:\n\n* Maximum client waiting time 0 minutes\n* Maximum waiting time for drivers in domestic arrivals 60 minutes\n* Maximum waiting time for drivers in international arrivals 90 minutes\n\n\n* In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.* In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.* If the details do not correspond with the reservation, please contact your agency immediately.* Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.* In the event of a delay on your flight or a problem with customs or luggage,  please call the emergency number in order to advise of the delay and take the necessary steps.\n\n* Exclusive ride for you* Door to door service* Available 24/7* 1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm\n\n',
              mandatory: true
            }
          ]
        },
        price: {
          totalAmount: 88.29,
          netAmount: 76.64,
          currencyId: 'EUR'
        },
        rateKey:
          'ARRIVAL|IATA|PMI|ATLAS|1523|2019-10-26T10:00|10~1~1|16|108470|TAB-PVT-BUS-M|108470|TAB-PVT-BUS-M|1|PRVT|M|MNBS|STND|88.29|AEROPUERTO|CPASTILLA|3941|2212|PMI',
        cancellationPolicies: [
          {
            amount: 88.29,
            from: '2019-10-24T23:59:00',
            currencyId: 'EUR'
          }
        ]
      },
      {
        id: '_3',
        direction: 'RETURN',
        transferType: 'SHARED',
        pickupInformation: {
          from: {
            code: 'PMI',
            description: 'Palma Majorca, Son Sant Joan Airport',
            type: 'IATA'
          },
          to: {
            code: '1523',
            description: 'Visit Hotel Alexandra',
            type: 'ATLAS',
          },
          date: '2019-10-26',
          time: '05:00:00',
          pickup: {
            address: null,
            number: null,
            town: null,
            zip: null,
            description:
              'Once you have collected your luggage, proceed to the Transunion counter located at the arrivals hall in front of exit D. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694. Languages spoken at the call centre: English, German, Spanish Please do not leave the pick-up area without having contacted the agent/driver first.',
            altitude: null,
            latitude: 39.547654,
            longitude: 2.730388,
            checkPickup: {
              mustCheckPickupTime: false,
              url: null,
              hoursBeforeConsulting: null
            },
            pickupId: 3942,
            stopName: 'PMI OFICINA AEROPUERTO SHARED',
            image: 'http://media.activitiesbank.com/pickup/maps/PMI-shared2.jpg'
          }
        },
        content: {
          vehicle: {
            code: 'BSEB',
            name: 'Bus bicycle carriage included'
          },
          category: {
            code: 'STND',
            name: 'Standard'
          },
          images: [
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/small/shrd-stnd-bseb.png',
              type: 'SMALL'
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/medium/shrd-stnd-bseb.png',
              type: 'MEDIUM'
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/large/shrd-stnd-bseb.png',
              type: 'LARGE'
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/extralarge/shrd-stnd-bseb.png',
              type: 'EXTRALARGE'
            }
          ],
          transferDetailInfo: [
            {
              id: 'MST',
              name: 'Maximum',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'BAHB',
              name: '1 item of hand baggage allowed per person',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'BA',
              name:
                '1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'SPLU',
              name: 'SPECIAL LUGGAGE',
              description:
                'In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'MTPT',
              name: "CAN'T FIND MEETING POINT",
              description:
                'In the event of being unable to locate the meeting point, please call the emergency number indicated in this voucher.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'LGPR',
              name: 'LUGGAGE PROBLEMS',
              description:
                'In the event of a delay on your flight or a problem with customs or luggage,  please call the emergency number in order to advise of the delay and take the necessary steps.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'CHIN',
              name: 'CHECK INFORMATION',
              description:
                'If the details do not correspond with the reservation, please contact your agency immediately.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'CBBS',
              name: 'CHILDBOOSTER / BABY SEAT',
              description:
                'Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'RESTACCESS',
              name: 'restricted access',
              description:
                'Please note that due to traffic restrictions within the city centre there are some hotels in which you will be dropped as close as possible with the chance of a short walk, instead of directly outside.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'DOMEST',
              name: 'Domestic arrivals',
              description:
                'Domestic arrivals: Journeys made within the borders of the same country or European airspace (Schengen) or from Schengen space to any of these countries: Bulgaria, Cyprus, Croatia, Ireland, United Kingdom and Romania. Passengers do not have to go through customs controll',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'INTERNAT',
              name: 'International arrivals',
              description:
                'International arrivals: Journeys made with the origin outside of European airspace (Schengen) and arrival into it. Passengers must pass through customs control.',
              type: 'GENERIC_GUIDELINES',
            }
          ],
          customerTransferTimeInfo: [
            {
              value: 60,
              type: 'CUSTOMER_MAX_WAITING_TIME',
              metric: 'minutes',
            }
          ],
          supplierTransferTimeInfo: [
            {
              value: 60,
              type: 'SUPPLIER_MAX_WAITING_TIME_DOMESTIC',
              metric: 'minutes',
            },
            {
              value: 90,
              type: 'SUPPLIER_MAX_WAITING_TIME_INTERNATIONAL',
              metric: 'minutes',
            }
          ],
          transferRemarks: [
            {
              type: 'CONTRACT',
              description:
                'Pick-up point\nOnce you have collected your luggage, proceed to the Transunion counter located at the arrivals hall in front of exit D. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694. Languages spoken at the call centre: English, German, Spanish Please do not leave the pick-up area without having contacted the agent/driver first.\n\nTransfer information:\n\n* Maximum client waiting time 60 minutes\n* Maximum waiting time for drivers in domestic arrivals 60 minutes\n* Maximum waiting time for drivers in international arrivals 90 minutes\n\n\n* In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.* In the event of being unable to locate the meeting point, please call the emergency number indicated in this voucher.* In the event of a delay on your flight or a problem with customs or luggage,  please call the emergency number in order to advise of the delay and take the necessary steps.* If the details do not correspond with the reservation, please contact your agency immediately.* Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.* Please note that due to traffic restrictions within the city centre there are some hotels in which you will be dropped as close as possible with the chance of a short walk, instead of directly outside.* Domestic arrivals: Journeys made within the borders of the same country or European airspace (Schengen) or from Schengen space to any of these countries: Bulgaria, Cyprus, Croatia, Ireland, United Kingdom and Romania. Passengers do not have to go through customs controll* International arrivals: Journeys made with the origin outside of European airspace (Schengen) and arrival into it. Passengers must pass through customs control.\n\n* Maximum* 1 item of hand baggage allowed per person* 1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm\n\n',
              mandatory: true
            }
          ]
        },
        price: {
          totalAmount: 167.4,
          netAmount: 145.3,
          currencyId: 'EUR'
        },
        rateKey:
          'ARRIVAL|IATA|PMI|ATLAS|1523|2019-10-26T05:00|10~1~1|999|108466|TAB-SHT-XBIKE-1|108466|TAB-SHT-XBIKE-1|1|SHRD|1|BSEB|STND|167.4|AEROPUERTO|CPASTILLA|3942|218|PMI',
        cancellationPolicies: [
          {
            amount: 167.44,
            from: '2019-10-23T23:59:00',
            currencyId: 'EUR'
          }
        ]
      },
      {
        id: '_4',
        direction: 'RETURN',
        transferType: 'SHARED',
        pickupInformation: {
          from: {
            code: 'PMI',
            description: 'Palma Majorca, Son Sant Joan Airport',
            type: 'IATA'
          },
          to: {
            code: '1523',
            description: 'Visit Hotel Alexandra',
            type: 'ATLAS'
          },
          date: '2019-10-26',
          time: '05:00:00',
          pickup: {
            address: null,
            number: null,
            town: null,
            zip: null,
            description:
              'Once you have collected your luggage, proceed to the Transunion counter located at the arrivals hall in front of exit D. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694. Languages spoken at the call centre: English, German, Spanish Please do not leave the pick-up area without having contacted the agent/driver first.',
            altitude: null,
            latitude: 39.547654,
            longitude: 2.730388,
            checkPickup: {
              mustCheckPickupTime: false,
              url: null,
              hoursBeforeConsulting: null
            },
            pickupId: 3942,
            stopName: 'PMI OFICINA AEROPUERTO SHARED',
            image: 'http://media.activitiesbank.com/pickup/maps/PMI-shared2.jpg'
          }
        },
        content: {
          vehicle: {
            code: 'BS',
            name: 'Bus'
          },
          category: {
            code: 'STND',
            name: 'Standard'
          },
          images: [
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/small/shrd-stnd-bs.png',
              type: 'SMALL'
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/medium/shrd-stnd-bs.png',
              type: 'MEDIUM'
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/large/shrd-stnd-bs.png',
              type: 'LARGE'
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/extralarge/shrd-stnd-bs.png',
              type: 'EXTRALARGE'
            }
          ],
          transferDetailInfo: [
            {
              id: 'MST',
              name: 'Maximum',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'BAHB',
              name: '1 item of hand baggage allowed per person',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'BA',
              name:
                '1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'CHIN',
              name: 'CHECK INFORMATION',
              description:
                'If the details do not correspond with the reservation, please contact your agency immediately.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'DRVR',
              name: "CAN'T FIND DRIVER",
              description:
                'In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'DOMEST',
              name: 'Domestic arrivals',
              description:
                'Domestic arrivals: Journeys made within the borders of the same country or European airspace (Schengen) or from Schengen space to any of these countries: Bulgaria, Cyprus, Croatia, Ireland, United Kingdom and Romania. Passengers do not have to go through customs controll',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'INTERNAT',
              name: 'International arrivals',
              description:
                'International arrivals: Journeys made with the origin outside of European airspace (Schengen) and arrival into it. Passengers must pass through customs control.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'LGPR',
              name: 'LUGGAGE PROBLEMS',
              description:
                'In the event of a delay on your flight or a problem with customs or luggage,  please call the emergency number in order to advise of the delay and take the necessary steps.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'BCNACCESS',
              name: 'Bus restricted access',
              description:
                'Some hotels have restricted bus access, in these cases costumers must follow indications as arrivals and departures will be from a meeting point.',
              type: 'GENERIC_GUIDELINES',
            }
          ],
          customerTransferTimeInfo: [
            {
              value: 60,
              type: 'CUSTOMER_MAX_WAITING_TIME',
              metric: 'minutes',
            }
          ],
          supplierTransferTimeInfo: [
            {
              value: 60,
              type: 'SUPPLIER_MAX_WAITING_TIME_DOMESTIC',
              metric: 'minutes',
            },
            {
              value: 90,
              type: 'SUPPLIER_MAX_WAITING_TIME_INTERNATIONAL',
              metric: 'minutes',
            }
          ],
          transferRemarks: [
            {
              type: 'CONTRACT',
              description:
                'Pick-up point\nOnce you have collected your luggage, proceed to the Transunion counter located at the arrivals hall in front of exit D. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694. Languages spoken at the call centre: English, German, Spanish Please do not leave the pick-up area without having contacted the agent/driver first.\n\nTransfer information:\n\n* Maximum client waiting time 60 minutes\n* Maximum waiting time for drivers in domestic arrivals 60 minutes\n* Maximum waiting time for drivers in international arrivals 90 minutes\n\n\n* If the details do not correspond with the reservation, please contact your agency immediately.* In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.* Domestic arrivals: Journeys made within the borders of the same country or European airspace (Schengen) or from Schengen space to any of these countries: Bulgaria, Cyprus, Croatia, Ireland, United Kingdom and Romania. Passengers do not have to go through customs controll* International arrivals: Journeys made with the origin outside of European airspace (Schengen) and arrival into it. Passengers must pass through customs control.* In the event of a delay on your flight or a problem with customs or luggage,  please call the emergency number in order to advise of the delay and take the necessary steps.* Some hotels have restricted bus access, in these cases costumers must follow indications as arrivals and departures will be from a meeting point.\n\n* Maximum* 1 item of hand baggage allowed per person* 1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm\n\n',
              mandatory: true
            }
          ]
        },
        price: {
          totalAmount: 81.6,
          netAmount: 70.83,
          currencyId: 'EUR'
        },
        rateKey:
          'ARRIVAL|IATA|PMI|ATLAS|1523|2019-10-26T05:00|10~1~1|999|108462|TAB-SHT-STD-B|108462|TAB-SHT-STD-B|1|SHRD|B|BS|STND|81.6|AEROPUERTO|CPASTILLA|3942|2143|PMI',
        cancellationPolicies: [
          {
            amount: 81.57,
            from: '2019-10-24T23:59:00',
            currencyId: 'EUR'
          }
        ]
      },
      {
        id: '_5',
        direction: 'RETURN',
        transferType: 'SHARED',
        pickupInformation: {
          from: {
            code: 'PMI',
            description: 'Palma Majorca, Son Sant Joan Airport',
            type: 'IATA'
          },
          to: {
            code: '1523',
            description: 'Visit Hotel Alexandra',
            type: 'ATLAS'
          },
          date: '2019-10-26',
          time: '05:00:00',
          pickup: {
            address: null,
            number: null,
            town: null,
            zip: null,
            description:
              'Once you have collected your luggage, proceed to the Transunion counter located at the arrivals hall in front of exit D. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694. Languages spoken at the call centre: English, German, Spanish Please do not leave the pick-up area without having contacted the agent/driver first.',
            altitude: null,
            latitude: 39.547654,
            longitude: 2.730388,
            checkPickup: {
              mustCheckPickupTime: false,
              url: null,
              hoursBeforeConsulting: null
            },
            pickupId: 3942,
            stopName: 'PMI OFICINA AEROPUERTO SHARED',
            image: 'http://media.activitiesbank.com/pickup/maps/PMI-shared2.jpg'
          }
        },
        content: {
          vehicle: {
            code: 'BSEB',
            name: 'Bus bicycle carriage included'
          },
          category: {
            code: 'STND',
            name: 'Standard'
          },
          images: [
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/small/shrd-stnd-bseb.png',
              type: 'SMALL'
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/medium/shrd-stnd-bseb.png',
              type: 'MEDIUM'
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/large/shrd-stnd-bseb.png',
              type: 'LARGE'
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/extralarge/shrd-stnd-bseb.png',
              type: 'EXTRALARGE'
            }
          ],
          transferDetailInfo: [
            {
              id: 'MST',
              name: 'Maximum',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'BAHB',
              name: '1 item of hand baggage allowed per person',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'BA',
              name:
                '1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'SPLU',
              name: 'SPECIAL LUGGAGE',
              description:
                'In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'MTPT',
              name: "CAN'T FIND MEETING POINT",
              description:
                'In the event of being unable to locate the meeting point, please call the emergency number indicated in this voucher.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'LGPR',
              name: 'LUGGAGE PROBLEMS',
              description:
                'In the event of a delay on your flight or a problem with customs or luggage,  please call the emergency number in order to advise of the delay and take the necessary steps.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'CHIN',
              name: 'CHECK INFORMATION',
              description:
                'If the details do not correspond with the reservation, please contact your agency immediately.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'CBBS',
              name: 'CHILDBOOSTER / BABY SEAT',
              description:
                'Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'RESTACCESS',
              name: 'restricted access',
              description:
                'Please note that due to traffic restrictions within the city centre there are some hotels in which you will be dropped as close as possible with the chance of a short walk, instead of directly outside.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'DOMEST',
              name: 'Domestic arrivals',
              description:
                'Domestic arrivals: Journeys made within the borders of the same country or European airspace (Schengen) or from Schengen space to any of these countries: Bulgaria, Cyprus, Croatia, Ireland, United Kingdom and Romania. Passengers do not have to go through customs controll',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'INTERNAT',
              name: 'International arrivals',
              description:
                'International arrivals: Journeys made with the origin outside of European airspace (Schengen) and arrival into it. Passengers must pass through customs control.',
              type: 'GENERIC_GUIDELINES',
            },
          ],
          customerTransferTimeInfo: [
            {
              value: 60,
              type: 'CUSTOMER_MAX_WAITING_TIME',
              metric: 'minutes',
            },
          ],
          supplierTransferTimeInfo: [
            {
              value: 60,
              type: 'SUPPLIER_MAX_WAITING_TIME_DOMESTIC',
              metric: 'minutes',
            },
            {
              value: 90,
              type: 'SUPPLIER_MAX_WAITING_TIME_INTERNATIONAL',
              metric: 'minutes',
            }
          ],
          transferRemarks: [
            {
              type: 'CONTRACT',
              description:
                'Pick-up point\nOnce you have collected your luggage, proceed to the Transunion counter located at the arrivals hall in front of exit D. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694. Languages spoken at the call centre: English, German, Spanish Please do not leave the pick-up area without having contacted the agent/driver first.\n\nTransfer information:\n\n* Maximum client waiting time 60 minutes\n* Maximum waiting time for drivers in domestic arrivals 60 minutes\n* Maximum waiting time for drivers in international arrivals 90 minutes\n\n\n* In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.* In the event of being unable to locate the meeting point, please call the emergency number indicated in this voucher.* In the event of a delay on your flight or a problem with customs or luggage,  please call the emergency number in order to advise of the delay and take the necessary steps.* If the details do not correspond with the reservation, please contact your agency immediately.* Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.* Please note that due to traffic restrictions within the city centre there are some hotels in which you will be dropped as close as possible with the chance of a short walk, instead of directly outside.* Domestic arrivals: Journeys made within the borders of the same country or European airspace (Schengen) or from Schengen space to any of these countries: Bulgaria, Cyprus, Croatia, Ireland, United Kingdom and Romania. Passengers do not have to go through customs controll* International arrivals: Journeys made with the origin outside of European airspace (Schengen) and arrival into it. Passengers must pass through customs control.\n\n* Maximum* 1 item of hand baggage allowed per person* 1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm\n\n',
              mandatory: true
            }
          ]
        },
        price: {
          totalAmount: 167.4,
          netAmount: 145.3,
          currencyId: 'EUR'
        },
        rateKey:
          'ARRIVAL|IATA|PMI|ATLAS|1523|2019-10-26T05:00|10~1~1|999|107147|TAB SHR XBIKE|107147|TAB SHR XBIKE|1|SHRD|1|BSEB|STND|167.4|AEROPUERTO|CPASTILLA|3942|218|PMI',
        cancellationPolicies: [
          {
            amount: 167.44,
            from: '2019-10-23T23:59:00',
            currencyId: 'EUR'
          }
        ]
      },
      {
        id: '_6',
        direction: 'RETURN',
        transferType: 'SHARED',
        pickupInformation: {
          from: {
            code: 'PMI',
            description: 'Palma Majorca, Son Sant Joan Airport',
            type: 'IATA'
          },
          to: {
            code: '1523',
            description: 'Visit Hotel Alexandra',
            type: 'ATLAS'
          },
          date: '2019-10-26',
          time: '05:00:00',
          pickup: {
            address: null,
            number: null,
            town: null,
            zip: null,
            description:
              'Once you have collected your luggage, proceed to the Transunion counter located at the arrivals hall in front of exit D. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694. Languages spoken at the call centre: English, German, Spanish Please do not leave the pick-up area without having contacted the agent/driver first.',
            altitude: null,
            latitude: 39.547654,
            longitude: 2.730388,
            checkPickup: {
              mustCheckPickupTime: false,
              url: null,
              hoursBeforeConsulting: null
            },
            pickupId: 3942,
            stopName: 'PMI OFICINA AEROPUERTO SHARED',
            image: 'http://media.activitiesbank.com/pickup/maps/PMI-shared2.jpg'
          }
        },
        content: {
          vehicle: {
            code: 'BS',
            name: 'Bus'
          },
          category: {
            code: 'STND',
            name: 'Standard'
          },
          images: [
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/small/shrd-stnd-bs.png',
              type: 'SMALL'
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/medium/shrd-stnd-bs.png',
              type: 'MEDIUM'
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/large/shrd-stnd-bs.png',
              type: 'LARGE'
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/extralarge/shrd-stnd-bs.png',
              type: 'EXTRALARGE'
            }
          ],
          transferDetailInfo: [
            {
              id: 'MST',
              name: 'Maximum',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'BAHB',
              name: '1 item of hand baggage allowed per person',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'BA',
              name:
                '1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'CHIN',
              name: 'CHECK INFORMATION',
              description:
                'If the details do not correspond with the reservation, please contact your agency immediately.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'DRVR',
              name: "CAN'T FIND DRIVER",
              description:
                'In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'DOMEST',
              name: 'Domestic arrivals',
              description:
                'Domestic arrivals: Journeys made within the borders of the same country or European airspace (Schengen) or from Schengen space to any of these countries: Bulgaria, Cyprus, Croatia, Ireland, United Kingdom and Romania. Passengers do not have to go through customs controll',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'INTERNAT',
              name: 'International arrivals',
              description:
                'International arrivals: Journeys made with the origin outside of European airspace (Schengen) and arrival into it. Passengers must pass through customs control.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'LGPR',
              name: 'LUGGAGE PROBLEMS',
              description:
                'In the event of a delay on your flight or a problem with customs or luggage,  please call the emergency number in order to advise of the delay and take the necessary steps.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'BCNACCESS',
              name: 'Bus restricted access',
              description:
                'Some hotels have restricted bus access, in these cases costumers must follow indications as arrivals and departures will be from a meeting point.',
              type: 'GENERIC_GUIDELINES',
            }
          ],
          customerTransferTimeInfo: [
            {
              value: 60,
              type: 'CUSTOMER_MAX_WAITING_TIME',
              metric: 'minutes',
            }
          ],
          supplierTransferTimeInfo: [
            {
              value: 60,
              type: 'SUPPLIER_MAX_WAITING_TIME_DOMESTIC',
              metric: 'minutes',
            },
            {
              value: 90,
              type: 'SUPPLIER_MAX_WAITING_TIME_INTERNATIONAL',
              metric: 'minutes',
            }
          ],
          transferRemarks: [
            {
              type: 'CONTRACT',
              description:
                'Pick-up point\nOnce you have collected your luggage, proceed to the Transunion counter located at the arrivals hall in front of exit D. If you are unable to locate the driver/agent, please call Destination Services on +34 971 922 694. Languages spoken at the call centre: English, German, Spanish Please do not leave the pick-up area without having contacted the agent/driver first.\n\nTransfer information:\n\n* Maximum client waiting time 60 minutes\n* Maximum waiting time for drivers in domestic arrivals 60 minutes\n* Maximum waiting time for drivers in international arrivals 90 minutes\n\n\n* If the details do not correspond with the reservation, please contact your agency immediately.* In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.* Domestic arrivals: Journeys made within the borders of the same country or European airspace (Schengen) or from Schengen space to any of these countries: Bulgaria, Cyprus, Croatia, Ireland, United Kingdom and Romania. Passengers do not have to go through customs controll* International arrivals: Journeys made with the origin outside of European airspace (Schengen) and arrival into it. Passengers must pass through customs control.* In the event of a delay on your flight or a problem with customs or luggage,  please call the emergency number in order to advise of the delay and take the necessary steps.* Some hotels have restricted bus access, in these cases costumers must follow indications as arrivals and departures will be from a meeting point.\n\n* Maximum* 1 item of hand baggage allowed per person* 1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm\n\n',
              mandatory: true
            }
          ]
        },
        price: {
          totalAmount: 81.6,
          netAmount: 70.83,
          currencyId: 'EUR'
        },
        rateKey:
          'ARRIVAL|IATA|PMI|ATLAS|1523|2019-10-26T05:00|10~1~1|999|107146|TAB SHT STD|107146|TAB SHT STD|1|SHRD|B|BS|STND|81.6|AEROPUERTO|CPASTILLA|3942|2143|PMI',
        cancellationPolicies: [
          {
            amount: 81.57,
            from: '2019-10-24T23:59:00',
            currencyId: 'EUR'
          }
        ]
      },
      {
        id: '_7',
        direction: 'RETURN',
        transferType: 'PRIVATE',
        pickupInformation: {
          from: {
            code: 'PMI',
            description: 'Palma Majorca, Son Sant Joan Airport',
            type: 'IATA'
          },
          to: {
            code: '1523',
            description: 'Visit Hotel Alexandra',
            type: 'ATLAS'
          },
          date: '2019-10-26',
          time: '10:00:00',
          pickup: {
            address: null,
            number: null,
            town: null,
            zip: null,
            description:
              'Once you have collected your luggage, leave the baggage area through door B, turn right to proceed to the Destination Services airport Desk which is the last desk in the arrival area, located in front of column number 1.  Here you will be attendend and escorted to the transfer vehicle.-If you are unable to locate the driver/agent, please call DESTINATION SERVICES on +34 971 922 694.Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.',
            altitude: null,
            latitude: 39.547654,
            longitude: 2.730388,
            checkPickup: {
              mustCheckPickupTime: false,
              url: null,
              hoursBeforeConsulting: null
            },
            pickupId: 3941,
            stopName: 'PMI OFICINA AEROPUERTO',
            image: 'http://media.activitiesbank.com/pickup/maps/all.jpg'
          }
        },
        content: {
          vehicle: {
            code: 'MNBS',
            name: 'Minibus'
          },
          category: {
            code: 'STND',
            name: 'Standard'
          },
          images: [
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/small/prvt-stnd-mnbs.png',
              type: 'SMALL'
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/medium/prvt-stnd-mnbs.png',
              type: 'MEDIUM'
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/large/prvt-stnd-mnbs.png',
              type: 'LARGE'
            },
            {
              url:
                'http://media.stage.activitiesbank.com/giata/transfers/TRD/extralarge/prvt-stnd-mnbs.png',
              type: 'EXTRALARGE'
            }
          ],
          transferDetailInfo: [
            {
              id: 'ER',
              name: 'Exclusive ride for you',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'DTDS',
              name: 'Door to door service',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'AV247',
              name: 'Available 24/7',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'BA',
              name:
                '1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm',
              description: null,
              type: 'GENERAL_INFO',
            },
            {
              id: 'SPLU',
              name: 'SPECIAL LUGGAGE',
              description:
                'In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'DRVR',
              name: "CAN'T FIND DRIVER",
              description:
                'In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'CHIN',
              name: 'CHECK INFORMATION',
              description:
                'If the details do not correspond with the reservation, please contact your agency immediately.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'CBBS',
              name: 'CHILDBOOSTER / BABY SEAT',
              description:
                'Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.',
              type: 'GENERIC_GUIDELINES',
            },
            {
              id: 'LGPR',
              name: 'LUGGAGE PROBLEMS',
              description:
                'In the event of a delay on your flight or a problem with customs or luggage,  please call the emergency number in order to advise of the delay and take the necessary steps.',
              type: 'GENERIC_GUIDELINES',
            }
          ],
          customerTransferTimeInfo: [
            {
              value: 0,
              type: 'CUSTOMER_MAX_WAITING_TIME',
              metric: 'minutes',
            }
          ],
          supplierTransferTimeInfo: [
            {
              value: 60,
              type: 'SUPPLIER_MAX_WAITING_TIME_DOMESTIC',
              metric: 'minutes',
            },
            {
              value: 90,
              type: 'SUPPLIER_MAX_WAITING_TIME_INTERNATIONAL',
              metric: 'minutes',
            }
          ],
          transferRemarks: [
            {
              type: 'CONTRACT',
              description:
                'Pick-up point\nOnce you have collected your luggage, leave the baggage area through door B, turn right to proceed to the Destination Services airport Desk which is the last desk in the arrival area, located in front of column number 1.  Here you will be attendend and escorted to the transfer vehicle.-If you are unable to locate the driver/agent, please call DESTINATION SERVICES on +34 971 922 694.Languages spoken at the call centre: English, Spanish. Please do not leave the pick-up area without having contacted the agent/driver first.\n\nTransfer information:\n\n* Maximum client waiting time 0 minutes\n* Maximum waiting time for drivers in domestic arrivals 60 minutes\n* Maximum waiting time for drivers in international arrivals 90 minutes\n\n\n* In the event of extra luggage or sport equipment being checked in, please contact us, as this may carry an extra charge.* In the event of being unable to locate the driver, please call the emergency number indicated in this voucher.* If the details do not correspond with the reservation, please contact your agency immediately.* Child car seats and boosters are not included unless specified in your booking and can carry an extra cost. Should you need to book them, please contact your point of sale prior to travelling.* In the event of a delay on your flight or a problem with customs or luggage,  please call the emergency number in order to advise of the delay and take the necessary steps.\n\n* Exclusive ride for you* Door to door service* Available 24/7* 1 piece of baggage allowed per person ( max.dimensions 158cm) length+width+height=158cm\n\n',
              mandatory: true
            }
          ]
        },
        price: {
          totalAmount: 87.83,
          netAmount: 76.24,
          currencyId: 'EUR'
        },
        rateKey:
          'ARRIVAL|IATA|PMI|ATLAS|1523|2019-10-26T10:00|10~1~1|16|107141|TAB PVT BUS|107141|TAB PVT BUS|1|PRVT|M|MNBS|STND|87.83|AEROPUERTO|CPASTILLA|3941|2212|PMI',
        cancellationPolicies: [
          {
            amount: 87.83,
            from: '2019-10-23T23:59:00',
            currencyId: 'EUR'
          }
        ]
      }
    ]
  },
  errors: {}
};

module.exports = {
  searchDone,
};
