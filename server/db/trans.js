const translation = {
  status: 200,
  data: {
    ar: {
      cars: 'سيارات',
      flights: 'رحلات طيران',
      hotels: 'فنادق',
      hotelsandflights: 'طيران + فنادق',
      privacy: 'الخصوصية',
      'social media': 'تواصل إجتماعي',
      'terms & condtions': 'الشروط والاحكام',
      'who are we?': 'من نحن؟',
    },
    en: {
      cars: 'cars',
      flights: 'Flights ',
      hotels: 'Hotels',
      hotelsandflights: 'Hotels + Flights',
      privacy: 'Privacy',
      'social media': 'Social Media',
      'terms & condtions': 'Terms & Condtions',
      'who are we?': 'Who are we ?',
    },
  },
};

module.exports = {
  translation,
};
