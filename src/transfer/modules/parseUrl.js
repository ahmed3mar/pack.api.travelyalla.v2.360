let instance = null;

export default class ParseUrl {
  constructor() {
    this.params = [];
    this.schemaOneWay = `{from}-{from_code}/{to}-{to_code}/{outbound}/{outboundTime}/{adults}-{children}-{infants}`;
    this.schemaRoundTrip = `{from}-{from_code}/{to}-{to_code}/{outbound}/{outboundTime}/{inbound}/{inboundTime}/{adults}-{children}-{infants}`;

    if (instance) {
      return instance;
    }
  }

  toString(obj){
    console.log('obj', obj);

    let url = this.schemaOneWay;

    if(obj.inbound){
      url = this.schemaRoundTrip;
    }

    Object.keys(obj).map((k) => {
      let val = obj[k];
      try {
        val = obj[k].split(' ').join('');
      } catch (e) { }
      url = url.replace(`{${k}}`, val);
    });
    return url;
  }

  toObject(str){

    let param = str.split('/')
    let schema = this.schemaOneWay.split('/')
    let obj = {};

    if(param.length > 5){
      schema = this.schemaRoundTrip.split('/');
    }

    schema.map((s, i) => {
      let keys = s;
      let values = param[i];

      if(keys.search('-') > -1){
        keys = s.split('-');
        values = param[i].split('-');
      }

      if(typeof keys === 'object'){
        keys.map((key, ii) => {
          obj[key.replace(/([^a-zA-Z0-9_])/g, '')] = values[ii];
        })
        return;
      }

      obj[keys.replace(/([^a-zA-Z0-9_])/g, '')] = values;

    })

    return obj;
  }
}
