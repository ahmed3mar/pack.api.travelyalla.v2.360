let instance = null;

export default class ObservableAll {
  constructor() {
    this.observers = [];
    if (instance) {
      return instance;
    }
  }

  subscribe(fn) {
    this.observers.push(fn);
  }

  unsubscribe(fn) {
    this.observers = this.observers.filter(subscriber => subscriber !== fn);
  }

  notify(data) {
    this.observers.forEach(observer => observer(data));
  }
}
