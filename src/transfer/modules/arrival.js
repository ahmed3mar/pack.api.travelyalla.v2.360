import Transfer from '../index';


export default class Arrival {
  constructor(arrival) {
    this.arrival = arrival;
  }

  transfer(){
    return new Transfer();
  }

  data() {
    return `${this.arrival.name}, ${this.arrival.country}, ${this.arrival.country_code}`;
  }

  type() {
    return this.arrival.type;
  }

  async to() {
    const route = this.transfer().route('arrivals');
    return (await this.transfer().get(route, { name: this.arrival.name, code: this.arrival.code })
      .then(res => (res.data.status === 200 ? res.data.data : this.handleError())))
      .map(item => new Arrival(item));
  }
}
