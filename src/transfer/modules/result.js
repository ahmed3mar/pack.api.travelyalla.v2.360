/* eslint-disable class-methods-use-this */
import pipe from '../../common/pipe';
import getValueByObject from '../../common/getValueByObject';
import ObservableAll from './observableAll.js';

const observable = new ObservableAll();

let instance = null;
export default class Result {
  constructor() {
    this.data = [];

    this.filteredData = [];

    this.selected = null;

    this.observers = [];

    this.paginationinfo = {
      pageLimit: 15,
      pages: null,
      total: null,
      current: 1,
    };

    this.filtrationInfo = {
      transferType: new Set([]),
      price: new Set([]),
      category: new Set([]),
    };

    this.isCurrent = false;

    if (instance) {
      return instance;
    }
  }


  set isCurrentValue(val) {
    this.isCurrent = val
  }

  set setData(data) {
    this.data = data;
    this.setFiltrationInfo(data);
    if(this.isCurrent){
      this.run();
    }
  }


  /**
   * ANCHOR Srot Function
   * @author: yas
   * @desc
   * @param {object} params
   * params content
   * ---------------------------------------------------------
   *   name          type      required   default   example
   * ---------------------------------------------------------
   * - price         {string}  no         none      'DESC' || 'ASC'
   * ---------------------------------------------------------
   * @return {object} this
   */
  sort(params) {
    const keysPostion = {
      price: 'price.totalAmount',
    };

    Object.keys(params).map((k) => {
      const type = k;
      const value = params[k];
      this.data = this.data.sort((a, b) => {
        const dir = value === 'ASC' ? [a, b] : [b, a];
        return (
          parseInt(getValueByObject(dir[0], keysPostion[type]), 10)
          - parseInt(getValueByObject(dir[1], keysPostion[type]), 10));
      });
      return this;
    });
    return this;
  }


  /**
   * ANCHOR Filter Function
   * @author: yas
   * @desc
   * @param {object} params
   * params content
   * ---------------------------------------------------------
   *   name          type      required   default   example
   * ---------------------------------------------------------
   * - price         {object}  no         none      { max: 500, min: 0 }
   * ---------------------------------------------------------
   * @return {object} this
   */
  filter(params) {
    const keysPostion = {
      price: 'price.totalAmount',
      transferType: 'transferType',
      category: 'content.category.name'
    };

    this.data = [...this.filteredData, ...this.data];
    this.filteredData = [];

    Object.keys(params).map((k) => {
      const type = k;
      const value = params[k];

      this.data = this.data.filter((item) => {
        let val = getValueByObject(item, keysPostion[type]);
        if (typeof value === 'object' && type === 'price') {
          if(Math.floor(val) <= value.max && Math.ceil(val) >= value.min){
            return true;
          }else {
            this.filteredData.push(item);
            return false;
          }
        }

        if (type === 'transferType') {
          if(value.size === 0) return true;
          if(value.has(val)){
            return true;
          }else {
            this.filteredData.push(item);
            return false;
          }
        }

        if (type === 'category') {
          if(value.size === 0) return true;
          if(value.has(val)){
            return true;
          }else {
            this.filteredData.push(item);
            return false;
          }
        }
        return true;
      });
      return this;
    });

    return this;
  }


  /**
   * ANCHOR set filtration info
   * @author: yas
   * @desc
   * @param {object} params
   */
  setFiltrationInfo(data){

    this.filtrationInfo = {
      transferType: new Set([]),
      price: new Set([]),
      category: new Set([]),
    };

    data.map(d => {
      if(!this.filtrationInfo.transferType.has(d.transferType)){
        this.filtrationInfo.transferType.add(d.transferType)
      }

      if(!this.filtrationInfo.price.has(d.price.totalAmount)){
        this.filtrationInfo.price.add(d.price.totalAmount)
      }

      if(!this.filtrationInfo.category.has(d.content.category.name)){
        this.filtrationInfo.category.add(d.content.category.name)
      }
    });
    return this;
  }

  getFiltrationInfo(){
    return this.filtrationInfo;
  }


  /**
   * ANCHOR Pagination Function
   * @author: yas
   * @desc
   * @param {object || number || string} param
   * @param {number} page
   * param status
   *  string : 'next' || 'prev'
   *  number : page number
   *  object : pagination config
   * ---------------------------------------------------------
   *   name          type      required   default   example
   * ---------------------------------------------------------
   * - limit         {number}  yes        1         10
   * ---------------------------------------------------------
   * @return {*} this || currentData
   */
  pagination(param = 1, page) {
    const { pageLimit, current } = this.paginationinfo;

    let data = null;

    if (typeof param === 'object') {
      const { limit } = param;
      const total = this.data.length;
      this.paginationinfo = {
        pageLimit: limit,
        pages: Math.ceil(total / limit),
        total,
        current: 1,
      };
      data = page ? this.pagination(page) : this;
    }

    this.paginationinfo = {
      ...this.paginationinfo,
      total: this.data.length,
      pages: Math.ceil(this.data.length / this.paginationinfo.pageLimit),
    };

    if (typeof param === 'number') {
      this.paginationinfo = {
        ...this.paginationinfo,
        current: param,
      };

      if (param === 1) {
        data = [...this.data.slice(0, pageLimit)];
      }
      data = [...this.data.slice(((param - 1) * pageLimit), param * pageLimit)];
    }

    if (param === 'next') {
      this.paginationinfo.current += 1;
      data = this.pagination(current + 1);
    }

    if (param === 'prev') {
      this.paginationinfo.current -= 1;
      data = this.pagination(current - 1);
    }

    if (param === 'info') {
      data = this.paginationinfo;
    }

    if (param !== 'info') {
      this.notify(data);
    }

    return data;
  }


  /**
   * ANCHOR Process Function
   * @author: yas
   * @desc
   * @param {array} fns
   * @return {object} this
   */
  process(fns = []) {
    this.data = pipe(...fns)(this.data);
    return this;
  }


  /**
 * ANCHOR Run Function
 * @author: yas
 * @desc
 * @return {function} this.pagination
 */
  run() {
    return this.pagination();
  }


    /**
 * ANCHOR subscribeAll Function
 * @author: yas
 * @desc
 * @param {function} fn
 * @return {object} this
 */
  subscribeAll(fn) {
    observable.subscribe(fn);
    return this;
  }

  /**
 * ANCHOR Subscribe Function
 * @author: yas
 * @desc
 * @param {function} fn
 * @return {object} this
 */
  subscribe(fn) {
    this.observers.push(fn);
    return this;
  }


  /**
 * ANCHOR Unsubscribe Function
 * @author: yas
 * @desc
 * @param {function} fn
 * @return {object} this
 */
  unsubscribe(fn) {
    this.observers = this.observers.filter(subscriber => subscriber !== fn);
    observable.unsubscribe(fn);
    return this;
  }


  get selectedTrip(){
    return this.selected
  }

  select(obj){
    this.selected = obj
  }

  /**
 * ANCHOR notify Function
 * @author: yas
 * @desc
 * @return {object} this
 */
  notify(data) {
    this.observers.forEach(observer => observer(data));
    observable.notify(data);
    return this;
  }
}
