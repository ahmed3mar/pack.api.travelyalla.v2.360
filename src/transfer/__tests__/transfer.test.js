import Transfer from '../index';
import searchData from '../../../server/db/search';

const config = {
  name: 'transfer',
  url: 'http://localhost:5004/api',
  version: 1,
  data: { transfer: true },
  routes: { search: 'transfer/search', details: 'test', arrivals: 'arrivals' } };

const searchReq = {
  from: 'ATLAS',
  from_code: 1523,
  to: 'IATA',
  to_code: 'PMI',
  outbound: '2019-10-25',
  outboundTime: '5:00:00',
  inbound: '2019-10-26',
  inboundTime: '10:00:00',
  adults: 10,
  children: 1,
  infants: 1,
};

const dataFrom = searchData.searchDone.response.departure;
const dataTo = searchData.searchDone.response.return;
const original = {
  from: dataFrom,
  to: dataTo,
};

const transferInstanceFirst = new Transfer(config);
const transferInstanceLast = new Transfer(config);

describe('Transfer', () => {
  it('Test Transfer singleton class', () => {
    expect(transferInstanceFirst === transferInstanceLast).toEqual(true);
  });

  it('Test config', () => {
    expect(transferInstanceFirst.configObject).toEqual(config);
  });

  test('Test get arrivals : arrivals() && Test from()', () => transferInstanceFirst.arrivals('dub').then((res) => {
    expect(res[0].from()).toEqual({ dep_id: '6808', dep_name: 'Avari Dubai', dep_type: 'ATLAS' });
  }));

  test('Test get arrivals : arrivals() && Test to()', () => transferInstanceFirst.arrivals('dub').then((res) => {
    // result 'server/db/arrivals'
    expect(res[0].to('World')).toEqual([{
      arrival_id: 'DWC',
      arrival_name: 'Dubai World Central Airport',
      arrival_type: 'IATA',
    }]);
  }));

  test('Test the data fetch function : search()', async () => {
    const res = await transferInstanceFirst.search(searchReq);
    return expect(res.from.run()).toEqual(original.from);
  });

  it('Test get data : from && to && run()', async () => {
    await transferInstanceFirst.search(searchReq);
    expect(transferInstanceFirst.from.run()).toEqual(dataFrom);
    expect(transferInstanceFirst.to.run()).toEqual(dataTo);
    expect(transferInstanceFirst.to.run()).toEqual(dataTo);
  });

  it('Test sort function : sort()', async () => {
    await transferInstanceFirst.search(searchReq);
    const sortFromDESC = transferInstanceFirst.from.sort({ price: 'DESC' }).run();
    const sortToASC = transferInstanceFirst.to.sort({ price: 'DESC' }).run();
    expect(sortFromDESC[0].price.totalAmount)
      .toBeGreaterThanOrEqual(sortFromDESC[1].price.totalAmount);
    expect(sortToASC[0].price.totalAmount).toBeGreaterThanOrEqual(sortToASC[1].price.totalAmount);
  });

  it('Test filter function : filter()', async () => {
    await transferInstanceFirst.search(searchReq);
    const filterFrom = transferInstanceFirst
      .from
      .filter({ price: { max: 500, min: 0 } })
      .run();

    filterFrom.map((item) => {
      expect(item.price.totalAmount)
        .toBeGreaterThanOrEqual(0);
      expect(item.price.totalAmount)
        .toBeLessThanOrEqual(500);
      return false;
    });
  });

  it('Test pagination functions : pagination()', async () => {
    await transferInstanceFirst.search(searchReq);

    transferInstanceFirst.from
      .pagination({ limit: 2 }, 1);
    expect(transferInstanceFirst.from.pagination('info')).toEqual({ pageLimit: 2, pages: Math.ceil(dataFrom.length / 2), total: dataFrom.length, current: 1 });

    expect(transferInstanceFirst.from.pagination(3)).toEqual(dataFrom.slice(((3 - 1) * 2), 3 * 2));
    expect(transferInstanceFirst.from.pagination('next')).toEqual(dataFrom.slice(((4 - 1) * 2), 4 * 2));
    expect(transferInstanceFirst.from.pagination('prev')).toEqual(dataFrom.slice(((3 - 1) * 2), 3 * 2));
  });

  it('Test observable functions : subscribe()', async () => {
    const subscribeSearchTo = res => expect(res).toEqual(dataTo);
    const subscribeSearchFrom = res => expect(res).toEqual(dataFrom);
    const subscribeSortFrom = res => expect(res[0].price.totalAmount)
      .toBeGreaterThanOrEqual(res[1].price.totalAmount);

    transferInstanceFirst.to.subscribe(subscribeSearchTo);
    transferInstanceFirst.from.subscribe(subscribeSearchFrom);
    transferInstanceFirst.from.subscribe(subscribeSortFrom);

    await transferInstanceFirst.search(searchReq);
    transferInstanceFirst.from.sort({ price: 'DESC' });
  });
});
