import Api from '../api';
import Arrival from './modules/arrival';
import Result from './modules/result';
import config from '../config/transfer';
import ParseUrl from './modules/parseUrl';
import testData from './testData.js';

let instance = null;
export default class Transfer extends Api {
  constructor(props) {
    super({ ...config, ...props });

    if (instance) {
      return instance;
    }

    this.parseUrl = new ParseUrl();
    this.fromInstance = new Result();
    this.toInstance = new Result();
    this.current = 'from';

    this.paginationinfo = {
      pageLimit: 10,
      pages: null,
      total: null,
      current: 1,
    };

    this.arrivals = this.arrivals.bind(this);
    this.search = this.search.bind(this);
    this.book = this.book.bind(this);
    instance = this;
  }

  /**
   * ANCHOR Arrivals Function
   * @author: yas
   * @desc get arrivals data
   * @param {string} key
   * @return {array} Arrivals Array
   */
  async arrivals(param) {
    const route = this.route('arrivals');
    return (await this.get(route, param)
      .then(res => (res.data.status === 200 ? res.data.data : this.handleError(res))))
      .map(item => new Arrival(item));
  }

  /**
   * ANCHOR Search Function
   * @author: yas
   * @desc
   * @param {object} params
   * params content
   * ---------------------------------------------------------
   *   name          type      required   default   example
   * ---------------------------------------------------------
   * - from          {string}  yes        none     'ATLAS'
   * - from_code     {string}  yes        none     '1523'
   * - to            {string}  yes        none     'IATA'
   * - to_code       {string}  yes        none     'PMI'
   * - outbound      {string}  yes        none     '2019-10-25'
   * - outboundTime  {string}  yes        none     '5:00:00'
   * - inbound       {string}  no         none     '2019-10-26'
   * - inboundTime   {string}  no         none     '10:00:00'
   * - adults        {number}  yes        none     10
   * - children      {number}  yes        none     1
   * - infants       {number}  yes        none     1
   * ---------------------------------------------------------
   * @return {object} original data
   */
  async search(params) {
    try {
      const route = this.route('search');
      const res = await this.post(route, params);
      if (res.status === 200) {
        const data = res.status ? res.data.data : this.handleError();
        this.fromInstance.setData = data.departure;
        this.toInstance.setData = data.return;
        return {
          from: this.fromInstance,
          to: this.toInstance,
        };
      }
    } catch (e) {
      return e;
    }
    return this;
  }

  // async search(params) {
  //   const data = testData.data;
  //   this.fromInstance.setData = data.departure;
  //   this.toInstance.setData = data.return;
  //   return {
  //     from: this.fromInstance,
  //     to: this.toInstance,
  //   };
  // }

  /**
   * ANCHOR From Function
   * @author: yas
   * @desc
   * @return {object} this
   */
  get from() {
    return this.fromInstance;
  }

  /**
   * ANCHOR To Function
   * @author: yas
   * @desc
   * @return {object} this
   */
  get to() {
    return this.toInstance;
  }

  /**
   * ANCHOR To Function
   * @author: yas
   * @desc
   * @return {object} this
   */
  get currentData() {
    this.fromInstance.isCurrentValue = false;
    this.toInstance.isCurrentValue = false;
    this[`${this.current}Instance`].isCurrentValue = true;
    return this[`${this.current}Instance`];
  }

  setCurrent(newCurrent) {
    this.current = newCurrent;
    return this;
  }

  /**
   * ANCHOR book
   * @author: yas
   * @desc
   */
  book(info){
    console.log(">>this.fromInstance", this.fromInstance.selectedTrip);
    console.log(">>this.toInstance", this.toInstance.selectedTrip);
    
    console.log('info argo',info)
    
    let data = {
      "holder": {
        "email": info.email.value,
        "name": {},
        "phone": "123456789",
        "surname": "sam",
        "title": "mr",
        "type": "ADULT"
      },
      "language": "en",
      "clientReference": "IntegrationAgency",
      "remark": "string",
      "transfers": [
          {
            "detail": {
              "departureFlightNumber": "IB4321"
            },
            "rateKey": "DEPARTURE|ATLAS|1523|IATA|PMI|2019-11-25|02:40|2019-11-25|05:00|1~0~0|3|108473|TAB-PVT-PRM-X|108473|TAB-PVT-PRM-X|1|PRVT|X|CR|PRM|53.99|CPASTILLA|AEROPUERTO|13171|2246|PMI|SIMPLE|137dc5e239595495cabbf15df5b76570"
          }
      ]
    }

  //   {
  //     "clientReference": "string",
  //     "holder": {
  //         "email": "sameh@gmail.com",
  //         "name": "sameh",
  //         "phone": "123456789",
  //         "surname": "sam",
  //         "title": "MR",
  //         "type": "ADULT"
  //     },
  //     "language": "en",
  //     "transfers": [
  //         {
  //             "detail": {
  //                 "arrivalFlightNumber": "string",
  //                 "arrivalShipName": "string",
  //                 "arrivalTrainInfo": {
  //                     "trainCompanyName": "string",
  //                     "trainNumber": "string"
  //                 },
  //                 "departureFlightNumber": "string",
  //                 "departureShipName": "string",
  //                 "departureTrainInfo": {
  //                     "trainCompanyName": "string",
  //                     "trainNumber": "string"
  //                 }
  //             },
  //             "rateKey": "DEPARTURE|ATLAS|1523|IATA|PMI|2019-10-25T02:40|10~1~1|16|108470|TAB-PVT-BUS-M|108470|TAB-PVT-BUS-M|1|PRVT|M|MNBS|STND|88.29|CPASTILLA|AEROPUERTO|13171|2212|PMI"
  //         }
  //     ]
  // }

      try {
        // const route = this.route('book');
        // const res = await this.post(route, params);
        // if (res.status === 200) {
        //   const data = res.status ? res.data.data : this.handleError();
        //   this.fromInstance.setData = data.departure;
        //   this.toInstance.setData = data.return;
        //   return {
        //     from: this.fromInstance,
        //     to: this.toInstance,
        //   };
        // }
      } catch (e) {
        return e;
      }
      return this;
  }

  handleError(res){
    console.log("handleError",res);
  }
}
