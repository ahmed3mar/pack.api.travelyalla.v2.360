# Transfer Class

## Functions : 

* ### **Search :**
  Search Function Description.

  #### params :
    name         | type   | required | default 
   --------------|--------|----------|---------
    params       | object | yes      | none    

   > ---------------------------------------------------------
   > #### params content :
   >-----------------------------------------------------------
   > name         | type   | required | default | example
   >--------------|--------|----------|---------|--------
   > from         | string | yes      | none    | 'ATLAS'
   > from_code    | string | yes      | none    | '1523'
   > to           | string | yes      | none    | 'IATA'
   > to_code      | string | yes      | none    | 'PMI'
   > outbound     | string | yes      | none    | '2019-10-25'
   > outboundTime | string | yes      | none    | '5:00:00'
   > inbound      | string | yes      | none    | '2019-10-26'
   > inboundTime  | string | yes      | none    | '10:00:00'
   > adults       | number | yes      | none    | 10
   > children     | number | yes      | none    | 1
   > infants      | number | yes      | none    | 1
   > -----------------------------------------------------------

  ```javascript
    Transfer.search({...params});
  ```

* ### **From and To :**
  From and To Description.

  ```javascript
    Transfer.from; // return from data
    Transfer.to; // return to data
  ```


* ### **Sort :**
  Sort Function Description.

  #### params :
    name         | type   | required | default 
   --------------|--------|----------|---------
    params       | object | yes      | none    

   > --------------------------------------------------------------
   > #### params content :
   > --------------------------------------------------------------
   >  name         | type   | required | default | example
   > --------------|--------|----------|---------|--------
   >  price        | string | no       | none    | 'DESC' || 'ASC'
   > --------------------------------------------------------------

  ```javascript
    Transfer.from.sort({...params});
    Transfer.to.sort({...params});
  ```

* ### **Filter :**
  Filter Function Description.

  #### params :
    name         | type   | required | default 
   --------------|--------|----------|---------
    params       | object | yes      | none    

   > --------------------------------------------------------------
   > #### params content :
   > --------------------------------------------------------------
   >  name         | type   | required | default | example
   > --------------|--------|----------|---------|--------
   >  price        | object | no       | none    | { max: 500, min: 0 }
   > --------------------------------------------------------------

  ```javascript
    Transfer.from.filter({...params});
  ```


## Example : 

  ```javascript

  // transfer config object
  const config = {
      name: 'transfer',
      url: 'http://localhost:5004/api',
      version: 1,
      data: { transfer: true },
      routes: { search: 'transfer/search', details: 'test', arrivals: 'arrivals' } 
    };

  // search request
  const searchReq = {
    from: 'ATLAS',
    from_code: 1523,
    to: 'IATA',
    to_code: 'PMI',
    outbound: '2019-10-25',
    outboundTime: '5:00:00',
    inbound: '2019-10-26',
    inboundTime: '10:00:00',
    adults: 10,
    children: 1,
    infants: 1,
  };

    // create instance
    const transferInstance = new Transfer(config);

    // call search function
    await transferInstance.search(searchReq);

    // subscribe ( from )
    transferInstance.from.subscribe((res) => {
      console.log("from", res);
    });

    // subscribe ( to )
    transferInstance.to.subscribe((res) => {
      console.log("to", res);
    });

    // process chain ( from )
    transferInstance
    .from
    .process([(items) => items.map((item) => {
      return {...item, yas: true}
    })])
    .filter({ price: { max: 700, min: 300 } })
    .sort({ price: 'DESC' })
    .pagination({ limit: 2 })
    .run();

    // process chain ( to )
    transferInstance
    .to
    .filter({ price: { max: 100, min: 0 } })
    .pagination({ limit: 10 })
    .run();

    // pagination ( from )
    transferInstance.from.pagination(3)
  ```




## Life cycle :
![alt text](./transfer.png)