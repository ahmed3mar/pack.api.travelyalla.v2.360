import Hotels from './hotels';
import Transfer from './transfer';
import Mobile from './mobile';
import Auth from './auth';
import Url from './url';
import Fetch from './api';
import Promo from './promo';

const hotels = new Hotels();
const transfer = new Transfer();
const mobile = new Mobile();
const auth = new Auth();
const promo = new Promo();

export {
  Hotels,
  hotels,
  Transfer,
  transfer,
  Mobile,
  mobile,
  Url,
  Auth,
  auth,
  promo,
};

export default Fetch;
