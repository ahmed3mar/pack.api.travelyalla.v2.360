/* eslint-disable no-underscore-dangle */
/* eslint-disable camelcase */
import Api from '../api';

let instance = null;

export default class Flight extends Api {
  constructor(props) {
    super(props);

    if (instance) {
      return instance;
    }
    instance = this;
  }

  search(obj){
    console.log(">>>", obj);
  }

}