import Api from '../api';
import config from "../config/mobile";

let instance = null;

export default class Mobile extends Api {
  constructor(props) {
    super({ ...config, ...props });
    if (instance) {
      return instance;
    }
    instance = this;
  }

  async request(req) {
    const route = this.route('start');
    return await this.post(route, req).then( res => res.data.data);
  }
}