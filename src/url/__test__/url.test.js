import Url from '../index';

global.window = Object.create(window);

Object.defineProperty(window, 'location', {
  value: {
    pathname: '/ar-eg/transfer/search/ATLAS-1523/IATA-PMI/04AUG2019-1000/07AUG2019-1230/ADT-2/CHI-1/INF-5',
    search: '?test=true&test2=true2',
    hash: '#test',
    host: 'travelyalla.com',
    hostname: 'travelyalla.com',
    href: 'https://travelyalla.com/ar-eg/transfer/search/ATLAS-1523/IATA-PMI/04AUG2019-1000/07AUG2019-1230/ADT-2/CHI-1/INF-5?test=true&test2=true2#test',
    origin: 'https://travelyalla.com',
    port: '8000',
    protocol: 'https:',
  },
});


const url = new Url({
  'transfer/search': '/lang&nationality:{lang}-{nationality}/transfer/search/from:{from}-{from_code}/to:{to}-{to_code}/outbound:{outbound}-{outboundTime}/inbound:{inbound}-{inboundTime}/adults:ADT-{adults}/children:CHI-{children}/infants:INF-{infants}',
  'transfer/details': '/lang&nationality:{lang}-{nationality}/transfer/details/from:{from}-{from_code}/to:{to}-{to_code}/outbound:{outbound}-{outboundTime}/inbound:{inbound}-{inboundTime}/adults:ADT-{adults}/children:CHI-{children}/infants:INF-{infants}',
  'transfer/booking': '/lang&nationality:{lang}-{nationality}/transfer/booking/ratekey:{ratekey}',
});

describe('Url', () => {
  it('Test get query', () => {
    expect(url.query).toEqual({ test: 'true', test2: 'true2' });
  });

  it('Test get params', () => {
    expect(url.params).toEqual({
      lang: 'ar',
      nationality: 'eg',
      from: 'ATLAS',
      from_code: '1523',
      to: 'IATA',
      to_code: 'PMI',
      outbound: '04AUG2019',
      outboundTime: '1000',
      inbound: '07AUG2019',
      inboundTime: '1230',
      adults: '2',
      children: '1',
      infants: '5',
    });
  });

  it('Test get routename', () => {
    expect(url.routename).toEqual('transfer/search');
  });
});
