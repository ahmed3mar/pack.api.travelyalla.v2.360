/* eslint-disable no-useless-escape */

export default class Url {
  constructor(routes) {
    this.routes = Url.parseRoutes(routes);
  }

  get getRoutes() {
    return this.routes;
  }

  get params() {
    return this.parse().params;
  }

  get query() {
    return this.parse().query;
  }

  get routename() {
    return this.getRouteName();
  }

  /**
   * ANCHOR Parse Function
   * @author: yas
   * @desc return Params
   * @return {object}
   */
  parse() {
    const { pathname, search } = window.location;
    const routename = this.getRouteName();
    const segments = pathname.slice(1).split('/');
    const params = Url.getParams(segments, this.routes[routename]);
    const query = Url.getQuery(search);
    return {
      ...window.location,
      routename,
      params,
      query,
      format: this.routes[routename].format,
    };
  }

  /**
   * ANCHOR GetRouteName Function
   * @author: yas
   * @desc
   * @return {string} route name
   */
  getRouteName() {
    const { href } = window.location;
    let routeName;
    Object.keys(this.getRoutes).map((route) => {
      if (href.indexOf(route) > -1) {
        routeName = route;
      }
      return false;
    });
    return routeName;
  }

  /**
   * ANCHOR GetSegment Function
   * @author: yas
   * @desc
   * @param {object} routes
   * @return {object}
   */
  static parseRoutes(routes) {
    const newRoutes = {};
    Object.keys(routes).map((key) => {
      newRoutes[key] = {
        format: routes[key],
        parse: (() => routes[key].slice(1).split('/').map((item) => {
          const [name, format] = item.split(':');
          return [name,
            {
              name,
              format: format ? format.replace(/({)([A-Za-z0-9])\w+\S/g, '') : '',
              content: format ? format.replace(/(([a-zA-Z]*-{)|(}([a-zA-z]){)|(}(-){)|{|})/g, '|').split('|').filter(i => i !== '') : [],
            }];
        }))(),
      };
      return newRoutes[key];
    });
    return newRoutes;
  }

  static getSegment(url, route) {
    const obj = {};
    url.map((item, i) => {
      obj[route.parse[i][0]] = item;
      return false;
    });
    return obj;
  }

  static getParams(segments, route) {
    const obj = {};
    if (!route) return obj;
    segments.map((seqment, i) => {
      const values = route.parse[i][1].format !== '' ? seqment.split(route.parse[i][1].format).filter(ii => ii !== '') : [seqment];
      values.map((param, iii) => {
        if (route.parse[i][1].content[iii]) {
          obj[route.parse[i][1].content[iii]] = param;
        }
        return false;
      });
      return false;
    });
    return obj;
  }

  static getQuery(str) {
    if (str) {
      return str.slice(1).split('&')
        .map(p => p.split('='))
        .reduce((obj, pair) => {
          const [key, value] = pair.map(decodeURIComponent);
          return ({ ...obj, [key]: value });
        }, {});
    }
    return {};
  }
}
