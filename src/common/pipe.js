function pipe(...fns) {
  return x => fns.reduce((v, f) => (f !== null ? f(v) : v), x);
}
export default pipe;
