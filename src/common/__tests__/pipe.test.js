import pipe from '../pipe';

describe('Pipe', () => {
  it('Test pipe function', () => {
    const fn = num => (num + 10);
    const fn2 = num => num - 5;
    expect(pipe(fn, fn2)(10)).toEqual(15);
  });

  it('reTest pipe function', () => {
    const fn = num => (num + 10);
    const fn2 = num => num - 5;
    const fn3 = null;
    const fn4 = num => `${num}test`;
    expect(pipe(fn, fn2, fn3, fn4)(10)).toEqual('15test');
  });
});
