import Observable from '../observable';

const observableInstance = new Observable();

describe('Observable', () => {
  it('Test subscribe & notify function : subscribe() + notify()', () => {
    const fn1 = res => expect(res).toEqual('test');
    const fn2 = res => expect(res).toEqual('test');
    observableInstance.subscribe(fn1);
    observableInstance.subscribe(fn2);
    observableInstance.notify('test');
  });

  it('Test unsubscribe function : unsubscribe()', () => {
    const fn1 = res => res;
    const fn2 = res => res;
    observableInstance.subscribe(fn1);
    observableInstance.subscribe(fn2);
    observableInstance.unsubscribe(fn2);
    expect(observableInstance.observers.find(fn => fn === fn1)).toEqual(fn1);
    expect(observableInstance.observers.find(fn => fn === fn2)).toEqual(undefined);
  });
});
