/**
 * ANCHOR GetValueByObject Function
 * @author: yas
 * @desc
 * @param {object} object
 * @param {str} str
 * @return {*}
 */
function getValueByObject(object, str) {
  const arr = str.split('.');
  let val = object;
  arr.map((k) => {
    val = val[k];
    return false;
  });
  return val;
}
export default getValueByObject;
