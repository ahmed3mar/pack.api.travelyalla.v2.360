/* eslint-disable no-underscore-dangle */
/* eslint-disable camelcase */
import Api from '../api';
import config from '../config/hotels';

let instance = null;

/**
 * example : Hotels.search({...params}, [sort, fliter, tec...])
 */
export default class Hotels extends Api {
  constructor(props) {
    super({ ...config, ...props });

    if (instance) {
      return instance;
    }
    instance = this;
  }

  async details(id) {
    const route = this.route('details', { id });
    return (await this.get(route, { id }).then(res => res.data.data));
  }

  async rooms(id, { check_in, check_out, rooms }) {
    const route = this.route('rooms', { id });
    if (typeof rooms === 'object') rooms = JSON.stringify(rooms);
    return await this.post(route, { check_in, check_out, rooms }).then( res => res.data.data);
  }

  cancellationPolicy(id, { check_in, check_out, rooms, ...params }) {
    const route = this.route('cancellationPolicy', { id });

    if (typeof rooms === 'object') rooms = JSON.stringify(rooms);
    return this.post(route, { check_in, check_out, rooms, ...params });
  }

  async room(id, { check_in, check_out, rooms, ...params }) {
    const route = this.route('room', { id });
    return await this.post(route, { check_in, check_out, rooms, ...params }).then( res => res.data.data);
  }

  async preBooking({ ...props }) {
    const route = this.route('pre_booking');
    return await this.post(route, props).then( res => res.data.data );
  }

  // roomDetails(id, { check_in, check_out, rooms, ...params }) {
  //   return this.room(id, { check_in, check_out, rooms, ...params });
  // }

  // cancellationPolicy(id, { check_in, check_out, rooms, ...params }) {
  //   if (typeof rooms === 'object') rooms = JSON.stringify(rooms);//   return Api.post(Api.url(`hotels/${id}/cancellation-policy`), { check_in, check_out, rooms, ...params });
  // }

  // search(req, fn = []) {
  //   let fns = fn;

  //   if (req && typeof req === 'object' && req.constructor === Array) {
  //     fns = req;
  //   }

  //   if (fns.length > 0) {
  //     this._fns = fns;
  //   }

  //   if (req && typeof req === 'object' && req.constructor === Object) {
  //     this._req = req;
  //   }

  //   const { config } = this;
  //   const { data } = config;
  //   const param = { ...data, ...req };
  //   return new Promise((resolve, reject) => {
  //     const request = axios.post(Api.url('hotels/search'), { ...param });
  //     request.then((res) => {
  //       resolve(pipe(...fns)(res.data));
  //       this._results = res.data;
  //     });
  //     request.catch((err) => {
  //       reject(err);
  //     });
  //   });
  // }
}
