import Hotels from '../index';

const hotels = new Hotels();
jest.setTimeout(300000);


// async function xx() {
//   console.log("=====")
//   console.log(await hotels.rooms('TY0526159', { check_in: '19OCT2019', check_out: '20OCT2019' }))
//   console.log("=====")
// }
//
// xx();


describe('Hotel', () => {
  it('Test Hotel Details', async () => {
    const hotel = await hotels.details('TY1227353');
    if (hotel) {
      const rooms = await hotels.rooms(hotel.code, {
        check_in: '28OCT2019',
        check_out: '30OCT2019',
        rooms: '[{"adults": 1, "children": []}, {"adults": 1, "children": []}]',
      });
      if (rooms) {
        const room = await hotels.room(hotel.code, {
          check_in: '28OCT2019',
          check_out: '30OCT2019',
          rooms: '[{"adults": 1, "children": []}, {"adults": 1, "children": []}]',
          ...rooms.rooms[0].params,
        })
        if (room) {

          try {

            const book = await hotels.preBooking({
              hotel_id: hotel.code,
              title: 'Mr',
              first_name: 'Ahmed',
              last_name: 'Ammar',
              country_code: '+20',
              mobile: '01069769798',
              email: 'ahmed@gmail.com',
              passengers: [
                [
                  {
                    title: 'Mr',
                    first_name: 'Ahmed',
                    last_name: 'Ammar',
                  },
                  // {
                  //   title: 'Mr',
                  //   first_name: 'Ahmed',
                  //   last_name: 'Test 2',
                  //   age: 6,
                  // },

                ],
                [
                  {
                    title: 'Mr',
                    first_name: 'TTTTEST',
                    last_name: 'Ammar',
                  },
                ],
              ],

              check_in: '28OCT2019',
              check_out: '30OCT2019',
              rooms: [{"adults": 1, "children": []}, {"adults": 1, "children": []}],
              adults: 1,
              children: 2,
              device: 'mobile',
              ...rooms.rooms[0].params,
            })
            console.log("bookbookbook", book)
          } catch (e) {
            console.log("NNNNNNO")
          }


        } else {
          console.log("NO ROOM")
        }
      } else {
        console.log("FFFF");
      }
    }

      // console.log("Hotel", hotel)

      // hotels.rooms(hotel.code).then(rooms => {
      //   console.log(rooms);
      //
      //   expect(hotel.code).toEqual('TY1227353');
      //
      // })
      //   .catch(err => {
      //     console.log("ROOMS ERROR")
      //   })
      // console.log(hotel);

  });


  // it('Test Hotel Rooms', () => {
  //   // hotels.details('TY0526159').then((hotel) => {
  //     hotels.rooms('TY0526159', {
  //       check_in: '18AUG2019',
  //       check_out: '19AUG2019',
  //       rooms: [{ adults: 2, children: [] }],
  //     }).then((rooms) => {
  //
  //       expect(hotel.code).toEqual('TY0526159');
  //
  //       // hotels.room(hotel.code, {
  //       //   check_in: '18AUG2019',
  //       //   check_out: '19AUG2019',
  //       //   rooms: [{ adults: 2, children: [] }],
  //       //   ...rooms.rooms[0].params,
  //       // }).then((room) => {
  //       //   console.log('THE ROOOOOOM');
  //       //   // expect(true).toEqual(true);
  //       // }).catch((err) => {
  //       //   console.log('ERROROROROOR', rooms.rooms[0].params);
  //       //   // console.log('ROOM ERROR', err);
  //       // });
  //     }).catch((err) => {
  //       // console.log('==');
  //     });
  //   // });
  // });
});
