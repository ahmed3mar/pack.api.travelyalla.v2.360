export default {
  name: 'transfer',
  url: 'http://192.168.1.111:8000/api',
  version: 1,
  data: { transfer: true },
  routes: {
    search: 'transfer/search',
    details: 'test',
    arrivals: (url, version, params) => `${url}/v${version}/transfers/transfer_locations/${params.key}`,
  },
};
