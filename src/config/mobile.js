export default {
  name: 'mobile',
  url: 'https://www.travelyalla.com/api',
  // url: 'http://localhost:8000/api',
  version: 1,
  data: { transfer: true },
  routes: {
    start: 'start',
  },
};
