export default {
  name: 'hotels',
  url: 'https://www.travelyalla.com/api',
  // url: 'http://localhost:8000/api',
  version: 1,
  data: { transfer: true },
  routes: {
    login: 'auth/login',
    register: 'auth/register',
    forgotPassword: 'auth/password/email',
    resetPassword: 'auth/password/reset',
  },
};
