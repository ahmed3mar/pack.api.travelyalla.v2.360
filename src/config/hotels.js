export default {
  name: 'hotels',
  url: 'https://www.travelyalla.com/api',
  // url: 'http://localhost:8000/api',
  version: 1,
  data: { transfer: true },
  routes: {
    details: (url, version, params) => `${url}/v${version}/hotels/${params.id}/details`,
    rooms: (url, version, params) => `${url}/v${version}/hotels/${params.id}/rooms`,
    room: (url, version, params) => `${url}/v${version}/hotels/${params.id}/reservation`,
    cancellationPolicy: (url, version, params) => `${url}/v${version}/hotels/${params.id}/cancellation-policy`,
    arrivals: 'arrivals',
    pre_booking: 'hotels/pre_booking',
  },
};
