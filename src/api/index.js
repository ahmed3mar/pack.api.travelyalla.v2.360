import axios from 'axios';
import 'babel-polyfill';

export default class Api {
  constructor(config = {}) {
    this.config = config;
    Api.info = this.config;
  }

  setHeaders(headers) {
    this.config = { ...this.config,
      data: {
        ...this.config.data,
        headers: { ...this.config.headers, ...headers },
      } };
    return this;
  }

  /**
   * ANCHOR Route Function
   * @author: yas
   * @param {string} param
   * @return {string} url
   */
  route(param, params = {}) {
    const { url, version } = this.config;

    if (typeof param !== 'string') {
      // add new route
      const route = Api.routeParser(url, version, param.path);
      this.config.routes[param.name] = param.path;
      return route;
    }

    // return route
    return Api.routeParser(url, version, this.config.routes[param], params);
  }

  /**
   * ANCHOR Get Function
   * @author: yas
   * @param {string} url
   * @param {object} params
   * @return {object} return axios result
   */
  async get(url, params) {
    try {
      const { data } = this.config;
      const param = { ...data, ...params };
      // const headers = {
      //   'Access-Control-Allow-Origin': '*'
      // };
      return await axios.get(url, {
        params: {
          ...params,
        },
        // headers: headers,
        ...data,
      });
    } catch (e) {
      throw e;
    }
  }

  /**
   * ANCHOR Post Function
   * @author: yas
   * @param {string} url
   * @param {object} params
   * @return {object} return axios result
   */
  async post(url, params) {
    try {
      const { data } = this.config;
      const headers = {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        ...data.headers,
      };
      return await axios.post(url, params, { ...data, headers: headers} );
    } catch (e) {
      throw e;
    }
  }

  /**
   * ANCHOR Get Info Function
   * @author: yas
   * @return {object} api information
   */
  static get info() {
    return this.APIINFO;
  }

  /**
 * ANCHOR Set Info Function
 * @author: yas
 * @param {object} i
 */
  static set info(i) {
    if (!Object.prototype.hasOwnProperty.call(Api, 'APIINFO')) {
      this.APIINFO = {};
    }
    this.APIINFO = { ...this.APIINFO, [i.name]: i };
  }

  /**
 * ANCHOR Route Parser Function
 * @author: yas
 * @param {string} url
 * @param {number} version
 * @param {string} path
 * @return {string} path
 */
  static routeParser(url, version, path, params) {
    if (typeof path === 'function') {
      return path(url, version, params);
    }
    return `${url}/v${version}/${path}`;
  }

  /**
 * ANCHOR Route Parser Function
 * @author: yas
 * @return {object} config
 */
  get configObject() {
    return this.config;
  }
}
