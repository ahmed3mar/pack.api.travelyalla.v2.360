import Api from '../index';

// transfer instance
const transferConfig = {
  name: 'transfer',
  url: 'http://localhost:5004/api',
  version: 1,
  data: { transfer: true },
  routes: { search: 'search', details: 'test' }
};
const ApiTransferInstance = new Api(transferConfig);
const TransferRoutes = ApiTransferInstance.route('search'); // 'http://localhost:5004/api/v1/search'

// hotel instance
const HotelConfig = { name: 'hotel', url: 'http://localhost:5004/api', version: 1, data: { hotel: true }, routes: { search: 'search', details: 'test' } };
const ApiHotelInstance = new Api(HotelConfig);
const HotelRoutes = ApiHotelInstance.route('search');

describe('Api', () => {
  describe('Test initialize ', () => {
    it('Test initialize function : new Api()', () => {
      expect(ApiTransferInstance.configObject).toEqual(transferConfig);
    });
  });

  describe('Test get routes ', () => {
    it('Test get Route : Api.route()', () => {
      expect(ApiTransferInstance.route('search')).toEqual('http://localhost:5004/api/v1/search');
    });

    it('Test add Route : Api.route()', () => {
      const TransferReservation = ApiTransferInstance.route({ name: 'reservation', path: 'reservation' });
      expect(ApiTransferInstance.configObject.routes.reservation).toEqual('reservation');
    });
  });

  describe('Test get and post route ', () => {
    test('Test get function : Api.get()', () => ApiTransferInstance.get(TransferRoutes, { boo: false }).then((res) => {
      expect(res.data).toEqual({ status: 200, response: { transfer: 'true', boo: 'false' } });
    }));


    test('Test post function : Api.post()', () => ApiHotelInstance.post(HotelRoutes, { boo: false }).then((res) => {
      expect(res.data).toEqual({ status: 200, response: { hotel: true, boo: false } });
    }));
  });

  describe('Test get state ', () => {
    it('state : Api.state', () => {
      expect(Api.info).toEqual({
        hotel: { name: 'hotel', url: 'http://localhost:5004/api', version: 1, data: { hotel: true }, routes: { search: 'search', details: 'test' } },
        transfer: { name: 'transfer', url: 'http://localhost:5004/api', version: 1, data: { transfer: true }, routes: { search: 'search', details: 'test', reservation: 'reservation' } },
      });
    });
  });
});
