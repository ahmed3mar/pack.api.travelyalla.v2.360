/* eslint-disable no-underscore-dangle */
/* eslint-disable camelcase */
import Api from '../api';
import config from '../config/promo';

let instance = null;

/**
 * example : Hotels.search({...params}, [sort, fliter, tec...])
 */
export default class Promo extends Api {
  constructor(props) {
    super({ ...config, ...props });

    if (instance) {
      return instance;
    }
    instance = this;
  }

  async check({ type, promo, price, currency, platform }) {
    const route = this.route('check');
    return (await this.post(route, { type, promo, price, currency, platform }).then(res => res.data.data));
  }
}
