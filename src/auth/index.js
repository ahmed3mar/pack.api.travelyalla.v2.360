import Api from '../api';
import config from '../config/auth';

let instance = null;

export default class Auth extends Api {
  constructor(props) {
    super({ ...config, ...props });

    if (instance) {
      return instance;
    }
    instance = this;
  }

  async login(email, password) {
    const route = this.route('login');
    return (await this.post(route, { email, password })
      .then(res => (res.status === 200 ? res.data.data : this.handleError())));
  }

  // eslint-disable-next-line camelcase
  async register({ name, email, password, password_confirmation }) {
    const route = this.route('register');
    try {
      return await this.post(route, { name, email, password, password_confirmation })
        .then(res => (res.status === 200 ? res.data.data : this.handleError()));
    } catch (e) {
      throw e;
    }
  }

  async forgotPassword({ email }) {
    // return Api.post(Api.url('auth/password/email'), { email });
    const route = this.route('forgotPassword');
    return (await this.post(route, { email })
      .then(res => (res.status === 200 ? res.data.data : this.handleError())));
  }

  async resetPassword({ code, email, password }) {
    const route = this.route('forgotPassword');
    return (await this.post(route, { code, email, password })
      .then(res => (res.status === 200 ? res.data.data : this.handleError())));
    // return Api.post(Api.url('auth/password/reset'), { code, email, password });
  }

  handleError(err){
    console.log("error", err);
  }
}
